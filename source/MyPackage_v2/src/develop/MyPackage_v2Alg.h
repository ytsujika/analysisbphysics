#ifndef MYPACKAGE_V2_MYPACKAGE_V2ALG_H
#define MYPACKAGE_V2_MYPACKAGE_V2ALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
// TMVA include(s)
#if not defined(__CINT__) || defined(__MAKECINT__)
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#endif

#include <vector>
#include <string>
#include <map>
#include <TLorentzVector.h>
#include <functional>
#include <unordered_set>
#include <algorithm>
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TVector3.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TTree.h"

#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"

//Example ROOT Includes
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "AthAnalysisBaseComps/AthAnalysisHelper.h"
#include "AthContainers/ConstDataVector.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "JetAnalysisInterfaces/IJetSelectorTool.h"
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetModifier.h"
#include "JetInterface/IJetSelector.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetMomentTools/JetForwardJvtTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "METInterface/IMETMaker.h"
#include "METUtilities/METHelpers.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h"
#include "PathResolver/PathResolver.h"
#include "TrigDecisionTool/TrigDecisionTool.h"


struct Particle_s {
  TVector3 p3;
  Int_t pdgId;
  Double_t mass;
  Double_t charge;
};

struct Decay_s {
  Particle_s self;
  std::vector<Particle_s> parents;
  std::vector<Particle_s> siblings;
};

struct Vtx_param {
  Bool_t is_Valid;
  Float_t eta1;
  Float_t phi1;
  Float_t eta2;
  Float_t phi2;
  Float_t min_d;
};

class MyPackage_v2Alg: public ::AthAnalysisAlgorithm {
 public:
  MyPackage_v2Alg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyPackage_v2Alg();

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  //virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed

  xAOD::TruthParticle* descend(xAOD::TruthParticle*, int);
  bool isMuon(const xAOD::TrackParticle*, const xAOD::MuonContainer*);
  StatusCode getTruthPartMatch(const xAOD::TruthEventContainer*, std::vector<Decay_s>&, std::vector<Double_t>&);
  //StatusCode Initialize_variable();

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp

  //Consts
  Float_t EQ_THR      = 1.0e-5;
  Float_t MU_MASS     = 105.66;
  Float_t K_MASS      = 493.68;
  Float_t PI_MASS     = 139.57;
  Float_t D0_MASS     = 1864.83;
  Float_t Dpst_MASS   = 2010.26;
  Float_t MASS_WIDTH  = 1.0;

  Float_t DR_TO_JET_CUT = 0.4;

  //Float_t DIS_CUT = 10;
  Float_t DIS_CUT = 1.;

  private:

  //Example algorithm property, see constructor for declaration:
  //int m_nProperty = 0;

  // Dict of Jets and Events

  //Output tree
  TTree* B_tree = 0;
  TTree* Truth_B_tree = 0;
  TTree* MET_tree = 0;
  TTree* Truth_MET_tree = 0;
  
  //Variables for output branches
  std::unique_ptr<Int_t> evNum;
  std::unique_ptr<Int_t> jetNum;

  std::unique_ptr<std::vector<Bool_t>> m_isMatch;
  std::unique_ptr<std::vector<Bool_t>> m_isMatch_Pass;
  std::unique_ptr<std::vector<Bool_t>> m_isPassed;
  std::unique_ptr<std::vector<Bool_t>> m_isPassedTight;
  std::unique_ptr<Bool_t> m_TriggerPass;
  std::unique_ptr<std::vector<Bool_t>> m_TriggerType;
  std::unique_ptr<Bool_t> m_PassGRL;

  std::unique_ptr<Int_t> m_n_Track;
  std::unique_ptr<std::vector<Float_t>> m_Track_d0;

  std::unique_ptr<Int_t> m_n_B;
  std::unique_ptr<Int_t> m_n_B_Truth;
  std::unique_ptr<Int_t> m_n_Dstar1;
  std::unique_ptr<Int_t> m_n_Dstar1_Pass;
  std::unique_ptr<Int_t> m_n_Dstar1_Pass_Tight;
  std::unique_ptr<Int_t> m_n_Dstar1_Match;
  std::unique_ptr<Int_t> m_n_Dstar1_Match_Pass;
  std::unique_ptr<Int_t> m_n_Dstar1_Truth;
  std::unique_ptr<Int_t> m_n_Dstar1_Truth_HighPt;
  std::unique_ptr<Int_t> m_n_Dstar2;
  std::unique_ptr<Int_t> m_n_Dstar2_Pass;
  std::unique_ptr<Int_t> m_n_Dstar2_Pass_Tight;
  std::unique_ptr<Int_t> m_n_Dstar2_Match;
  std::unique_ptr<Int_t> m_n_Dstar2_Match_Pass;
  std::unique_ptr<Int_t> m_n_Dstar2_Truth;
  std::unique_ptr<Int_t> m_n_Dstar2_Truth_HighPt;
  std::unique_ptr<Int_t> m_n_Mu_Truth;
  std::unique_ptr<Int_t> m_n_MET;
  std::unique_ptr<Int_t> m_n_MET_Truth;
  std::unique_ptr<std::vector<Int_t>> m_n_PV;
  std::unique_ptr<std::vector<Int_t>> m_n_RPV;

  std::unique_ptr<Float_t> m_px_MET;
  std::unique_ptr<Float_t> m_py_MET;
  std::unique_ptr<Float_t> m_sum_MET;
  
  std::unique_ptr<Float_t> m_px_Rec_MET;
  std::unique_ptr<Float_t> m_py_Rec_MET;
  std::unique_ptr<Float_t> m_sum_Rec_MET;
  
  std::unique_ptr<Float_t> m_px_Rec_MET_Mu;
  std::unique_ptr<Float_t> m_py_Rec_MET_Mu;
  std::unique_ptr<Float_t> m_sum_Rec_MET_Mu;
  
  std::unique_ptr<Float_t> m_px_MET_Track;
  std::unique_ptr<Float_t> m_py_MET_Track;
  std::unique_ptr<Float_t> m_sum_MET_Track;

  std::unique_ptr<Float_t> m_px_MET_Truth;
  std::unique_ptr<Float_t> m_py_MET_Truth;
  std::unique_ptr<Float_t> m_sum_MET_Truth;

  std::unique_ptr<std::vector<Int_t>> m_DecayType;
  std::unique_ptr<std::vector<Float_t>> m_BDT_score;
  std::unique_ptr<std::vector<Int_t>> m_B_flavor;
  std::unique_ptr<std::vector<Float_t>> m_B_nPart;
  std::unique_ptr<std::vector<Float_t>> m_B_x;
  std::unique_ptr<std::vector<Float_t>> m_B_y;
  std::unique_ptr<std::vector<Float_t>> m_B_z;
  std::unique_ptr<std::vector<Float_t>> m_B_mass;
  std::unique_ptr<std::vector<Float_t>> m_B_mass_err;
  std::unique_ptr<std::vector<Float_t>> m_B_eta;
  std::unique_ptr<std::vector<Float_t>> m_B_phi;
  std::unique_ptr<std::vector<Float_t>> m_B_Chi2;
  std::unique_ptr<std::vector<Float_t>> m_B_nDoF;
  std::unique_ptr<std::vector<Float_t>> m_B_Pt;
  std::unique_ptr<std::vector<Float_t>> m_B_Px;
  std::unique_ptr<std::vector<Float_t>> m_B_Py;
  std::unique_ptr<std::vector<Float_t>> m_B_Pz;
  std::unique_ptr<std::vector<Float_t>> m_B_E;
  std::unique_ptr<std::vector<Float_t>> m_Visible_mass;
  std::unique_ptr<std::vector<Float_t>> m_Visible_pT;
  std::unique_ptr<std::vector<Float_t>> m_Visible_eta;
  std::unique_ptr<std::vector<Float_t>> m_Visible_phi;

  std::unique_ptr<std::vector<Float_t>> m_PiD0_mass;
  std::unique_ptr<std::vector<Float_t>> m_D0Pi_mass;
  std::unique_ptr<std::vector<Float_t>> m_D0Pi_pT;
  std::unique_ptr<std::vector<Float_t>> m_D0Pi_eta;
  std::unique_ptr<std::vector<Float_t>> m_D0Pi_phi;
  std::unique_ptr<std::vector<Float_t>> m_D0Pi_Chi2;

  std::unique_ptr<std::vector<Float_t>> m_PV_x;
  std::unique_ptr<std::vector<Float_t>> m_PV_y;
  std::unique_ptr<std::vector<Float_t>> m_PV_z;
  std::unique_ptr<std::vector<Float_t>> m_RPV_x;
  std::unique_ptr<std::vector<Float_t>> m_RPV_y;
  std::unique_ptr<std::vector<Float_t>> m_RPV_z;

  std::unique_ptr<std::vector<Float_t>> m_LxyMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_LxyErrMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_A0MaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_A0ErrMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_A0xyMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_A0xyErrMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_Z0MaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_Z0ErrMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_TauInvMassPVMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_TauErrInvMassPVMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_TauConstMassPVMaxSumPt2;
  std::unique_ptr<std::vector<Float_t>> m_TauErrConstMassPVMaxSumPt2;

  std::unique_ptr<std::vector<Int_t>>   m_n_MuPi;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_mass;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_pT;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_eta;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_phi;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_chi2;
  std::unique_ptr<std::vector<Float_t>> m_MuPi_nDoF;
  std::unique_ptr<std::vector<Float_t>> m_MuPiAft_mass;
  std::unique_ptr<std::vector<Float_t>> m_MuPiLxyMaxSumPt2;

  std::unique_ptr<std::vector<Double_t>> m_Pi_s_m;
  std::unique_ptr<std::vector<Double_t>> m_Pi_s_pT;
  std::unique_ptr<std::vector<Double_t>> m_Pi_s_charge;
  std::unique_ptr<std::vector<Double_t>> m_Pi_s_eta;
  std::unique_ptr<std::vector<Double_t>> m_Pi_s_phi;
  std::unique_ptr<std::vector<Double_t>> m_Pi_s_theta_star;
  std::unique_ptr<std::vector<Float_t>> m_Pi_s_d0;
  std::unique_ptr<std::vector<Float_t>> m_Pi_s_z0;
  std::unique_ptr<std::vector<Float_t>> m_Pi_s_qOverP;
  std::unique_ptr<std::vector<Int_t>> m_n_Mu;
  std::unique_ptr<std::vector<Int_t>> m_Mu_quality;
  std::unique_ptr<std::vector<Bool_t>> m_Mu_PassIso;
  std::unique_ptr<std::vector<Double_t>> m_Mu_m;
  std::unique_ptr<std::vector<Double_t>> m_Mu_pT;
  std::unique_ptr<std::vector<Double_t>> m_Mu_charge;
  std::unique_ptr<std::vector<Double_t>> m_Mu_eta;
  std::unique_ptr<std::vector<Double_t>> m_Mu_phi;
  std::unique_ptr<std::vector<Float_t>> m_Mu_d0;
  std::unique_ptr<std::vector<Float_t>> m_Mu_z0;
  std::unique_ptr<std::vector<Float_t>> m_Mu_qOverP;
  std::unique_ptr<std::vector<Float_t>> m_Mu_chi2;
  std::unique_ptr<std::vector<Float_t>> m_Mu_nDoF;
  std::unique_ptr<std::vector<Float_t>> m_Mu_chi2_B;
  std::unique_ptr<std::vector<Float_t>> m_Mu_nDoF_B;

  std::unique_ptr<std::vector<Float_t>> m_D0_mass;
  std::unique_ptr<std::vector<Float_t>> m_D0_massErr;
  std::unique_ptr<std::vector<Float_t>> m_D0_Pt;
  std::unique_ptr<std::vector<Float_t>> m_D0_PtErr;
  std::unique_ptr<std::vector<Float_t>> m_D0_Lxy;
  std::unique_ptr<std::vector<Float_t>> m_D0_LxyErr;
  std::unique_ptr<std::vector<Float_t>> m_D0_Tau;
  std::unique_ptr<std::vector<Float_t>> m_D0_TauErr;
  std::unique_ptr<std::vector<Float_t>> m_D0_chi2;
  std::unique_ptr<std::vector<Float_t>> m_D0_nDoF;
  std::unique_ptr<std::vector<Bool_t>> m_D0_flag;
  std::unique_ptr<std::vector<Float_t>> m_Kpi_mass;
  std::unique_ptr<std::vector<Float_t>> m_Kpi_pT;
  std::unique_ptr<std::vector<Float_t>> m_Kpi_eta;
  std::unique_ptr<std::vector<Float_t>> m_Kpi_phi;

  std::unique_ptr<std::vector<Double_t>> m_Pi1_m;
  std::unique_ptr<std::vector<Double_t>> m_Pi1_pT;
  std::unique_ptr<std::vector<Double_t>> m_Pi1_charge;
  std::unique_ptr<std::vector<Double_t>> m_Pi1_eta;
  std::unique_ptr<std::vector<Double_t>> m_Pi1_phi;
  std::unique_ptr<std::vector<Double_t>> m_Pi1_theta_star;
  std::unique_ptr<std::vector<Float_t>> m_Pi1_d0;
  std::unique_ptr<std::vector<Float_t>> m_Pi1_z0;
  std::unique_ptr<std::vector<Float_t>> m_Pi1_qOverP;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_m;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_pT;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_charge;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_eta;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_phi;
  std::unique_ptr<std::vector<Double_t>> m_Pi2_theta_star;
  std::unique_ptr<std::vector<Float_t>> m_Pi2_d0;
  std::unique_ptr<std::vector<Float_t>> m_Pi2_z0;
  std::unique_ptr<std::vector<Float_t>> m_Pi2_qOverP;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_m;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_pT;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_charge;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_eta;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_phi;
  std::unique_ptr<std::vector<Double_t>> m_Pi3_theta_star;
  std::unique_ptr<std::vector<Float_t>> m_Pi3_d0;
  std::unique_ptr<std::vector<Float_t>> m_Pi3_z0;
  std::unique_ptr<std::vector<Float_t>> m_Pi3_qOverP;
  std::unique_ptr<std::vector<Double_t>> m_K_m;
  std::unique_ptr<std::vector<Double_t>> m_K_pT;
  std::unique_ptr<std::vector<Double_t>> m_K_charge;
  std::unique_ptr<std::vector<Double_t>> m_K_eta;
  std::unique_ptr<std::vector<Double_t>> m_K_phi;
  std::unique_ptr<std::vector<Double_t>> m_K_theta_star;
  std::unique_ptr<std::vector<Float_t>> m_K_d0;
  std::unique_ptr<std::vector<Float_t>> m_K_z0;
  std::unique_ptr<std::vector<Float_t>> m_K_qOverP;

  std::unique_ptr<Int_t> num_B;
  std::unique_ptr<Int_t> num_B_Truth;
  std::unique_ptr<Int_t> num_Dstar1;
  std::unique_ptr<Int_t> num_Dstar1_Pass;
  std::unique_ptr<Int_t> num_Dstar1_Match;
  std::unique_ptr<Int_t> num_Dstar1_Match_Pass;
  std::unique_ptr<Int_t> num_Dstar1_Truth;
  std::unique_ptr<Int_t> num_Dstar1_Truth_HighPt;
  std::unique_ptr<Int_t> num_Dstar2;
  std::unique_ptr<Int_t> num_Dstar2_Pass;
  std::unique_ptr<Int_t> num_Dstar2_Match;
  std::unique_ptr<Int_t> num_Dstar2_Match_Pass;
  std::unique_ptr<Int_t> num_Dstar2_Truth;
  std::unique_ptr<Int_t> num_Dstar2_Truth_HighPt;
  std::unique_ptr<Int_t> num_B_Dstar_Truth;
  std::unique_ptr<Int_t> num_B_Dstar_D0Pi_Truth;
  std::unique_ptr<std::vector<Bool_t>>                Truth_isHighPt;
  std::unique_ptr<std::vector<Int_t>>                 Truth_Flavor;
  std::unique_ptr<std::vector<Float_t>>               Truth_DecayTime;
  std::unique_ptr<std::vector<Float_t>>               Truth_DeltaT;
  std::unique_ptr<std::vector<Bool_t>>                Truth_hasMuon;
  std::unique_ptr<std::vector<Int_t>>                 Truth_DecayType;
  std::unique_ptr<std::vector<Int_t>>                 Truth_pdgID;
  std::unique_ptr<std::vector<Int_t>>                 Truth_parent_num;
  std::unique_ptr<std::vector<std::vector<Int_t>>>    Truth_parent_pdgID;
  std::unique_ptr<std::vector<Int_t>>                 Truth_trk_num;
  std::unique_ptr<std::vector<std::vector<Int_t>>>    Truth_trk_pdgID;
  std::unique_ptr<std::vector<std::vector<Int_t>>>    Truth_child_num;
  std::unique_ptr<std::vector<std::vector<std::vector<Int_t>>>>  Truth_child_pdgID;

  std::unique_ptr<std::vector<Double_t>>               Truth_x;
  std::unique_ptr<std::vector<Double_t>>               Truth_y;
  std::unique_ptr<std::vector<Double_t>>               Truth_z;
  std::unique_ptr<std::vector<Double_t>>               Truth_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_B_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_B_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_B_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Dstar_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_Dstar_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_Dstar_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Dstar_charge;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_mass;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Mu_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_Mu_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_Mu_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Mu_charge;
  std::unique_ptr<std::vector<Double_t>>               Truth_Nu_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_Nu_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_Nu_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_mass;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_pT;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_eta;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_phi;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_charge;
  std::unique_ptr<std::vector<Double_t>>               Truth_Pi_s_pdgID;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_child_num;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_charged_child_num;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_reco_mass;
  std::unique_ptr<std::vector<Double_t>>               Truth_D0_reco_pT;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_mass;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_pT;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_eta;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_phi;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_charge;
  std::unique_ptr<std::vector<std::vector<Double_t>>>  Truth_D0_child_pdgID;

  std::unique_ptr<std::vector<Bool_t>>                isMatch;
  std::unique_ptr<std::vector<Float_t>>               Min_d;
  std::unique_ptr<std::vector<Float_t>>               Min_dx;
  std::unique_ptr<std::vector<Float_t>>               Min_dy;
  std::unique_ptr<std::vector<Float_t>>               Min_dz;
  std::unique_ptr<std::vector<Float_t>>               Reco_x;
  std::unique_ptr<std::vector<Float_t>>               Reco_y;
  std::unique_ptr<std::vector<Float_t>>               Reco_z;
  std::unique_ptr<std::vector<Float_t>>               Reco_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_phi;
  std::unique_ptr<std::vector<Bool_t>>                Reco_isPassed;
  std::unique_ptr<std::vector<Int_t>>                 Reco_DecayType;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_m;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_pT;
  std::unique_ptr<std::vector<Int_t>>                 Reco_B_flavor;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_m;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_pT;
  std::unique_ptr<std::vector<Int_t>>                 Reco_Dstar_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_m;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_charge;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_pT;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_eta;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_phi;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_charge;

  std::unique_ptr<std::vector<Bool_t>>                isMatch_Pass;
  std::unique_ptr<std::vector<Float_t>>               Min_d_Pass;
  std::unique_ptr<std::vector<Float_t>>               Min_dx_Pass;
  std::unique_ptr<std::vector<Float_t>>               Min_dy_Pass;
  std::unique_ptr<std::vector<Float_t>>               Min_dz_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_x_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_y_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_z_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_phi_Pass;
  std::unique_ptr<std::vector<Int_t>>                 Reco_DecayType_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_m_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_B_pT_Pass;
  std::unique_ptr<std::vector<Int_t>>                 Reco_B_flavor_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_m_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Dstar_pT_Pass;
  std::unique_ptr<std::vector<Int_t>>                 Reco_Dstar_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_m_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_D0_pT_Pass;
  std::unique_ptr<std::vector<Int_t>>                 Reco_D0_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Mu_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi_s_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi1_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi2_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_Pi3_charge_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_pT_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_eta_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_phi_Pass;
  std::unique_ptr<std::vector<Float_t>>               Reco_K_charge_Pass;


  //MET information
  std::unique_ptr<std::vector<Float_t>> m_MET_px;
  std::unique_ptr<std::vector<Float_t>> m_MET_py;
  std::unique_ptr<std::vector<Float_t>> m_MET_sum;
  
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_px;
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_py;
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_sum;
  
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_Mu_px;
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_Mu_py;
  std::unique_ptr<std::vector<Float_t>> m_Rec_MET_Mu_sum;
  
  std::unique_ptr<std::vector<Float_t>> m_Track_MET_px;
  std::unique_ptr<std::vector<Float_t>> m_Track_MET_py;
  std::unique_ptr<std::vector<Float_t>> m_Track_MET_sum;
  
  std::unique_ptr<std::vector<Float_t>> m_Truth_MET_px;
  std::unique_ptr<std::vector<Float_t>> m_Truth_MET_py;
  std::unique_ptr<std::vector<Float_t>> m_Truth_MET_sum;
  
  
  // Control isEventFlagBitSet
  bool m_is_mc;

  // Tools
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl;
  asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelection;
  asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationAndSmearingTool;
  asg::AnaToolHandle<CP::IIsolationSelectionTool> m_isoTool; 
  asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;
  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibration;
  asg::AnaToolHandle<IJetSelector> m_jetCleaningTool;
  asg::AnaToolHandle<IJetModifier> m_jetFwdJvtTool;
  asg::AnaToolHandle<IJetUpdateJvt> m_jetJvtUpdateTool;
  asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_EgammaCalibrationAndSmearingTool;
  asg::AnaToolHandle<IMETMaker> m_metMaker;

  //Trigger List
  std::vector<TString> triggerNames;
  
  TMVA::Reader* reader;
  Float_t i_Mu_pT=0;
  Float_t i_Pi_pT=0;
  Float_t i_K_pT=0;
  Float_t i_Pi_s_pT=0;
  Float_t i_Pi_Mu_dR=0;
  Float_t i_K_Mu_dR=0;
  Float_t i_Pi_s_Mu_dR=0;
  Float_t i_Pi_Pi_s_dR=0;
  Float_t i_K_Pi_s_dR=0;
  Float_t i_K_Pi_dR=0;
  Float_t i_D0_chi2=0;
  Float_t i_B_chi2=0;
  Float_t i_Cos_theta_star_K=0;
  Float_t i_B_pT=0;
  Float_t i_B_Mass=0;

  // Initialize of variables and trees
  virtual StatusCode initialize() {
    ATH_MSG_INFO ("Initializing " << name() << "...");
    //
    //This is called once, before the start of the event loop
    //Retrieves of tools you have configured in the joboptions go here
    //

    //HERE IS AN EXAMPLE
    //We will create a histogram and a ttree and register them to the histsvc
    //Remember to configure the histsvc stream in the joboptions
    //
    //m_myHist = new TH1D("myHist","myHist",100,0,100);
    //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
    //m_myTree = new TTree("myTree","myTree");
    //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory


    //GRL filtering
    std::vector<std::string> vecgrl;
    vecgrl.push_back("GoodRunsLists/data17_5TeV/data17_5TeV.periodAllYear_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml");
    vecgrl.push_back("GoodRunsLists/data17_13TeV/20200425/data17_13TeV.periodN_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml");
    vecgrl.push_back("GoodRunsLists/data18_13TeV/20200425/data18_13TeV.periodG4J_DetStatus-v105-pro22-13_MERGED_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml");
    //vecgrl.push_back("GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml");
    ATH_CHECK (m_grl.setProperty("GoodRunsListVec", vecgrl));
    ATH_CHECK (m_grl.setProperty("PassThrough", false));
    ATH_CHECK (m_grl.initialize());

    //Muon Selection
    ATH_CHECK (m_muonSelection.setProperty("MaxEta", 2.4));
    //ATH_CHECK (m_muonSelection.setProperty("MuQuality", (int) xAOD::Muon::Quality::Tight));
    //ATH_CHECK (m_muonSelection.setProperty("MuQuality", (int) xAOD::Muon::Quality::Medium));
    ATH_CHECK (m_muonSelection.setProperty("MuQuality", (int) xAOD::Muon::Quality::Loose));
    ATH_CHECK (m_muonSelection.initialize());
    //Muon Calibration
    ATH_CHECK (m_muonCalibrationAndSmearingTool.initialize());

    //Muon Isolation
    /// use "myTestWP" WP for muon
    //ATH_CHECK ( m_isoTool.setProperty("MuonWP", "FixedCutHighPtTrackOnly") );
    ATH_CHECK ( m_isoTool.retrieve() );

    //Jet Calibration
    TString jetAlgo = "AntiKt4EMPFlow"; // Jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see below) 
    TString config = "JES_MC16Recommendation_LowMu1718_MCJES_GSC_PFlow_May2022_Rel21.config"; // Global config (see below)
    TString calibArea = "00-04-82"; // Calibration Area tag (see below) 
    TString calibSeq;
    bool isData;
    //calibSeq = "JetArea_Residual_EtaJES_GSC_Smear"; // Calibration sequence to apply (see below) 
    calibSeq = "EtaJES_GSC"; // Calibration sequence to apply (see below) 
    if(m_is_mc){
      isData = false; // bool describing if the events are data or from simulation
    } else {
      //calibSeq = "EtaJES_GSC"; // Calibration sequence to apply (see below) 
      isData = true; // bool describing if the events are data or from simulation
    }
    m_jetCalibration.setTypeAndName("JetCalibrationTool/JetCalibrationTool_EMPFlow");
    if( !m_jetCalibration.isUserConfigured() ){
      ATH_CHECK( m_jetCalibration.setProperty("JetCollection",jetAlgo.Data()) );
      ATH_CHECK( m_jetCalibration.setProperty("ConfigFile",config.Data()) );
      ATH_CHECK( m_jetCalibration.setProperty("CalibSequence",calibSeq.Data()) );
      ATH_CHECK( m_jetCalibration.setProperty("CalibArea",calibArea.Data()) );
      ATH_CHECK( m_jetCalibration.setProperty("IsData",isData) );
      ATH_CHECK( m_jetCalibration.retrieve() );
    }

    //Jet Cleaning
    ATH_CHECK( m_jetCleaningTool.setProperty("CutLevel", "LooseBad") );
    ATH_CHECK( m_jetCleaningTool.setProperty("DoUgly", false) );
    ATH_CHECK( m_jetCleaningTool.retrieve() );

    //Jet pileup removal
    ATH_CHECK( m_jetJvtUpdateTool.retrieve() );
    ATH_CHECK( m_jetFwdJvtTool.setProperty("UseTightOP", true) );
    ATH_CHECK( m_jetFwdJvtTool.setProperty("ForwardMaxPt",60e3) );
    ATH_CHECK( m_jetFwdJvtTool.setProperty("OutputDec", "passFJvt") );
    ATH_CHECK( m_jetFwdJvtTool.retrieve() );

    //Photon Calibration
    ATH_CHECK( m_EgammaCalibrationAndSmearingTool.setProperty("ESModel", "es2018_R21_v0") );
    ATH_CHECK( m_EgammaCalibrationAndSmearingTool.retrieve() );
    
    //MET Calibration
    ATH_CHECK( m_metMaker.setProperty("ORCaloTaggedMuons", true) );
    ATH_CHECK( m_metMaker.setProperty("DoSetMuonJetEMScale", true) );
    ATH_CHECK( m_metMaker.setProperty("DoRemoveMuonJets", true) );
    //ATH_CHECK( m_metMaker.setProperty("JetRejectionDec","passFJvt"));
    ATH_CHECK( m_metMaker.retrieve() );
    
    //TMVA  -- BDT --
    reader = new TMVA::Reader( "!Color:!Silent" );   
    reader->AddVariable("Mu_pT", &i_Mu_pT);
    reader->AddVariable("Pi_pT", &i_Pi_pT);
    reader->AddVariable("K_pT", &i_K_pT);
    reader->AddVariable("Pi_s_pT", &i_Pi_s_pT);
    reader->AddVariable("Pi_Mu_dR", &i_Pi_Mu_dR);
    reader->AddVariable("K_Mu_dR", &i_K_Mu_dR);
    reader->AddVariable("Pi_s_Mu_dR", &i_Pi_s_Mu_dR);
    reader->AddVariable("Pi_Pi_s_dR", &i_Pi_Pi_s_dR);
    reader->AddVariable("K_Pi_s_dR", &i_K_Pi_s_dR);
    reader->AddVariable("K_Pi_dR", &i_K_Pi_dR);
    reader->AddVariable("D0_chi2", &i_D0_chi2);
    reader->AddVariable("B_chi2", &i_B_chi2);
    reader->AddVariable("Cos_theta_K", &i_Cos_theta_star_K);
    reader->AddVariable("B_pT", &i_B_pT);
    reader->AddVariable("B_Mass", &i_B_Mass);
   
    // --- Book the MVA methods
    TString dir    = "/home/ytsujika/TMVA/Dataset/weights/";
    TString prefix = "TMVAClassification";
    // Book method(s)
    TString methodName("BDT method");
    TString weightfile = dir + prefix + TString("_BDT.weights.xml");
    reader->BookMVA(methodName, weightfile);

    //Load Trigger Lists
    TString dir2   = "/home/ytsujika/AnalysisBphys/source/MyPackage_v2/data/trigger/";
    TString listfile = dir2 + TString("data.txt");
    // Open file
    std::ifstream List_file ( listfile );
    if( List_file.is_open() ){
      TString trigger;
      while( List_file ){
	// Read line
	trigger.ReadLine(List_file);
	if( trigger.Contains("#") || trigger.IsWhitespace() ) continue;
	// Save and remove whitespace around trigger name
	triggerNames.push_back(trigger.Strip(TString::kBoth));
      }   
    }

    // Set output tree
    B_tree = new TTree("B", "B");
    CHECK( histSvc()->regTree("/MYSTREAM/B", B_tree ) );

    Truth_B_tree = new TTree("Truth_B", "Truth_B");
    CHECK( histSvc()->regTree("/MYSTREAM/Truth_B", Truth_B_tree ) );

    MET_tree = new TTree("MET", "MET");
    CHECK( histSvc()->regTree("/MYSTREAM/MET", MET_tree ) );

    Truth_MET_tree = new TTree("Truth_MET", "Truth_MET");
    CHECK( histSvc()->regTree("/MYSTREAM/Truth_MET", Truth_MET_tree ) );

    // Setup branches
    evNum = std::make_unique<Int_t> ();
    B_tree->Branch("event_num", evNum.get() );
  
    jetNum = std::make_unique<Int_t> ();
    B_tree->Branch("jet_num", jetNum.get() );

    m_PassGRL   = std::make_unique<Bool_t> (); B_tree->Branch("PassGRL", m_PassGRL.get()  );
    m_n_Track   = std::make_unique<Int_t> ();  B_tree->Branch("n_Track", m_n_Track.get()  );
    m_Track_d0  = std::make_unique<std::vector<Float_t>> ();   B_tree->Branch("Track_d0",   m_Track_d0.get()  );

    m_isMatch       = std::make_unique<std::vector<Bool_t>> ();  B_tree->Branch("isMatch",       m_isMatch.get()  );
    m_isMatch_Pass  = std::make_unique<std::vector<Bool_t>> ();  B_tree->Branch("isMatch_Pass",  m_isMatch_Pass.get()  );
    m_isPassed      = std::make_unique<std::vector<Bool_t>> ();  B_tree->Branch("isPassed",      m_isPassed.get()  );
    m_isPassedTight = std::make_unique<std::vector<Bool_t>> ();  B_tree->Branch("isPassedTight", m_isPassedTight.get()  );
    m_TriggerPass   = std::make_unique<Bool_t> (); B_tree->Branch("TriggerPass", m_TriggerPass.get()  );
    m_TriggerType   = std::make_unique<std::vector<Bool_t>> (); B_tree->Branch("TriggerType", m_TriggerType.get()  );

    // (Mu + Pi) + D0(K + Pi) - B0 parameter 
    m_n_B                 = std::make_unique<Int_t> (); B_tree->Branch("n_B",                 m_n_B.get()  );
    m_n_B_Truth           = std::make_unique<Int_t> (); B_tree->Branch("n_B_Truth",           m_n_B_Truth.get()  );
    m_n_Dstar1            = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1",            m_n_Dstar1.get()  );
    m_n_Dstar1_Pass       = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Pass",       m_n_Dstar1_Pass.get()  );
    m_n_Dstar1_Pass_Tight = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Pass_Tight", m_n_Dstar1_Pass_Tight.get()  );
    m_n_Dstar1_Match      = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Match",      m_n_Dstar1_Match.get()  );
    m_n_Dstar1_Match_Pass = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Match_Pass", m_n_Dstar1_Match_Pass.get()  );
    m_n_Dstar1_Truth      = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Truth",      m_n_Dstar1_Truth.get()  );
    m_n_Dstar1_Truth_HighPt = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar1_Truth_HighPt",  m_n_Dstar1_Truth_HighPt.get()  );
    m_n_Dstar2            = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2",            m_n_Dstar2.get()  );
    m_n_Dstar2_Pass       = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Pass",       m_n_Dstar2_Pass.get()  );
    m_n_Dstar2_Pass_Tight = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Pass_Tight", m_n_Dstar2_Pass_Tight.get()  );
    m_n_Dstar2_Match      = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Match",      m_n_Dstar2_Match.get()  );
    m_n_Dstar2_Match_Pass = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Match_Pass", m_n_Dstar2_Match_Pass.get()  );
    m_n_Dstar2_Truth      = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Truth",      m_n_Dstar2_Truth.get()  );
    m_n_Dstar2_Truth_HighPt = std::make_unique<Int_t> (); B_tree->Branch("n_Dstar2_Truth_HighPt",  m_n_Dstar2_Truth_HighPt.get()  );
    m_n_Mu_Truth         = std::make_unique<Int_t> (); B_tree->Branch("n_Mu_Truth",         m_n_Mu_Truth.get()  );
    m_n_MET              = std::make_unique<Int_t> (); B_tree->Branch("n_MET",              m_n_MET.get()  );
    m_n_MET_Truth        = std::make_unique<Int_t> (); B_tree->Branch("n_MET_Truth",        m_n_MET_Truth.get()  );
    m_n_PV          = std::make_unique<std::vector<Int_t>> (); B_tree->Branch("n_PV",       m_n_PV.get()  );
    m_n_RPV         = std::make_unique<std::vector<Int_t>> (); B_tree->Branch("n_RPV",      m_n_RPV.get()  );

    m_px_MET             = std::make_unique<Float_t> ();   B_tree->Branch("MET_px",         m_px_MET.get()  );
    m_py_MET             = std::make_unique<Float_t> ();   B_tree->Branch("MET_py",         m_py_MET.get()  );
    m_sum_MET            = std::make_unique<Float_t> ();   B_tree->Branch("MET_sum",        m_sum_MET.get()  );
    m_px_Rec_MET         = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_px",     m_px_Rec_MET.get()  );
    m_py_Rec_MET         = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_py",     m_py_Rec_MET.get()  );
    m_sum_Rec_MET        = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_sum",    m_sum_Rec_MET.get()  );
    m_px_Rec_MET_Mu      = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_Mu_px",  m_px_Rec_MET_Mu.get()  );
    m_py_Rec_MET_Mu      = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_Mu_py",  m_py_Rec_MET_Mu.get()  );
    m_sum_Rec_MET_Mu     = std::make_unique<Float_t> ();   B_tree->Branch("Rec_MET_Mu_sum", m_sum_Rec_MET_Mu.get()  );
    m_px_MET_Track       = std::make_unique<Float_t> ();   B_tree->Branch("Track_MET_px",   m_px_MET_Track.get()  );
    m_py_MET_Track       = std::make_unique<Float_t> ();   B_tree->Branch("Track_MET_py",   m_py_MET_Track.get()  );
    m_sum_MET_Track      = std::make_unique<Float_t> ();   B_tree->Branch("Track_MET_sum",  m_sum_MET_Track.get()  );
    m_px_MET_Truth       = std::make_unique<Float_t> ();   B_tree->Branch("Truth_MET_px",   m_px_MET_Truth.get()  );
    m_py_MET_Truth       = std::make_unique<Float_t> ();   B_tree->Branch("Truth_MET_py",   m_py_MET_Truth.get()  );
    m_sum_MET_Truth      = std::make_unique<Float_t> ();   B_tree->Branch("Truth_MET_sum",  m_sum_MET_Truth.get()  );

    m_DecayType     = std::make_unique<std::vector<Int_t>> ();   B_tree->Branch("DecayType",     m_DecayType.get()  );
    m_BDT_score     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("BDT_score",     m_BDT_score.get()  );
    m_B_flavor      = std::make_unique<std::vector<Int_t>> ();   B_tree->Branch("B_flavor",      m_B_flavor.get()  );
    m_B_nPart       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_nPart",       m_B_nPart.get()  );
    m_B_mass        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_mass",        m_B_mass.get()  );
    m_B_mass_err    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_mass_err",    m_B_mass_err.get()  );
    m_B_eta         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_eta",         m_B_eta.get()  );
    m_B_phi         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_phi",         m_B_phi.get()  );
    m_B_Px          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_px",          m_B_Px.get()  );
    m_B_Py          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_py",          m_B_Py.get()  );
    m_B_Pz          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_pz",          m_B_Pz.get()  );
    m_B_E           = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_E",           m_B_E.get()  );
    m_B_Pt          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_pT",          m_B_Pt.get()  );
    m_B_Chi2        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_chi2",        m_B_Chi2.get()  );
    m_B_nDoF        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_nDoF",        m_B_nDoF.get()  );
    m_B_x           = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_x",           m_B_x.get()  );
    m_B_y           = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_y",           m_B_y.get()  );
    m_B_z           = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("B_z",           m_B_z.get()  );
    m_Visible_mass  = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Visible_mass",  m_Visible_mass.get()  );
    m_Visible_pT    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Visible_pT",    m_Visible_pT.get()  );
    m_Visible_eta   = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Visible_eta",   m_Visible_eta.get()  );
    m_Visible_phi   = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Visible_phi",   m_Visible_phi.get()  );
    m_PiD0_mass     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("PiD0_mass",     m_PiD0_mass.get()  );

    m_D0Pi_mass     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0Pi_mass",     m_D0Pi_mass.get()  );
    m_D0Pi_pT       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0Pi_pT",       m_D0Pi_pT.get()  );
    m_D0Pi_eta      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0Pi_eta",      m_D0Pi_eta.get()  );
    m_D0Pi_phi      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0Pi_phi",      m_D0Pi_phi.get()  );

    m_PV_x          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("PV_x",          m_PV_x.get()  );
    m_PV_y          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("PV_y",          m_PV_y.get()  );
    m_PV_z          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("PV_z",          m_PV_z.get()  );
    m_RPV_x         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("RPV_x",         m_RPV_x.get()  );
    m_RPV_y         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("RPV_y",         m_RPV_y.get()  );
    m_RPV_z         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("RPV_z",         m_RPV_z.get()  );

    m_LxyMaxSumPt2      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("LxyMaxSumPt2",      m_LxyMaxSumPt2.get()  );
    m_LxyErrMaxSumPt2   = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("LxyErrMaxSumPt2",   m_LxyErrMaxSumPt2.get()  );
    m_A0MaxSumPt2       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("A0MaxSumPt2",       m_A0MaxSumPt2.get()  );
    m_A0ErrMaxSumPt2    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("A0ErrMaxSumPt2",    m_A0ErrMaxSumPt2.get()  );
    m_A0xyMaxSumPt2     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("A0xyMaxSumPt2",     m_A0xyMaxSumPt2.get()  );
    m_A0xyErrMaxSumPt2  = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("A0xyErrMaxSumPt2",  m_A0xyErrMaxSumPt2.get()  );
    m_Z0MaxSumPt2       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Z0MaxSumPt2",       m_Z0MaxSumPt2.get()  );
    m_Z0ErrMaxSumPt2    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("Z0ErrMaxSumPt2",    m_Z0ErrMaxSumPt2.get()  );
    m_TauInvMassPVMaxSumPt2      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("TauInvMassPVMaxSumPt2",      m_TauInvMassPVMaxSumPt2.get()  );
    m_TauErrInvMassPVMaxSumPt2   = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("TauErrInvMassPVMaxSumPt2",   m_TauErrInvMassPVMaxSumPt2.get()  );
    m_TauConstMassPVMaxSumPt2    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("TauConstMassPVMaxSumPt2",    m_TauConstMassPVMaxSumPt2.get()  );
    m_TauErrConstMassPVMaxSumPt2 = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("TauErrConstMassPVMaxSumPt2", m_TauErrConstMassPVMaxSumPt2.get()  );

    //Mu + Soft-Pi - B0 vertex 
    m_n_MuPi           = std::make_unique<std::vector<Int_t>> ();   B_tree->Branch("n_MuPi",       m_n_MuPi.get()  );
    m_MuPi_mass        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_mass",    m_MuPi_mass.get()  );
    m_MuPi_pT          = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_pT",      m_MuPi_pT.get()  );
    m_MuPi_eta         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_eta",     m_MuPi_eta.get()  );
    m_MuPi_phi         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_phi",     m_MuPi_phi.get()  );
    m_MuPi_chi2        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_chi2",    m_MuPi_chi2.get()  );
    m_MuPi_nDoF        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPi_nDoF",    m_MuPi_nDoF.get()  );
    m_MuPiAft_mass     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPiAft_mass", m_MuPiAft_mass.get()  );
    m_MuPiLxyMaxSumPt2 = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("MuPiLxyMaxSumPt2", m_MuPiLxyMaxSumPt2.get()  );

    m_Pi_s_m      = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_mass",    m_Pi_s_m.get()  );
    m_Pi_s_pT     = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_pT",      m_Pi_s_pT.get()  );
    m_Pi_s_charge = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_charge",  m_Pi_s_charge.get()  );
    m_Pi_s_eta    = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_eta",     m_Pi_s_eta.get()  );
    m_Pi_s_phi    = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_phi",     m_Pi_s_phi.get()  );
    m_Pi_s_theta_star = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi_s_theta_star",     m_Pi_s_theta_star.get()  );
    m_Pi_s_d0     = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi_s_d0",      m_Pi_s_d0.get()  );
    m_Pi_s_z0     = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi_s_z0",      m_Pi_s_z0.get()  );
    m_Pi_s_qOverP = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi_s_qOverP",  m_Pi_s_qOverP.get()  );
    m_n_Mu        = std::make_unique<std::vector<Int_t>> ();    B_tree->Branch("n_Mu",         m_n_Mu.get()  );
    m_Mu_quality  = std::make_unique<std::vector<Int_t>> ();    B_tree->Branch("Mu_quality",   m_Mu_quality.get()  );
    m_Mu_PassIso  = std::make_unique<std::vector<Bool_t>> ();   B_tree->Branch("Mu_PassIso",   m_Mu_PassIso.get()  );
    m_Mu_m        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Mu_mass",      m_Mu_m.get()  );
    m_Mu_pT       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Mu_pT",        m_Mu_pT.get()  );
    m_Mu_charge   = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Mu_charge",    m_Mu_charge.get()  );
    m_Mu_eta      = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Mu_eta",       m_Mu_eta.get()  );
    m_Mu_phi      = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Mu_phi",       m_Mu_phi.get()  );
    m_Mu_d0       = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_d0",        m_Mu_d0.get()  );
    m_Mu_z0       = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_z0",        m_Mu_z0.get()  );
    m_Mu_qOverP   = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_qOverP",    m_Mu_qOverP.get()  );
    m_Mu_chi2     = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_chi2",      m_Mu_chi2.get()  );
    m_Mu_nDoF     = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_nDoF",      m_Mu_nDoF.get()  );
    m_Mu_chi2_B   = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_Chi2_B",    m_Mu_chi2_B.get()  );
    m_Mu_nDoF_B   = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Mu_nDoF_B",    m_Mu_nDoF_B.get()  );

    //K + Pi - D0 vertex 
    m_D0_mass       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_mass",      m_D0_mass.get()  );
    m_D0_massErr    = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_mass_err",  m_D0_massErr.get()  );
    m_D0_Pt         = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_pT",        m_D0_Pt.get()  );
    m_D0_PtErr      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_pT_err",    m_D0_PtErr.get()  );
    m_D0_Lxy        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_Lxy",       m_D0_Lxy.get()  );
    m_D0_LxyErr     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_Lxy_err",   m_D0_LxyErr.get()  );
    m_D0_Tau        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_Tau",       m_D0_Tau.get()  );
    m_D0_TauErr     = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_Tau_err",   m_D0_TauErr.get()  );
    m_D0_chi2       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_chi2",      m_D0_chi2.get()  );
    m_D0_nDoF       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("D0_nDoF",      m_D0_nDoF.get()  );
    m_D0_flag       = std::make_unique<std::vector<Bool_t>> ();  B_tree->Branch("D0_flag",      m_D0_flag.get()  );
    m_Kpi_mass      = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("KPi_mass",     m_Kpi_mass.get()  );
    m_Kpi_pT        = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("KPi_pT",       m_Kpi_pT.get()  );
    m_Kpi_eta       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("KPi_eta",      m_Kpi_eta.get()  );
    m_Kpi_phi       = std::make_unique<std::vector<Float_t>> (); B_tree->Branch("KPi_phi",      m_Kpi_phi.get()  );

    m_Pi1_m         = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_mass",    m_Pi1_m.get()  );
    m_Pi1_pT        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_pT",      m_Pi1_pT.get()  );
    m_Pi1_charge    = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_charge",  m_Pi1_charge.get()  );
    m_Pi1_eta       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_eta",     m_Pi1_eta.get()  );
    m_Pi1_phi       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_phi",     m_Pi1_phi.get()  );
    m_Pi1_theta_star = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi1_theta_star", m_Pi1_theta_star.get()  );
    m_Pi1_d0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi1_d0",      m_Pi1_d0.get()  );
    m_Pi1_z0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi1_z0",      m_Pi1_z0.get()  );
    m_Pi1_qOverP    = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi1_qOverP",  m_Pi1_qOverP.get()  );
    m_Pi2_m         = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_mass",    m_Pi2_m.get()  );
    m_Pi2_pT        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_pT",      m_Pi2_pT.get()  );
    m_Pi2_charge    = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_charge",  m_Pi2_charge.get()  );
    m_Pi2_eta       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_eta",     m_Pi2_eta.get()  );
    m_Pi2_phi       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_phi",     m_Pi2_phi.get()  );
    m_Pi2_theta_star = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi2_theta_star", m_Pi2_theta_star.get()  );
    m_Pi2_d0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi2_d0",      m_Pi2_d0.get()  );
    m_Pi2_z0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi2_z0",      m_Pi2_z0.get()  );
    m_Pi2_qOverP    = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi2_qOverP",  m_Pi2_qOverP.get()  );
    m_Pi3_m         = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_mass",    m_Pi3_m.get()  );
    m_Pi3_pT        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_pT",      m_Pi3_pT.get()  );
    m_Pi3_charge    = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_charge",  m_Pi3_charge.get()  );
    m_Pi3_eta       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_eta",     m_Pi3_eta.get()  );
    m_Pi3_phi       = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_phi",     m_Pi3_phi.get()  );
    m_Pi3_theta_star = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("Pi3_theta_star", m_Pi3_theta_star.get()  );
    m_Pi3_d0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi3_d0",      m_Pi3_d0.get()  );
    m_Pi3_z0        = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi3_z0",      m_Pi3_z0.get()  );
    m_Pi3_qOverP    = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("Pi3_qOverP",  m_Pi3_qOverP.get()  );
    m_K_m          = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_mass",     m_K_m.get()  );
    m_K_pT         = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_pT",       m_K_pT.get()  );
    m_K_charge     = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_charge",   m_K_charge.get()  );
    m_K_eta        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_eta",      m_K_eta.get()  );
    m_K_phi        = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_phi",      m_K_phi.get()  );
    m_K_theta_star = std::make_unique<std::vector<Double_t>> (); B_tree->Branch("K_theta_star",      m_K_theta_star.get()  );
    m_K_d0      = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("K_d0",       m_K_d0.get()  );
    m_K_z0      = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("K_z0",       m_K_z0.get()  );
    m_K_qOverP  = std::make_unique<std::vector<Float_t>> ();  B_tree->Branch("K_qOverP",   m_K_qOverP.get()  );

    num_B                  = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_B",                   num_B.get() );
    num_B_Truth            = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_B_Truth",             num_B_Truth.get() );
    num_Dstar1              = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1",              num_Dstar1.get() );
    num_Dstar1_Pass         = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1_Pass",         num_Dstar1_Pass.get() );
    num_Dstar1_Match        = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1_Match",        num_Dstar1_Match.get() );
    num_Dstar1_Match_Pass   = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1_Match_Pass",   num_Dstar1_Match_Pass.get() );
    num_Dstar1_Truth        = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1_Truth",        num_Dstar1_Truth.get() );
    num_Dstar1_Truth_HighPt = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar1_Truth_HighPt", num_Dstar1_Truth_HighPt.get() );
    num_Dstar2              = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2",              num_Dstar2.get() );
    num_Dstar2_Pass         = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2_Pass",         num_Dstar2_Pass.get() );
    num_Dstar2_Match        = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2_Match",        num_Dstar2_Match.get() );
    num_Dstar2_Match_Pass   = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2_Match_Pass",   num_Dstar2_Match_Pass.get() );
    num_Dstar2_Truth        = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2_Truth",        num_Dstar2_Truth.get() );
    num_Dstar2_Truth_HighPt = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_Dstar2_Truth_HighPt", num_Dstar2_Truth_HighPt.get() );
    num_B_Dstar_Truth      = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_B_Dstar_Truth",      num_B_Dstar_Truth.get() );
    num_B_Dstar_D0Pi_Truth = std::make_unique<Int_t> ();   Truth_B_tree->Branch("n_B_Dstar_D0Pi_Truth", num_B_Dstar_D0Pi_Truth.get() );
    Truth_B_tree->Branch("n_MET",              m_n_MET.get()  );
    Truth_B_tree->Branch("n_MET_Truth",        m_n_MET_Truth.get()  );

    Truth_isHighPt     = std::make_unique<std::vector<Bool_t>> ();  Truth_B_tree->Branch("isHighPt", Truth_isHighPt.get() );
    Truth_Flavor       = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Flavor", Truth_Flavor.get() );
    Truth_DecayTime    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("DecayTime", Truth_DecayTime.get() );
    Truth_DeltaT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("DeltaT", Truth_DeltaT.get() );
    Truth_hasMuon      = std::make_unique<std::vector<Bool_t>> ();  Truth_B_tree->Branch("hasMuon", Truth_hasMuon.get() );
    Truth_DecayType    = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("DecayType", Truth_DecayType.get() );
    Truth_pdgID        = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("pdgID", Truth_pdgID.get() );
    Truth_parent_num   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("parent_num", Truth_parent_num.get() );
    Truth_parent_pdgID = std::make_unique<std::vector<std::vector<Int_t>>> (); Truth_B_tree->Branch("parent_pdgID", Truth_parent_pdgID.get() );
    Truth_trk_num      = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("track_num", Truth_trk_num.get() );
    Truth_trk_pdgID    = std::make_unique<std::vector<std::vector<Int_t>>> (); Truth_B_tree->Branch("track_pdgID", Truth_trk_pdgID.get() );
    Truth_child_num    = std::make_unique<std::vector<std::vector<Int_t>>> ();   Truth_B_tree->Branch("child_num", Truth_child_num.get() );
    Truth_child_pdgID  = std::make_unique<std::vector<std::vector<std::vector<Int_t>>>> (); Truth_B_tree->Branch("child_pdgID", Truth_child_pdgID.get() );
    Truth_x          = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Truth_x", Truth_x.get() );
    Truth_y          = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Truth_y", Truth_y.get() );
    Truth_z          = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Truth_z", Truth_z.get() );
    Truth_eta        = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Truth_eta", Truth_eta.get() );
    Truth_phi        = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Truth_phi", Truth_phi.get() );
    Truth_B_pT       = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("B_pT",     Truth_B_pT.get() );
    Truth_B_eta      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("B_eta",    Truth_B_eta.get() );
    Truth_B_phi      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("B_phi",    Truth_B_phi.get() );
    Truth_Dstar_pT      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Dstar_pT",     Truth_Dstar_pT.get() );
    Truth_Dstar_eta     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Dstar_eta",    Truth_Dstar_eta.get() );
    Truth_Dstar_phi     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Dstar_phi",    Truth_Dstar_phi.get() );
    Truth_Dstar_charge  = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Dstar_charge", Truth_Dstar_charge.get() );
    Truth_D0_mass    = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_mass",   Truth_D0_mass.get() );
    Truth_D0_pT      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_pT",     Truth_D0_pT.get() );
    Truth_D0_eta     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_eta",    Truth_D0_eta.get() );
    Truth_D0_phi     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_phi",    Truth_D0_phi.get() );
    Truth_Mu_pT      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Mu_pT",     Truth_Mu_pT.get() );
    Truth_Mu_eta     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Mu_eta",    Truth_Mu_eta.get() );
    Truth_Mu_phi     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Mu_phi",    Truth_Mu_phi.get() );
    Truth_Mu_charge  = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Mu_charge", Truth_Mu_charge.get() );
    Truth_Nu_pT      = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Nu_pT",     Truth_Nu_pT.get() );
    Truth_Nu_eta     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Nu_eta",    Truth_Nu_eta.get() );
    Truth_Nu_phi     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Nu_phi",    Truth_Nu_phi.get() );
    Truth_Pi_s_mass   = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_mass",   Truth_Pi_s_mass.get() );
    Truth_Pi_s_pT     = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_pT",     Truth_Pi_s_pT.get() );
    Truth_Pi_s_eta    = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_eta",    Truth_Pi_s_eta.get() );
    Truth_Pi_s_phi    = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_phi",    Truth_Pi_s_phi.get() );
    Truth_Pi_s_charge = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_charge", Truth_Pi_s_charge.get() );
    Truth_Pi_s_pdgID  = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("Pi_s_pdgID",  Truth_Pi_s_pdgID.get() );
    Truth_D0_child_num  = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_child_num",  Truth_D0_child_num.get() );
    Truth_D0_charged_child_num  = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_charged_child_num",  Truth_D0_charged_child_num.get() );
    Truth_D0_reco_mass    = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_reco_mass",   Truth_D0_reco_mass.get() );
    Truth_D0_reco_pT    = std::make_unique<std::vector<Double_t>> (); Truth_B_tree->Branch("D0_reco_pT",   Truth_D0_reco_pT.get() );
    Truth_D0_child_mass    = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_mass",   Truth_D0_child_mass.get() );
    Truth_D0_child_pT      = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_pT",     Truth_D0_child_pT.get() );
    Truth_D0_child_eta     = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_eta",    Truth_D0_child_eta.get() );
    Truth_D0_child_phi     = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_phi",    Truth_D0_child_phi.get() );
    Truth_D0_child_charge  = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_charge", Truth_D0_child_charge.get() );
    Truth_D0_child_pdgID   = std::make_unique<std::vector<std::vector<Double_t>>> (); Truth_B_tree->Branch("D0_child_pdgID",  Truth_D0_child_pdgID.get() );

    isMatch          = std::make_unique<std::vector<Bool_t>> ();  Truth_B_tree->Branch("isMatch", isMatch.get() );
    Min_d            = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_d", Min_d.get() );
    Min_dx           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dx", Min_dx.get() );
    Min_dy           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dy", Min_dy.get() );
    Min_dz           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dz", Min_dz.get() );
    Reco_x           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_x",   Reco_x.get() );
    Reco_y           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_y",   Reco_y.get() );
    Reco_z           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_z",   Reco_z.get() );
    Reco_eta         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_eta", Reco_eta.get() );
    Reco_phi         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_phi", Reco_phi.get() );
    Reco_isPassed    = std::make_unique<std::vector<Bool_t>> ();  Truth_B_tree->Branch("Reco_isPassed", Reco_isPassed.get() );
    Reco_DecayType   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_DecayType", Reco_DecayType.get() );
    Reco_B_m         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_mass",  Reco_B_m.get() );
    Reco_B_eta       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_eta",  Reco_B_eta.get() );
    Reco_B_phi       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_phi",  Reco_B_phi.get() );
    Reco_B_pT        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_pT",  Reco_B_pT.get() );
    Reco_B_flavor    = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_B_flavor", Reco_B_flavor.get() );
    Reco_Dstar_m        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_mass",  Reco_Dstar_m.get() );
    Reco_Dstar_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_eta",  Reco_Dstar_eta.get() );
    Reco_Dstar_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_phi",  Reco_Dstar_phi.get() );
    Reco_Dstar_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_pT",  Reco_Dstar_pT.get() );
    Reco_Dstar_charge   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_Dstar_charge", Reco_Dstar_charge.get() );
    Reco_D0_m        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_mass",  Reco_D0_m.get() );
    Reco_D0_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_eta",  Reco_D0_eta.get() );
    Reco_D0_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_phi",  Reco_D0_phi.get() );
    Reco_D0_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_pT",  Reco_D0_pT.get() );
    Reco_Mu_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_eta", Reco_Mu_eta.get() );
    Reco_Mu_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_phi", Reco_Mu_phi.get() );
    Reco_Mu_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_pT",  Reco_Mu_pT.get() );
    Reco_Mu_charge   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_charge", Reco_Mu_charge.get() );
    Reco_Pi_s_pT     = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_pT",  Reco_Pi_s_pT.get() );
    Reco_Pi_s_eta    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_eta",  Reco_Pi_s_eta.get() );
    Reco_Pi_s_phi    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_phi",  Reco_Pi_s_phi.get() );
    Reco_Pi_s_charge = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_charge", Reco_Pi_s_charge.get() );
    Reco_Pi1_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_pT",  Reco_Pi1_pT.get() );
    Reco_Pi1_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_eta",  Reco_Pi1_eta.get() );
    Reco_Pi1_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_phi",  Reco_Pi1_phi.get() );
    Reco_Pi1_charge   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_charge", Reco_Pi1_charge.get() );
    Reco_Pi2_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_pT",  Reco_Pi2_pT.get() );
    Reco_Pi2_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_eta",  Reco_Pi2_eta.get() );
    Reco_Pi2_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_phi",  Reco_Pi2_phi.get() );
    Reco_Pi2_charge   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_charge", Reco_Pi2_charge.get() );
    Reco_Pi3_pT       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_pT",  Reco_Pi3_pT.get() );
    Reco_Pi3_eta      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_eta",  Reco_Pi3_eta.get() );
    Reco_Pi3_phi      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_phi",  Reco_Pi3_phi.get() );
    Reco_Pi3_charge   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_charge", Reco_Pi3_charge.get() );
    Reco_K_pT        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_pT",  Reco_K_pT.get() );
    Reco_K_eta       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_eta",  Reco_K_eta.get() );
    Reco_K_phi       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_phi",  Reco_K_phi.get() );
    Reco_K_charge    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_charge", Reco_K_charge.get() );

    isMatch_Pass          = std::make_unique<std::vector<Bool_t>> ();  Truth_B_tree->Branch("isMatch_Pass", isMatch_Pass.get() );
    Min_d_Pass            = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_d_Pass", Min_d_Pass.get() );
    Min_dx_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dx_Pass", Min_dx_Pass.get() );
    Min_dy_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dy_Pass", Min_dy_Pass.get() );
    Min_dz_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Min_dz_Pass", Min_dz_Pass.get() );
    Reco_x_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_x_Pass",   Reco_x_Pass.get() );
    Reco_y_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_y_Pass",   Reco_y_Pass.get() );
    Reco_z_Pass           = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_z_Pass",   Reco_z_Pass.get() );
    Reco_eta_Pass         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_eta_Pass", Reco_eta_Pass.get() );
    Reco_phi_Pass         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_phi_Pass", Reco_phi_Pass.get() );
    Reco_DecayType_Pass   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_DecayType_Pass", Reco_DecayType_Pass.get() );
    Reco_B_m_Pass         = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_mass_Pass",  Reco_B_m_Pass.get() );
    Reco_B_eta_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_eta_Pass",  Reco_B_eta_Pass.get() );
    Reco_B_phi_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_phi_Pass",  Reco_B_phi_Pass.get() );
    Reco_B_pT_Pass        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_B_pT_Pass",  Reco_B_pT_Pass.get() );
    Reco_B_flavor_Pass    = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_B_flavor_Pass", Reco_B_flavor_Pass.get() );
    Reco_Dstar_m_Pass        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_mass_Pass",  Reco_Dstar_m_Pass.get() );
    Reco_Dstar_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_eta_Pass",  Reco_Dstar_eta_Pass.get() );
    Reco_Dstar_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_phi_Pass",  Reco_Dstar_phi_Pass.get() );
    Reco_Dstar_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Dstar_pT_Pass",  Reco_Dstar_pT_Pass.get() );
    Reco_Dstar_charge_Pass   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_Dstar_charge_Pass", Reco_Dstar_charge_Pass.get() );
    Reco_D0_m_Pass        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_mass_Pass",  Reco_D0_m_Pass.get() );
    Reco_D0_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_eta_Pass",  Reco_D0_eta_Pass.get() );
    Reco_D0_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_phi_Pass",  Reco_D0_phi_Pass.get() );
    Reco_D0_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_D0_pT_Pass",  Reco_D0_pT_Pass.get() );
    Reco_D0_charge_Pass   = std::make_unique<std::vector<Int_t>> ();   Truth_B_tree->Branch("Reco_D0_charge_Pass", Reco_D0_charge_Pass.get() );
    Reco_Mu_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_eta_Pass", Reco_Mu_eta_Pass.get() );
    Reco_Mu_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_phi_Pass", Reco_Mu_phi_Pass.get() );
    Reco_Mu_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_pT_Pass",  Reco_Mu_pT_Pass.get() );
    Reco_Mu_charge_Pass   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Mu_charge_Pass", Reco_Mu_charge_Pass.get() );
    Reco_Pi_s_pT_Pass     = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_pT_Pass",  Reco_Pi_s_pT_Pass.get() );
    Reco_Pi_s_eta_Pass    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_eta_Pass",  Reco_Pi_s_eta_Pass.get() );
    Reco_Pi_s_phi_Pass    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_phi_Pass",  Reco_Pi_s_phi_Pass.get() );
    Reco_Pi_s_charge_Pass = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi_s_charge_Pass", Reco_Pi_s_charge_Pass.get() );
    Reco_Pi1_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_pT_Pass",  Reco_Pi1_pT_Pass.get() );
    Reco_Pi1_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_eta_Pass",  Reco_Pi1_eta_Pass.get() );
    Reco_Pi1_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_phi_Pass",  Reco_Pi1_phi_Pass.get() );
    Reco_Pi1_charge_Pass   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi1_charge_Pass", Reco_Pi1_charge_Pass.get() );
    Reco_Pi2_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_pT_Pass",  Reco_Pi2_pT_Pass.get() );
    Reco_Pi2_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_eta_Pass",  Reco_Pi2_eta_Pass.get() );
    Reco_Pi2_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_phi_Pass",  Reco_Pi2_phi_Pass.get() );
    Reco_Pi2_charge_Pass   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi2_charge_Pass", Reco_Pi2_charge_Pass.get() );
    Reco_Pi3_pT_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_pT_Pass",  Reco_Pi3_pT_Pass.get() );
    Reco_Pi3_eta_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_eta_Pass",  Reco_Pi3_eta_Pass.get() );
    Reco_Pi3_phi_Pass      = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_phi_Pass",  Reco_Pi3_phi_Pass.get() );
    Reco_Pi3_charge_Pass   = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_Pi3_charge_Pass", Reco_Pi3_charge_Pass.get() );
    Reco_K_pT_Pass        = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_pT_Pass",  Reco_K_pT_Pass.get() );
    Reco_K_eta_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_eta_Pass",  Reco_K_eta_Pass.get() );
    Reco_K_phi_Pass       = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_phi_Pass",  Reco_K_phi_Pass.get() );
    Reco_K_charge_Pass    = std::make_unique<std::vector<Float_t>> (); Truth_B_tree->Branch("Reco_K_charge_Pass", Reco_K_charge_Pass.get() );


    MET_tree->Branch("n_MET",              m_n_MET.get()  );
    MET_tree->Branch("n_MET_Truth",        m_n_MET_Truth.get()  );
    m_MET_px   = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("MET_px",  m_MET_px.get() );
    m_MET_py   = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("MET_py",  m_MET_py.get() );
    m_MET_sum  = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("MET_sum", m_MET_sum.get() );

    m_Rec_MET_px   = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("Rec_MET_px",  m_Rec_MET_px.get() );
    m_Rec_MET_py   = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("Rec_MET_py",  m_Rec_MET_py.get() );
    m_Rec_MET_sum  = std::make_unique<std::vector<Float_t>> (); MET_tree->Branch("Rec_MET_sum", m_Rec_MET_sum.get() );
     
    m_Rec_MET_Mu_px = std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Rec_MET_Mu_px", m_Rec_MET_Mu_px.get() );
    m_Rec_MET_Mu_py = std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Rec_MET_Mu_py", m_Rec_MET_Mu_py.get() );
    m_Rec_MET_Mu_sum= std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Rec_MET_Mu_sum",m_Rec_MET_Mu_sum.get() );
    m_Track_MET_px   = std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Track_MET_px",  m_Track_MET_px.get() );
    m_Track_MET_py   = std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Track_MET_py",  m_Track_MET_py.get() );
    m_Track_MET_sum  = std::make_unique<std::vector<Float_t>>();MET_tree->Branch("Track_MET_sum", m_Track_MET_sum.get() );
     
    Truth_MET_tree->Branch("n_MET",              m_n_MET.get()  );
    Truth_MET_tree->Branch("n_MET_Truth",        m_n_MET_Truth.get()  );
    m_Truth_MET_px   = std::make_unique<std::vector<Float_t>> (); Truth_MET_tree->Branch("MET_px",  m_Truth_MET_px.get() );
    m_Truth_MET_py   = std::make_unique<std::vector<Float_t>> (); Truth_MET_tree->Branch("MET_py",  m_Truth_MET_py.get() );
    m_Truth_MET_sum  = std::make_unique<std::vector<Float_t>> (); Truth_MET_tree->Branch("MET_sum", m_Truth_MET_sum.get() );

    // trig decision tool
    m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
    CHECK (m_tdt.initialize());
    
    return StatusCode::SUCCESS;
  }


  StatusCode Initialize_variable()
  {
    m_isMatch->clear();
    m_isMatch_Pass->clear();
    m_isPassed->clear();
    m_isPassedTight->clear();
    m_TriggerType->clear();

    m_Track_d0->clear();
    m_DecayType->clear();
    m_BDT_score->clear();
    m_B_flavor->clear();
    m_B_nPart->clear();
    m_B_mass->clear();
    m_B_mass_err->clear();
    m_B_eta->clear();
    m_B_phi->clear();
    m_B_Px->clear();
    m_B_Py->clear();
    m_B_Pz->clear();
    m_B_E->clear();
    m_B_Pt->clear();
    m_B_Chi2->clear();
    m_B_nDoF->clear();
    m_B_x->clear();
    m_B_y->clear();
    m_B_z->clear();
    m_PiD0_mass->clear();
    m_Visible_mass->clear();
    m_Visible_pT->clear();
    m_Visible_eta->clear();
    m_Visible_phi->clear();

    m_D0Pi_mass->clear();
    m_D0Pi_pT->clear();
    m_D0Pi_eta->clear();
    m_D0Pi_phi->clear();

    m_n_PV->clear();
    m_n_RPV->clear();

    m_PV_x->clear();
    m_PV_y->clear();
    m_PV_z->clear();
    m_RPV_x->clear();
    m_RPV_y->clear();
    m_RPV_z->clear();

    m_LxyMaxSumPt2->clear();
    m_LxyErrMaxSumPt2->clear();
    m_A0MaxSumPt2->clear();
    m_A0ErrMaxSumPt2->clear();
    m_A0xyMaxSumPt2->clear();
    m_A0xyErrMaxSumPt2->clear();
    m_Z0MaxSumPt2->clear();
    m_Z0ErrMaxSumPt2->clear();
    m_TauInvMassPVMaxSumPt2->clear();
    m_TauErrInvMassPVMaxSumPt2->clear();
    m_TauConstMassPVMaxSumPt2->clear();
    m_TauErrConstMassPVMaxSumPt2->clear();

    m_n_MuPi->clear();
    m_MuPi_mass->clear();
    m_MuPi_pT->clear();
    m_MuPi_eta->clear();
    m_MuPi_phi->clear();
    m_MuPi_chi2->clear();
    m_MuPi_nDoF->clear();
    m_MuPiAft_mass->clear();
    m_MuPiLxyMaxSumPt2->clear();

    m_Pi_s_m->clear();
    m_Pi_s_pT->clear();
    m_Pi_s_charge->clear();
    m_Pi_s_eta->clear();
    m_Pi_s_phi->clear();
    m_Pi_s_theta_star->clear();
    m_Pi_s_d0->clear();
    m_Pi_s_z0->clear();
    m_Pi_s_qOverP->clear();
    m_n_Mu->clear();
    m_Mu_quality->clear();
    m_Mu_PassIso->clear();
    m_Mu_m->clear();
    m_Mu_pT->clear();
    m_Mu_charge->clear();
    m_Mu_eta->clear();
    m_Mu_phi->clear();
    m_Mu_d0->clear();
    m_Mu_z0->clear();
    m_Mu_qOverP->clear();
    m_Mu_chi2->clear();
    m_Mu_nDoF->clear();
    m_Mu_chi2_B->clear();
    m_Mu_nDoF_B->clear();

    m_D0_mass->clear();
    m_D0_massErr->clear();
    m_D0_Pt->clear();
    m_D0_PtErr->clear();
    m_D0_Lxy->clear();
    m_D0_LxyErr->clear();
    m_D0_Tau->clear();
    m_D0_TauErr->clear();
    m_D0_chi2->clear();
    m_D0_nDoF->clear();
    m_D0_flag->clear();
    m_Kpi_mass->clear();
    m_Kpi_pT->clear();
    m_Kpi_eta->clear();
    m_Kpi_phi->clear();

    m_Pi1_m->clear();
    m_Pi1_pT->clear();
    m_Pi1_charge->clear();
    m_Pi1_eta->clear();
    m_Pi1_phi->clear();
    m_Pi1_theta_star->clear();
    m_Pi1_d0->clear();
    m_Pi1_z0->clear();
    m_Pi1_qOverP->clear();
    m_Pi2_m->clear();
    m_Pi2_pT->clear();
    m_Pi2_charge->clear();
    m_Pi2_eta->clear();
    m_Pi2_phi->clear();
    m_Pi2_theta_star->clear();
    m_Pi2_d0->clear();
    m_Pi2_z0->clear();
    m_Pi2_qOverP->clear();
    m_Pi3_m->clear();
    m_Pi3_pT->clear();
    m_Pi3_charge->clear();
    m_Pi3_eta->clear();
    m_Pi3_phi->clear();
    m_Pi3_theta_star->clear();
    m_Pi3_d0->clear();
    m_Pi3_z0->clear();
    m_Pi3_qOverP->clear();
    m_K_m->clear();
    m_K_pT->clear();
    m_K_charge->clear();
    m_K_eta->clear();
    m_K_phi->clear();
    m_K_theta_star->clear();
    m_K_d0->clear();
    m_K_z0->clear();
    m_K_qOverP->clear();

    Truth_isHighPt->clear();
    Truth_Flavor->clear();
    Truth_DecayTime->clear();
    Truth_DeltaT->clear();
    Truth_hasMuon->clear();
    Truth_DecayType->clear();
    Truth_pdgID->clear();
    Truth_parent_num->clear();
    Truth_parent_pdgID->clear();
    Truth_trk_num->clear();
    Truth_trk_pdgID->clear();
    Truth_child_num->clear();
    Truth_child_pdgID->clear();
    Truth_x->clear();
    Truth_y->clear();
    Truth_z->clear();
    Truth_eta->clear();
    Truth_phi->clear();
    Truth_B_eta->clear();
    Truth_B_phi->clear();
    Truth_B_pT->clear();
    Truth_Dstar_eta->clear();
    Truth_Dstar_phi->clear();
    Truth_Dstar_pT->clear();
    Truth_Dstar_charge->clear();
    Truth_D0_mass->clear();
    Truth_D0_eta->clear();
    Truth_D0_phi->clear();
    Truth_D0_pT->clear();
    Truth_Mu_eta->clear();
    Truth_Mu_phi->clear();
    Truth_Mu_pT->clear();
    Truth_Mu_charge->clear();
    Truth_Nu_eta->clear();
    Truth_Nu_phi->clear();
    Truth_Nu_pT->clear();
    Truth_Pi_s_eta->clear();
    Truth_Pi_s_phi->clear();
    Truth_Pi_s_pT->clear();
    Truth_Pi_s_mass->clear();
    Truth_Pi_s_charge->clear();
    Truth_Pi_s_pdgID->clear();
    Truth_D0_child_num->clear();
    Truth_D0_charged_child_num->clear();
    Truth_D0_reco_mass->clear();
    Truth_D0_reco_pT->clear();
    Truth_D0_child_eta->clear();
    Truth_D0_child_phi->clear();
    Truth_D0_child_pT->clear();
    Truth_D0_child_mass->clear();
    Truth_D0_child_charge->clear();
    Truth_D0_child_pdgID->clear();

    isMatch->clear();
    Min_d->clear();
    Min_dx->clear();
    Min_dy->clear();
    Min_dz->clear();
    Reco_x->clear();
    Reco_y->clear();
    Reco_z->clear();
    Reco_eta->clear();
    Reco_phi->clear();
    Reco_isPassed->clear();
    Reco_DecayType->clear();
    Reco_B_m->clear();
    Reco_B_eta->clear();
    Reco_B_phi->clear();
    Reco_B_pT->clear();
    Reco_B_flavor->clear();
    Reco_Dstar_m->clear();
    Reco_Dstar_eta->clear();
    Reco_Dstar_phi->clear();
    Reco_Dstar_pT->clear();
    Reco_Dstar_charge->clear();
    Reco_D0_m->clear();
    Reco_D0_eta->clear();
    Reco_D0_phi->clear();
    Reco_D0_pT->clear();
    Reco_Mu_eta->clear();
    Reco_Mu_phi->clear();
    Reco_Mu_pT->clear();
    Reco_Mu_charge->clear();
    Reco_Pi_s_eta->clear();
    Reco_Pi_s_phi->clear();
    Reco_Pi_s_pT->clear();
    Reco_Pi_s_charge->clear();
    Reco_Pi1_eta->clear();
    Reco_Pi1_phi->clear();
    Reco_Pi1_pT->clear();
    Reco_Pi1_charge->clear();
    Reco_Pi2_eta->clear();
    Reco_Pi2_phi->clear();
    Reco_Pi2_pT->clear();
    Reco_Pi2_charge->clear();
    Reco_Pi3_eta->clear();
    Reco_Pi3_phi->clear();
    Reco_Pi3_pT->clear();
    Reco_Pi3_charge->clear();
    Reco_K_eta->clear();
    Reco_K_phi->clear();
    Reco_K_pT->clear();
    Reco_K_charge->clear();

    isMatch_Pass->clear();
    Min_d_Pass->clear();
    Min_dx_Pass->clear();
    Min_dy_Pass->clear();
    Min_dz_Pass->clear();
    Reco_x_Pass->clear();
    Reco_y_Pass->clear();
    Reco_z_Pass->clear();
    Reco_eta_Pass->clear();
    Reco_phi_Pass->clear();
    Reco_DecayType_Pass->clear();
    Reco_B_m_Pass->clear();
    Reco_B_eta_Pass->clear();
    Reco_B_phi_Pass->clear();
    Reco_B_pT_Pass->clear();
    Reco_B_flavor_Pass->clear();
    Reco_Dstar_m_Pass->clear();
    Reco_Dstar_eta_Pass->clear();
    Reco_Dstar_phi_Pass->clear();
    Reco_Dstar_pT_Pass->clear();
    Reco_Dstar_charge_Pass->clear();
    Reco_D0_m_Pass->clear();
    Reco_D0_eta_Pass->clear();
    Reco_D0_phi_Pass->clear();
    Reco_D0_pT_Pass->clear();
    Reco_D0_charge_Pass->clear();
    Reco_Mu_eta_Pass->clear();
    Reco_Mu_phi_Pass->clear();
    Reco_Mu_pT_Pass->clear();
    Reco_Mu_charge_Pass->clear();
    Reco_Pi_s_eta_Pass->clear();
    Reco_Pi_s_phi_Pass->clear();
    Reco_Pi_s_pT_Pass->clear();
    Reco_Pi_s_charge_Pass->clear();
    Reco_Pi1_eta_Pass->clear();
    Reco_Pi1_phi_Pass->clear();
    Reco_Pi1_pT_Pass->clear();
    Reco_Pi1_charge_Pass->clear();
    Reco_Pi2_eta_Pass->clear();
    Reco_Pi2_phi_Pass->clear();
    Reco_Pi2_pT_Pass->clear();
    Reco_Pi2_charge_Pass->clear();
    Reco_Pi3_eta_Pass->clear();
    Reco_Pi3_phi_Pass->clear();
    Reco_Pi3_pT_Pass->clear();
    Reco_Pi3_charge_Pass->clear();
    Reco_K_eta_Pass->clear();
    Reco_K_phi_Pass->clear();
    Reco_K_pT_Pass->clear();
    Reco_K_charge_Pass->clear();

    m_MET_px->clear();
    m_MET_py->clear();
    m_MET_sum->clear();
    m_Rec_MET_px->clear();
    m_Rec_MET_py->clear();
    m_Rec_MET_sum->clear();
    m_Rec_MET_Mu_px->clear();
    m_Rec_MET_Mu_py->clear();
    m_Rec_MET_Mu_sum->clear();
    m_Track_MET_px->clear();
    m_Track_MET_py->clear();
    m_Track_MET_sum->clear();
    m_Truth_MET_px->clear();
    m_Truth_MET_py->clear();
    m_Truth_MET_sum->clear();
     
    return StatusCode::SUCCESS;
  }


};

#endif //> !MYPACKAGE_V2_MYPACKAGE_V2ALG_H
