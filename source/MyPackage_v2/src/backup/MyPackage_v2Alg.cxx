// MyPackage_v2 includes
#include "MyPackage_v2Alg.h"

#include <vector>
#include <string>

#include "TTree.h"
#include "TString.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODBPhys/BPhysHypoHelper.h"
#include "xAODBPhys/BPhysHelper.h"
#include "xAODMuon/MuonContainer.h"
#include "PATInterfaces/CorrectionCode.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "HepPDT/ParticleDataTable.hh"
//#include "xAODTrigBphys/TrigBphysContainer.h"
#include "TSystem.h"
#include <TH1.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>
#include <functional>
#include <unordered_set>
#include <algorithm>


#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"


typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;

typedef ElementLink<xAOD::MuonContainer> MuonLink;
typedef std::vector<MuonLink> MuonLinkVector;

typedef TLorentzVector tlv;

MyPackage_v2Alg::MyPackage_v2Alg( const std::string& name, ISvcLocator* pSvcLocator ) :
  AthAnalysisAlgorithm( name, pSvcLocator ),
  m_muonSelection ("CP::MuonSelectionTool", this),
  m_muonCalibrationAndSmearingTool ("CP::MuonCalibrationAndSmearingTool/MuonCorrectionTool", this),
  m_isoTool ("CP::IsolationSelectionTool/IsolationSelectionTool"),
  m_jetCalibration (""),
  //m_jetCalibration ("JetCalibrationTool/JetCalibrationTool"),
  m_jetCleaningTool ("JetCleaningTool/JetCleaningTool"),
  //m_jetFwdJvtTool (""),
  m_jetFwdJvtTool ("JetForwardJvtTool/FJVTTool"),
  m_jetJvtUpdateTool ("JetVertexTaggerTool/JetVertexTaggerTool"),
  m_EgammaCalibrationAndSmearingTool ("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
  m_metMaker ("met::METMaker/METMaker"),
  m_grl ("GoodRunsListSelectionTool/grl", this)
{
  declareProperty("IsMC",  m_is_mc = true);
  m_jetCalibration.declarePropertyFor( this, "JetCalibTool", "The JetCalibTool" );
  //m_jetFwdJvtTool.declarePropertyFor( this, "JetFwdJvtTool", "The JetFwdJvtTool" );
}


MyPackage_v2Alg::~MyPackage_v2Alg() {}


StatusCode MyPackage_v2Alg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}


StatusCode MyPackage_v2Alg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  //setFilterPassed(false); //optional: start with algorithm not passed

  //Initialize variable
  Initialize_variable();

  ATH_MSG_INFO("Retrieving EventInfo");
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) ); //retrieves the event info

  Int_t en = ei->eventNumber();
  ATH_MSG_INFO("eventNumber = " << en ); //example printout of the event number
  
  // truth MC container
  const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  if(m_is_mc){
    CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));
  }

  const xAOD::MissingETContainer* MET = 0;
  CHECK( evtStore()->retrieve( MET , "MET_Core_AntiKt4EMPFlow" ) );

  const xAOD::MissingETAssociationMap* MET_map = 0;
  CHECK( evtStore()->retrieve( MET_map , "METAssoc_AntiKt4EMPFlow" ) );

  const xAOD::MissingETContainer* Truth_MET = 0;
  if(m_is_mc)
    CHECK( evtStore()->retrieve( Truth_MET , "MET_Truth" ) );

  const xAOD::VertexContainer* BMuDpst = 0;
  CHECK( evtStore()->retrieve( BMuDpst , "BMuDpstCascadeSV1" ) );

  const xAOD::VertexContainer* BVertex = 0;
  CHECK( evtStore()->retrieve( BVertex , "BMuDpstCascadeSV2" ) );

  const xAOD::VertexContainer* pvc = 0;
  CHECK( evtStore()->retrieve( pvc , "PrimaryVertices" ) );

  const xAOD::VertexContainer* MuPi_vtx = 0;
  const xAOD::MuonContainer* Mu = 0;
  const xAOD::VertexContainer* PV_vtx = 0;
  const xAOD::VertexContainer* RPV_vtx = 0;

  // Trigger
  bool TriggerPass1=false;
  bool TriggerPass2=false;
  bool TriggerPass3=false;
  bool TriggerPass=false;
  *m_TriggerPass=false;
  *m_TriggerType=0;
  for( const auto& trigChainName : triggerNames ){
    auto cg = this->m_tdt->getChainGroup( trigChainName.Data() );
    bool isPassed = cg->isPassed(); 
    // Only save triggers that passed
    if( !isPassed ) continue;
    else {
      *m_TriggerPass=true;
      TriggerPass=true;
    }
  }

  auto HLT_mu_cg = m_tdt->getChainGroup("HLT_mu.*");
  for(auto &HLT_trig : HLT_mu_cg->getListOfTriggers()) {
    auto HLT = m_tdt->getChainGroup(HLT_trig);
    if ( HLT->isPassed() ) TriggerPass1=true;
  };
  auto HLT_multi_mu_cg = m_tdt->getChainGroup("HLT_[3-9]mu.*");
  for(auto &HLT_trig : HLT_multi_mu_cg->getListOfTriggers()) {
    auto HLT = m_tdt->getChainGroup(HLT_trig);
    if ( HLT->isPassed() ) TriggerPass3=true;
  };
  auto HLT_2mu_cg = m_tdt->getChainGroup("HLT_2mu.*");
  for(auto &HLT_trig : HLT_2mu_cg->getListOfTriggers()) {
    auto HLT = m_tdt->getChainGroup(HLT_trig);
    if ( HLT->isPassed() ) TriggerPass2=true;
  };
  /*
  auto HLT_2mu4_cg = m_tdt->getChainGroup("HLT_2mu4");
  for(auto &HLT_trig : HLT_2mu4_cg->getListOfTriggers()) {
    auto HLT = m_tdt->getChainGroup(HLT_trig);
    if ( HLT->isPassed() ) *m_TriggerPass=true;
  };
  */
  if(TriggerPass1) *m_TriggerType+=1;
  if(TriggerPass2) *m_TriggerType+=2;
  if(TriggerPass3) *m_TriggerType+=4;
  if(TriggerPass) *m_TriggerType+=8;
  
  //GRL filtering
  bool passGRL = true;
  if (!m_is_mc) {
    if (!m_grl->passRunLB(*ei)) {
      passGRL = false;
    }
  } // end if the event is data
  *m_PassGRL = passGRL;
  
  //Track number
  const xAOD::TrackParticleContainer* ID_track = 0;
  CHECK( evtStore()->retrieve( ID_track , "InDetTrackParticles" ) );
  *m_n_Track = std::distance(ID_track->begin(), ID_track->end());
  
  // Truth Vtx Matching
  std::map<Int_t, Bool_t> recoVtx_isUsed_BMuDpst;
  std::map<Int_t, Bool_t> recoVtx_isPassed_BMuDpst;
  std::map<Int_t, Bool_t> recoVtx_isUsed_BMuDpst_Pass;

  struct isBmeson {
    Bool_t operator()(const xAOD::TruthParticle* p) {
      if( !( (fabs(p->pdgId()) == 511) && p->hasDecayVtx()) )  return false;
      if ( p->decayVtx()->nOutgoingParticles() < 3 ) return false;
      return true;
    }
  };

  std::function<Bool_t(const xAOD::TruthParticle*)> matchfunc = isBmeson();

  Int_t n_B=0;
  Int_t n_B_Pass=0;
  Int_t n_B_Match=0;
  Int_t n_B_Match_Pass=0;
  Int_t n_B_Truth=0;
  Int_t n_Dstar=0;
  Int_t n_Dstar_Pass=0;
  Int_t n_Dstar_Pass_Tight=0;
  Int_t n_Dstar_Match=0;
  Int_t n_Dstar_Match_Pass=0;
  Int_t n_Dstar_Truth=0;
  Int_t n_Dstar_Truth_HighPt=0;
  Int_t n_B_Dstar_Truth=0;
  Int_t n_B_Dstar_D0Pi_Truth=0;
  Int_t n_B_D_Truth=0;
  Int_t n_B_D_s_Truth=0;
  Int_t n_B_Lambda_c_Truth=0;
  Int_t n_Mu_Truth=0;
  Int_t n_MET=0;
  Int_t n_MET_Truth=0;

  xAOD::VertexContainer::const_iterator vtxItr;


  //MET information
  Float_t MET_px=0;
  Float_t MET_py=0;
  Float_t MET_sum=0;

  Float_t Rec_MET_Mu_px=0;
  Float_t Rec_MET_Mu_py=0;
  Float_t Rec_MET_Mu_sum=0;

  Float_t Rec_MET_px=0;
  Float_t Rec_MET_py=0;
  Float_t Rec_MET_sum=0;

  Float_t Track_MET_px=0;
  Float_t Track_MET_py=0;
  Float_t Track_MET_sum=0;

  Float_t Truth_MET_px=0;
  Float_t Truth_MET_py=0;
  Float_t Truth_MET_sum=0;

  for (auto itr=MET->begin(); itr!=MET->end(); ++itr) {
    n_MET++;
    xAOD::MissingET* MissingET = *itr;
    m_MET_px->push_back( MissingET->mpx() );
    m_MET_py->push_back( MissingET->mpy() );
    m_MET_sum->push_back( MissingET->sumet() );
    if(MissingET->name() == "PVSoftTrkCore"){
      MET_px=MissingET->mpx();
      MET_py=MissingET->mpy();
      MET_sum=MissingET->sumet();
    }
  }

  //Track MET
  TLorentzVector pMis_recoTracks;pMis_recoTracks.SetXYZT(0.,0.,0.,0.);
  for(auto track : *ID_track)
    pMis_recoTracks+=TLorentzVector(track->pt()*cos(track->phi()), track->pt()*sin(track->phi()), 0., track->pt());

  //MET calibration
  std::string m_eleTerm = "RefEle";
  std::string m_gammaTerm = "RefGamma";
  std::string m_tauTerm = "RefTau";
  std::string m_jetTerm = "RefJet";
  std::string m_muonTerm = "Muons";
  std::string softTerm = "PVSoftTrk";
  //std::string softTerm = "SoftClus";
  std::string m_inputMETMap = "METAssoc_AntiKt4EMPFlow";
  std::string m_inputMETCore = "MET_Core_AntiKt4EMPFlow";
  std::string m_outMETTerm = "Track";
  //std::string m_outMETTerm = "Final";
  xAOD::MissingETContainer* met = new xAOD::MissingETContainer;
  xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
  met->setStore(met_aux);
  met->reserve(10);
  xAOD::MissingETContainer* met_mu = new xAOD::MissingETContainer;
  xAOD::MissingETAuxContainer* met_mu_aux = new xAOD::MissingETAuxContainer;
  met_mu->setStore(met_mu_aux);
  met_mu->reserve(10);
  const xAOD::MissingETContainer* metcore(0);
  CHECK( evtStore()->retrieve( metcore, m_inputMETCore ) );
  const xAOD::MissingETAssociationMap* metMap(0);
  CHECK ( evtStore()->retrieve(metMap, m_inputMETMap) );  metMap->resetObjSelectionFlags();

  //Muon part
  const xAOD::MuonContainer* muons = 0;
  CHECK (evtStore()->retrieve(muons, "Muons"));
  if (muons) {
    auto muons_shallowCopy = xAOD::shallowCopyContainer( *muons );
    std::unique_ptr<xAOD::MuonContainer> muonsSC (muons_shallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC (muons_shallowCopy.second);
    bool setLinksMuon = xAOD::setOriginalObjectLink(*muons,*muonsSC);
    if(!setLinksMuon) {
      ATH_MSG_ERROR("execute(): Failed to set original electron links.");
      return StatusCode::FAILURE;
    }
    for (auto mu : *muonsSC) {
      if (!m_muonSelection->accept(*mu)) continue;
      if(m_muonCalibrationAndSmearingTool->applyCorrection(*mu) != CP::CorrectionCode::Ok)
	ATH_MSG_INFO ("execute(): Problem with Muon Calibration And Smearing Tool (Error or OutOfValidityRange) ");
    }
    ATH_MSG_VERBOSE("Build muon MET");
    ConstDataVector<xAOD::MuonContainer> metmuon(SG::VIEW_ELEMENTS);
    for (const xAOD::Muon* mu : *muonsSC) {
      // pass baseline selection
      if (!m_muonSelection->accept(*mu)) continue;
      //if ((float)mu->isolation(xAOD::Iso::ptvarcone30) / mu->pt()<0.06)
	metmuon.push_back(mu);
      //metmuon.push_back(mu);
    }
    ATH_CHECK( m_metMaker->rebuildMET(m_muonTerm, xAOD::Type::Muon, met_mu, metmuon.asDataVector(), metMap) );
    ATH_CHECK( m_metMaker->rebuildMET(m_muonTerm, xAOD::Type::Muon, met, metmuon.asDataVector(), metMap) );
  }

  //Jet part
  // get jet container of interest 
  const xAOD::JetContainer* jets = 0;
  CHECK(evtStore()->retrieve( jets, "AntiKt4EMPFlowJets" ));
  if (jets){
    auto jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
    std::unique_ptr<xAOD::JetContainer> jetsSC (jets_shallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> jetsAuxSC (jets_shallowCopy.second);
    bool setLinksJet = xAOD::setOriginalObjectLink(*jets,*jetsSC);
    if(!setLinksJet) {
      ATH_MSG_ERROR("execute(): Failed to set original electron links.");
      return StatusCode::FAILURE;
    }
    //for (auto jetSC : *jetsSC) {
    //m_jetCalibration->applyCalibration(*jetSC);
    //float jvt = m_jetJvtUpdateTool->updateJvt(*jetSC);
    //} // end for loop over jets  
    //m_jetFwdJvtTool->modify(*jetsSC);
    
    ConstDataVector<xAOD::JetContainer> metjets(SG::VIEW_ELEMENTS);
    //for (const xAOD::Jet* jet : *jetsSC) {
      //if (!m_jetCleaningTool->keep(*jet)) continue;
      //metjets.push_back(jet);
    //}
    ATH_CHECK( m_metMaker->rebuildTrackMET(m_jetTerm, softTerm, met, metjets.asDataVector(), metcore, metMap, false) );
  }

  const xAOD::MissingET *coreSoftTrk(0);
  xAOD::MissingET *metSoftTrk(0);
  
  const xAOD::MissingET* coreSoft = (*metcore)[softTerm+"Core"];
  coreSoftTrk = coreSoft;
  
  metSoftTrk = nullptr;
  ATH_CHECK( met::fillMET(metSoftTrk , met_mu, softTerm  , coreSoftTrk->source()) );

  ATH_MSG_VERBOSE("Build MET sum");
  ATH_CHECK( m_metMaker->buildMETSum(m_outMETTerm, met_mu, (*met)[softTerm]->source()) );
  ATH_CHECK( m_metMaker->buildMETSum(m_outMETTerm, met, (*met)[softTerm]->source()) );
  ATH_MSG_VERBOSE( "Done rebuilding MET." );

  m_Rec_MET_px->push_back( (*met)[m_outMETTerm]->mpx() );
  m_Rec_MET_py->push_back( (*met)[m_outMETTerm]->mpy() );
  m_Rec_MET_sum->push_back( (*met)[m_outMETTerm]->sumet() );

  Rec_MET_px=(*met)[m_outMETTerm]->mpx();
  Rec_MET_py=(*met)[m_outMETTerm]->mpy();
  Rec_MET_sum=(*met)[m_outMETTerm]->sumet();

  m_Rec_MET_Mu_px->push_back( (*met_mu)[m_outMETTerm]->mpx() );
  m_Rec_MET_Mu_py->push_back( (*met_mu)[m_outMETTerm]->mpy() );
  m_Rec_MET_Mu_sum->push_back( (*met_mu)[m_outMETTerm]->sumet() );

  Rec_MET_Mu_px=(*met_mu)[m_outMETTerm]->mpx();
  Rec_MET_Mu_py=(*met_mu)[m_outMETTerm]->mpy();
  Rec_MET_Mu_sum=(*met_mu)[m_outMETTerm]->sumet();

  delete met;
  delete met_aux;
  delete met_mu;
  delete met_mu_aux;

  m_Track_MET_px->push_back( pMis_recoTracks.Px() );
  m_Track_MET_py->push_back( pMis_recoTracks.Py() );
  m_Track_MET_sum->push_back( pMis_recoTracks.Pt() );

  Track_MET_px=pMis_recoTracks.Px();
  Track_MET_py=pMis_recoTracks.Py();
  Track_MET_sum=pMis_recoTracks.Pt();

  // Matching index container
  for (vtxItr=BMuDpst->begin(); vtxItr!=BMuDpst->end(); ++vtxItr) {
    recoVtx_isUsed_BMuDpst.emplace( std::distance(BMuDpst->begin(), vtxItr), false );
    recoVtx_isPassed_BMuDpst.emplace( std::distance(BMuDpst->begin(), vtxItr), false );
    recoVtx_isUsed_BMuDpst_Pass.emplace( std::distance(BMuDpst->begin(), vtxItr), false );
  }
  
  if(m_is_mc){

    ATH_MSG_INFO("MyPackage_v2Alg::getTruthVtxMatch > ");
    xAOD::TruthEventContainer::const_iterator itr;

    //TruthMET information
    for (auto itr=Truth_MET->begin(); itr!=Truth_MET->end(); ++itr) {
      n_MET_Truth++;
      xAOD::MissingET* Truth_MissingET = *itr;
      m_Truth_MET_px->push_back( Truth_MissingET->mpx() );
      m_Truth_MET_py->push_back( Truth_MissingET->mpy() );
      m_Truth_MET_sum->push_back( Truth_MissingET->sumet() );
      if(Truth_MissingET->name() == "NonInt"){
	Truth_MET_px=Truth_MissingET->mpx();
	Truth_MET_py=Truth_MissingET->mpy();
	Truth_MET_sum=Truth_MissingET->sumet();
      }
    }  

    for (auto itr=xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
      size_t nPart = (*itr)->truthParticleLinks().size();
      //Truth B meson information
      for (size_t iPart = 0; iPart < nPart; iPart++) {
	const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart);
	if (particle == 0) continue;
	
	if(std::fabs(particle->pdgId())==13) n_Mu_Truth++;
	// Check if the particle is a signal
	if ( !matchfunc(particle) ) continue;
	if ( !particle->hasDecayVtx() ) continue;

	n_B_Truth++;

	Float_t TruthDecayTime=0;
	Float_t TruthDeltaT=0;
	if(particle->hasProdVtx() && particle->hasDecayVtx()){
	  const xAOD::TruthVertex* StartVtx = particle->prodVtx();
	  const xAOD::TruthVertex* EndVtx = particle->decayVtx();
	  TVector3 StartPoint;
	  StartPoint.SetXYZ( StartVtx->x(), StartVtx->y(), StartVtx->z() );
	  TVector3 EndPoint;
	  EndPoint.SetXYZ( EndVtx->x(), EndVtx->y(), EndVtx->z() );
	  TVector3 TruthVector = EndPoint-StartPoint;
	  TruthDecayTime = (particle->px()*TruthVector.x()+particle->py()*TruthVector.y())/particle->pt();
	  TruthDeltaT = EndVtx->t()-StartVtx->t();
	}

	// children
	Bool_t flag_Dstar=false;
	Bool_t flag_Mu=false;
	Bool_t flag_Nu=false;
	Bool_t flag_D0=false;
	Bool_t flag_PiS=false;
	Bool_t flag_D0_mass=false;

	Int_t num_D0=0;
	Int_t num_isCharged=0;

	Bool_t flag_isHighPt_Mu=false;
	Bool_t flag_isHighPt_Pi_s=false;
	Bool_t flag_isHighPt_Dstar=false;
	Bool_t flag_isHighPt_D0=false;
	Bool_t flag_isHighPt_D0_child=false;

	//Bool_t isHighPt=false;

	std::vector<Double_t>  D0_child_mass;
	std::vector<Double_t>  D0_child_pT;
	std::vector<Double_t>  D0_child_eta;
	std::vector<Double_t>  D0_child_phi;
	std::vector<Double_t>  D0_child_charge;
	std::vector<Double_t>  D0_child_pdgID;
	std::vector<TLorentzVector>  p4_D0_child;
	TLorentzVector p4_D0_reco, D0_child_vec;
	Double_t D0_reco_mass = -9999;
	Double_t D0_reco_pT=-9999;

	for (size_t i_child=0; i_child<particle->nChildren(); i_child++) {
	  const xAOD::TruthParticle* child = particle->child(i_child);
	  if (child == 0) continue;
	  //Nu
	  if(std::fabs(child->pdgId())==14 && (!flag_Nu))
	    flag_Nu=true;
	  //Mu
	  if(std::fabs(child->pdgId())==13 && (!flag_Mu)){
	    flag_Mu=true;
	    if (child->pt() > 2700.) flag_isHighPt_Mu=true;
	  }
	  //Dstar
	  if(std::fabs(child->pdgId())==413 && (!flag_Dstar)){
	    flag_Dstar=true;
	    flag_isHighPt_Dstar=true;
	    for (size_t i2_child=0; i2_child<child->nChildren(); i2_child++) {
	      const xAOD::TruthParticle* child2 = child->child(i2_child);
	      if (child2 == 0) continue;
	      //Pi_s
	      if(std::fabs(child2->pdgId())==211 && (!flag_PiS)){
		flag_PiS=true;
		if (child2->pt() > 450.) flag_isHighPt_Pi_s=true;
	      }
	      //D0
	      if(std::fabs(child2->pdgId())==421 && (!flag_D0)){
		flag_D0=true;
		num_D0=child2->nChildren();
		flag_isHighPt_D0=true;
		for (size_t i3_child=0; i3_child<child2->nChildren(); i3_child++) {
		  const xAOD::TruthParticle* child3 = child2->child(i3_child);
		  if (child3 == 0) continue;
		  //K, Pi
		  if(child3->isCharged()) {
		    num_isCharged++;
		    D0_child_mass.push_back(child3->m());
		    D0_child_pT.push_back(child3->pt());
		    D0_child_eta.push_back(child3->eta());
		    D0_child_phi.push_back(child3->phi());
		    D0_child_charge.push_back(child3->charge());
		    D0_child_pdgID.push_back(child3->pdgId());
		    D0_child_vec.SetPtEtaPhiM(child3->pt(), child3->eta(), child3->phi(), child3->m());
		    p4_D0_child.push_back(D0_child_vec);
		  }
		}//end part D0
	      }//end D0
	    }//end part Dstar
	  }//end Dstar

	}//end B0

	if(flag_D0) {
	  for(size_t i_child1=0; i_child1<p4_D0_child.size();i_child1++){
	    for(size_t i_child2=i_child1+1; i_child2<p4_D0_child.size();i_child2++){
	      p4_D0_reco = p4_D0_child[i_child1] + p4_D0_child[i_child2];
	      if (fabs(D0_reco_mass-1864.83) > fabs(p4_D0_reco.M()-1864.83)) {
		D0_reco_mass=p4_D0_reco.M();
		D0_reco_pT=p4_D0_reco.Pt();
		if (D0_reco_mass > 1665. && D0_reco_mass < 2065.) {
		  flag_D0_mass=true;
		  if (p4_D0_child[i_child1].Pt() > 450. && p4_D0_child[i_child2].Pt() > 450.)
		    flag_isHighPt_D0_child=true;
		}
	      }
	    }
	  }
	}

	Int_t DecayType = 0;

	//Fill
	if(flag_Mu&&flag_Nu&&flag_Dstar&&flag_PiS&&flag_D0&&flag_D0_mass){
	  n_Dstar_Truth++;
	  n_B_Dstar_D0Pi_Truth++;
	  n_B_Dstar_Truth++;
	  DecayType=1;
	}
	else continue;
	if(DecayType==1&&flag_isHighPt_Mu&&flag_isHighPt_Dstar&&flag_isHighPt_D0&&flag_isHighPt_Pi_s&&flag_isHighPt_D0_child) {
	  Truth_isHighPt->push_back(true);
	  //isHighPt=true;
	  n_Dstar_Truth_HighPt++;
	} else
	  Truth_isHighPt->push_back(false);

	for (size_t i_child=0; i_child<particle->nChildren(); i_child++) {
	  const xAOD::TruthParticle* child = particle->child(i_child);
	  if (child == 0) continue;
	  //Nu
	  if(std::fabs(child->pdgId())==14){
	    Truth_Nu_pT->push_back(child->pt());
	    Truth_Nu_eta->push_back(child->eta());
	    Truth_Nu_phi->push_back(child->phi());
	  }
	  //Mu
	  if(std::fabs(child->pdgId())==13){
	    Truth_Mu_pT->push_back(child->pt());
	    Truth_Mu_eta->push_back(child->eta());
	    Truth_Mu_phi->push_back(child->phi());
	    Truth_Mu_charge->push_back(child->charge());
	  }
	  //Dstar
	  if(std::fabs(child->pdgId())==413){
	    Truth_Dstar_pT->push_back(child->pt());
	    Truth_Dstar_eta->push_back(child->eta());
	    Truth_Dstar_phi->push_back(child->phi());
	    Truth_Dstar_charge->push_back(child->charge());
	    for (size_t i2_child=0; i2_child<child->nChildren(); i2_child++) {
	      const xAOD::TruthParticle* child2 = child->child(i2_child);
	      if (child2 == 0) continue;
	      //Pi_s
	      if(std::fabs(child2->pdgId())==211){
		Truth_Pi_s_mass->push_back(child2->m());
		Truth_Pi_s_pT->push_back(child2->pt());
		Truth_Pi_s_eta->push_back(child2->eta());
		Truth_Pi_s_phi->push_back(child2->phi());
		Truth_Pi_s_charge->push_back(child2->charge());
		Truth_Pi_s_pdgID->push_back(child2->pdgId());
	      }
	      //D0
	      if(std::fabs(child2->pdgId())==421){
		Truth_D0_pT->push_back(child2->pt());
		Truth_D0_eta->push_back(child2->eta());
		Truth_D0_phi->push_back(child2->phi());
	      }//end D0
	    }//end part Dstar
	  }//end Dstar
	}//end B0 child

	Truth_D0_child_num->push_back(num_D0);
	Truth_D0_charged_child_num->push_back(num_isCharged);
	Truth_D0_reco_mass->push_back(D0_reco_mass);
	Truth_D0_reco_pT->push_back(D0_reco_pT);
	Truth_D0_child_mass->push_back(D0_child_mass);
	Truth_D0_child_pT->push_back(D0_child_pT);
	Truth_D0_child_eta->push_back(D0_child_eta);
	Truth_D0_child_phi->push_back(D0_child_phi);
	Truth_D0_child_charge->push_back(D0_child_charge);
	Truth_D0_child_pdgID->push_back(D0_child_pdgID);

	if(particle->pdgId()>=0)
	  Truth_Flavor->push_back(1);
	else
	  Truth_Flavor->push_back(-1);
	Truth_B_pT->push_back(particle->pt());
	Truth_B_eta->push_back(particle->eta());
	Truth_B_phi->push_back(particle->phi());

	Truth_pdgID->push_back(particle->pdgId());
	Truth_trk_num->push_back(particle->nChildren());
	Truth_parent_num->push_back(particle->nParents());

	std::vector<Int_t>  v_parents_id;
	std::vector<Int_t>  v_siblings_id;
	std::vector<Int_t>  v_children_id;
	std::vector<Int_t>  v_children_num;
	std::vector<std::vector<Int_t>>  v_v_children_id;
	Bool_t isMuon = false;
	// parents
	for (size_t i_parent=0; i_parent<particle->nParents(); i_parent++) {
	  const xAOD::TruthParticle* parent = particle->parent(i_parent);
	  if (parent == 0) continue;
	  v_parents_id.push_back( parent->pdgId() );
	}
	// children
	for (size_t i_child=0; i_child<particle->nChildren(); i_child++) {
	  const xAOD::TruthParticle* child = particle->child(i_child);
	  if (child == 0) continue;
	  if (fabs(child->pdgId()) == 13) isMuon = true;

	  v_siblings_id.push_back( child->pdgId() );
	  v_children_num.push_back(child->nChildren());
	  for (size_t i2_child=0; i2_child<child->nChildren(); i2_child++) {
	    const xAOD::TruthParticle* child2 = child->child(i2_child);
	    if (child2 == 0) continue;
	    v_children_id.push_back( child2->pdgId() );
	  }
	  //std::sort(v_children_id.begin(), v_children_id.end());
	  v_v_children_id.push_back( v_children_id );
	}

	Truth_DecayTime->push_back(TruthDecayTime);
	Truth_DeltaT->push_back(TruthDeltaT);
	Truth_hasMuon->push_back(isMuon);
	Truth_DecayType->push_back(DecayType);
	Truth_child_num->push_back(v_children_num);

	Truth_parent_pdgID->push_back( v_parents_id );
	Truth_trk_pdgID->push_back( v_siblings_id );
	Truth_child_pdgID->push_back( v_v_children_id );

	// only signals reach here
	ATH_MSG_INFO("B with barcode: " << particle->barcode());
	auto truthVtx = particle->decayVtx();
	//ATH_MSG_INFO("error?1>");
	TVector3 truthPos;
	truthPos.SetXYZ( truthVtx->x(), truthVtx->y(), truthVtx->z() );
	//ATH_MSG_INFO(">error?1");

	size_t  closest_cand_idx=-1, closest_cand_idx_Pass=-1;
	Float_t min_distance=9999., min_x=9999, min_y=9999, min_z=9999, Dx=9999, Dy=9999, Dz=9999;
	Float_t min_eta=9999, min_phi=9999;
	Float_t min_distance_Pass=9999., min_x_Pass=9999, min_y_Pass=9999, min_z_Pass=9999, Dx_Pass=9999, Dy_Pass=9999, Dz_Pass=9999;
	Float_t min_eta_Pass=9999, min_phi_Pass=9999;
	Float_t B_mass=-9999, B_pT=-9999, B_chi2=9999;
	Float_t Dstar_mass=-9999, Dstar_charge = 0, Dstar_chi2=9999, MuPi_chi2=9999;
	Float_t D0_mass=-9999, D0_Lxy=-9999;
	Float_t Mu_quality=0, Mu_charge = 0;
	Float_t Pi_charge = 0;
	Float_t Pi_s_charge = 0, Pi_s_theta_star = -9999;
	Float_t K_charge = 0, K_theta_star=-9999;
	TLorentzVector p4_kk, p4_mupi, p4_d0, p4_dx, p4_b;
	TLorentzVector p4_mu, p4_pi_s, p4_pi, p4_k, p4_k2;
	TLorentzVector mom_pi, mom_k, mom_pi_s, mom_d0, mom_dstar;
	Bool_t isPassed=false, isMatched=false, isMatched_Pass=false;

	Truth_eta->push_back(truthPos.Eta());
	Truth_phi->push_back(truthPos.Phi());
	Truth_x->push_back(truthPos.x());
	Truth_y->push_back(truthPos.y());
	Truth_z->push_back(truthPos.z());

	// Truth B0 -> Mu Nu D* -> Pi D0 ->K Pi
	for (vtxItr=BMuDpst->begin(); vtxItr!=BMuDpst->end(); ++vtxItr) {
	  size_t _idx = std::distance(BMuDpst->begin(), vtxItr);
	  xAOD::Vertex* vtx = *vtxItr;
	  TVector3 recoPos;
	  recoPos.SetXYZ( (*vtxItr)->x(), (*vtxItr)->y(), (*vtxItr)->z());

	  xAOD::BPhysHelper B_Helper(vtx);
	  xAOD::BPhysHypoHelper B_HypoHelper("B", vtx);
	  xAOD::BPhysHypoHelper PiD0_HypoHelper("PiD0", vtx);
	  xAOD::BPhysHypoHelper Kpi_HypoHelper("Kpi", vtx);

	  static SG::AuxElement::ConstAccessor<VertexLinkVector> MuPiAcc( "MuPiVertexLinks" );
	  static SG::AuxElement::ConstAccessor<MuonLinkVector> MuAcc( "MuonLinks" );
	  size_t index_Mu;
	  size_t index_MuPi;
    
	  if(MuPiAcc.isAvailable(*vtx)) {
	    for (auto MuPi_Itr : MuPiAcc(*vtx) ) {
	      VertexLink mupi_vtx = MuPi_Itr;
	      MuPi_vtx = mupi_vtx.getStorableObjectPointer();
	      index_MuPi = mupi_vtx.index();
        
	      if(mupi_vtx.isValid()) {
		xAOD::VertexContainer::const_iterator MuPiCand_Itr;
		for (MuPiCand_Itr=MuPi_vtx->begin(); MuPiCand_Itr!=MuPi_vtx->end(); ++MuPiCand_Itr ) {
		  size_t MuPi_idx = std::distance(MuPi_vtx->begin(), MuPiCand_Itr);
		  if(MuPi_idx!=index_MuPi) continue;
		  xAOD::Vertex* MuPiCand_vtx = *MuPiCand_Itr;
		  if(MuAcc.isAvailable(*MuPiCand_vtx)) {
		    for (auto Mu_Itr : MuAcc(*MuPiCand_vtx) ) {
		      MuonLink mu = Mu_Itr;
		      Mu = mu.getStorableObjectPointer();
		      index_Mu = mu.index();
		      if(mu.isValid()) {
			xAOD::MuonContainer::const_iterator MuCand_Itr;
			for (MuCand_Itr=Mu->begin(); MuCand_Itr!=Mu->end(); ++MuCand_Itr ) {
			  size_t Mu_idx = std::distance(Mu->begin(), MuCand_Itr);
			  if(Mu_idx!=index_Mu) continue;
			  const xAOD::Muon_v1* MuCand = *MuCand_Itr;
			  if(MuCand->quality() == xAOD::Muon::Tight)
			    Mu_quality=4;
			  else if(MuCand->quality() == xAOD::Muon::Medium)
			    Mu_quality=3;
			  else if(MuCand->quality() == xAOD::Muon::Loose)
			    Mu_quality=2;
			  else if(MuCand->quality() == xAOD::Muon::VeryLoose)
			    Mu_quality=1;
			  else
			    Mu_quality=0;
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }

	  bool tagD0(true);
	  auto MuPiCand_vtx = B_Helper.vtx();
	  auto CascadeCand_vtx = B_Helper.cascadeVertex(0);

	  Pi_s_charge=MuPiCand_vtx->trackParticle(1)->charge();
	  Mu_charge=MuPiCand_vtx->trackParticle(0)->charge();
	
	  p4_mu.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(0)->pt(),
			     MuPiCand_vtx->trackParticle(0)->eta(),
			     MuPiCand_vtx->trackParticle(0)->phi(),
			     MU_MASS); //mu
	  p4_pi_s.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(1)->pt(),
			       MuPiCand_vtx->trackParticle(1)->eta(),
			       MuPiCand_vtx->trackParticle(1)->phi(),
			       PI_MASS);  // pi_soft
	  p4_mupi = p4_mu + p4_pi_s;

	  mom_pi_s = MuPiCand_vtx->trackParticle(1)->p4();
	  mom_pi_s.SetXYZM(mom_pi_s.Px(), mom_pi_s.Py(), mom_pi_s.Pz(), PI_MASS);
	    
	  MuPi_chi2=MuPiCand_vtx->chiSquared();
	  if(MuPiCand_vtx->trackParticle(1)->charge()==-1) tagD0 = false; 
	  else tagD0 = true;
     
	  if(tagD0){
	    p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
			       CascadeCand_vtx->trackParticle(0)->eta(),
			       CascadeCand_vtx->trackParticle(0)->phi(),
			       PI_MASS); //pi
	    p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
			      CascadeCand_vtx->trackParticle(1)->eta(),
			      CascadeCand_vtx->trackParticle(1)->phi(),
			      K_MASS);  // k
	    p4_d0 = p4_pi + p4_k;
	    mom_pi = CascadeCand_vtx->trackParticle(0)->p4();
	    mom_k = CascadeCand_vtx->trackParticle(1)->p4();
	    mom_pi.SetXYZM(mom_pi.Px(), mom_pi.Py(), mom_pi.Pz(), PI_MASS);
	    mom_k.SetXYZM(mom_k.Px(), mom_k.Py(), mom_k.Pz(), K_MASS);
	    mom_d0 = mom_pi + mom_k;
	    //D0_mass=p4_d0.M();
	    const TVector3 Bboost = mom_d0.BoostVector();
	    mom_k.Boost ( -( Bboost ) );
	    K_theta_star = (mom_d0.Vect()).Angle((mom_k.Vect()));
	  }
	  else {
	    p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
			       CascadeCand_vtx->trackParticle(1)->eta(),
			       CascadeCand_vtx->trackParticle(1)->phi(),
			       PI_MASS); //pi
	    p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
			      CascadeCand_vtx->trackParticle(0)->eta(),
			      CascadeCand_vtx->trackParticle(0)->phi(),
			      K_MASS);  // k
	    p4_d0 = p4_pi + p4_k;
	    //D0_mass=p4_d0.M();
	    mom_pi = CascadeCand_vtx->trackParticle(1)->p4();
	    mom_k = CascadeCand_vtx->trackParticle(0)->p4();
	    mom_pi.SetXYZM(mom_pi.Px(), mom_pi.Py(), mom_pi.Pz(), PI_MASS);
	    mom_k.SetXYZM(mom_k.Px(), mom_k.Py(), mom_k.Pz(), K_MASS);
	    mom_d0 = mom_pi + mom_k;
	    const TVector3 Bboost = mom_d0.BoostVector();
	    mom_k.Boost ( -( Bboost ) );
	    K_theta_star = (mom_d0.Vect()).Angle((mom_k.Vect()));
	  }
    
	  D0_mass=Kpi_HypoHelper.mass();
	  Dstar_mass=PiD0_HypoHelper.mass();
	  Dstar_chi2=CascadeCand_vtx->chiSquared();
	  static SG::AuxElement::ConstAccessor<float> Acc_D0Lxy("D0_Lxy");
	  if(Acc_D0Lxy.isAvailable(*vtx))
	    D0_Lxy = Acc_D0Lxy(*vtx);
	  static SG::AuxElement::ConstAccessor<float> Acc_BChi2("ChiSquared");
	  if(Acc_BChi2.isAvailable(*vtx))
	    B_chi2=Acc_BChi2(*vtx);
	  static SG::AuxElement::ConstAccessor<float> Acc_Pt("Pt");
	  if(Acc_Pt.isAvailable(*vtx))
	    B_pT=Acc_Pt(*vtx);
	  B_mass=B_HypoHelper.mass();
	  if(p4_pi_s.M() > 0 && p4_d0.M() > 0){
	    p4_dx = p4_pi_s + p4_d0;
	    //Dstar_mass=p4_dx.M();
	    p4_b = p4_mu + p4_dx;
	  }
	  mom_dstar = mom_d0 + mom_pi_s;
	  const TVector3 Bboost2 = mom_dstar.BoostVector();
	  mom_pi_s.Boost ( -( Bboost2 ) );
	  Pi_s_theta_star = (mom_dstar.Vect()).Angle((mom_pi_s.Vect()));
	  //BDT score calculation
	  i_Mu_quality = Mu_quality;
	  i_MuPi_chi2 = MuPi_chi2;
	  i_D0_chi2 = Dstar_chi2;
	  i_B_chi2 = B_chi2;
	  i_D0_Lxy = D0_Lxy;
	  //i_B_Lxy = B_Lxy;
	  i_Cos_theta_star_K = fabs(cos(K_theta_star));
	  i_Cos_theta_star_Pi_s = fabs(cos(Pi_s_theta_star));
	  i_B_pT = B_pT;
	  //i_B_Mass = B_mass;
	  //i_Delta_Mass = Delta_Mass;
	  Float_t BDT_score = reader->EvaluateMVA("BDT method");

	  //if(BDT_score>-0.04) isPassed=true;
	  if(Mu_charge*Pi_s_charge<0 && Dstar_mass-D0_mass < 151) isPassed=true;
	  else isPassed=false;

	  recoVtx_isPassed_BMuDpst[_idx] = isPassed;

	  //if ( !recoVtx_isUsed_BMuDpst[_idx] && (truthPos-recoPos).Mag() < DIS_CUT ) {
	  if ( (!recoVtx_isUsed_BMuDpst[_idx]) && (truthPos-recoPos).Mag() < min_distance ) {
	    min_x = recoPos.x();
	    min_y = recoPos.y();
	    min_z = recoPos.z();
	    min_eta = recoPos.Eta();
	    min_phi = recoPos.Phi();
	    min_distance = (truthPos-recoPos).Mag();
	    Dx = (truthPos-recoPos).x();
	    Dy = (truthPos-recoPos).y();
	    Dz = (truthPos-recoPos).z();
	    closest_cand_idx = _idx;
	  }
	  if ( (truthPos-recoPos).Mag() < DIS_CUT ){
	    recoVtx_isUsed_BMuDpst[_idx]=true;
	    isMatched=true;
	  }
	  //if ( !recoVtx_isUsed_BMuDpst_Pass[_idx] && (truthPos-recoPos).Mag() < DIS_CUT && isPassed ) {
	  if ( (!recoVtx_isUsed_BMuDpst_Pass[_idx]) && (truthPos-recoPos).Mag() < min_distance_Pass && isPassed ) {
	    min_x_Pass = recoPos.x();
	    min_y_Pass = recoPos.y();
	    min_z_Pass = recoPos.z();
	    min_eta_Pass = recoPos.Eta();
	    min_phi_Pass = recoPos.Phi();
	    min_distance_Pass = (truthPos-recoPos).Mag();
	    Dx_Pass = (truthPos-recoPos).x();
	    Dy_Pass = (truthPos-recoPos).y();
	    Dz_Pass = (truthPos-recoPos).z();
	    closest_cand_idx_Pass = _idx;
	  }
	  if ( (truthPos-recoPos).Mag() < DIS_CUT && isPassed ){
	    recoVtx_isUsed_BMuDpst_Pass[_idx]=true;
	    isMatched_Pass=true;
	  }
	}

	Reco_x->push_back(min_x);
	Reco_y->push_back(min_y);
	Reco_z->push_back(min_z);
	Reco_eta->push_back(min_eta);
	Reco_phi->push_back(min_phi);	
	Min_d->push_back(min_distance);
	Min_dx->push_back(Dx);
	Min_dy->push_back(Dy);
	Min_dz->push_back(Dz);

	Reco_x_Pass->push_back(min_x_Pass);
	Reco_y_Pass->push_back(min_y_Pass);
	Reco_z_Pass->push_back(min_z_Pass);
	Reco_eta_Pass->push_back(min_eta_Pass);
	Reco_phi_Pass->push_back(min_phi_Pass);	
	Min_d_Pass->push_back(min_distance_Pass);
	Min_dx_Pass->push_back(Dx_Pass);
	Min_dy_Pass->push_back(Dy_Pass);
	Min_dz_Pass->push_back(Dz_Pass);

	isMatch->push_back(isMatched);
	isMatch_Pass->push_back(isMatched_Pass);
	if(isMatched){
	  n_Dstar_Match++;
	  recoVtx_isUsed_BMuDpst[closest_cand_idx]=true;
	}
	if(isMatched_Pass){
	  n_Dstar_Match_Pass++;
	  recoVtx_isUsed_BMuDpst_Pass[closest_cand_idx_Pass]=true;
	}

	if(closest_cand_idx==-1) {
	  Reco_B_m->push_back(-9999);
	  Reco_B_pT->push_back(-9999);
	  Reco_B_eta->push_back(-9999);
	  Reco_B_phi->push_back(-9999);
	  Reco_B_flavor->push_back(0);
	  Reco_Dstar_m->push_back(-9999);
	  Reco_Dstar_pT->push_back(-9999);
	  Reco_Dstar_eta->push_back(-9999);
	  Reco_Dstar_phi->push_back(-9999);
	  Reco_Dstar_charge->push_back(0);
	  Reco_D0_m->push_back(-9999);
	  Reco_D0_pT->push_back(-9999);
	  Reco_D0_eta->push_back(-9999);
	  Reco_D0_phi->push_back(-9999);
	  Reco_Mu_pT->push_back(-9999);
	  Reco_Mu_eta->push_back(-9999);
	  Reco_Mu_phi->push_back(-9999);
	  Reco_Mu_charge->push_back(0);
	  Reco_Pi_s_pT->push_back(-9999);
	  Reco_Pi_s_eta->push_back(-9999);
	  Reco_Pi_s_phi->push_back(-9999);
	  Reco_Pi_s_charge->push_back(0);
	  Reco_Pi_pT->push_back(-9999);
	  Reco_Pi_eta->push_back(-9999);
	  Reco_Pi_phi->push_back(-9999);
	  Reco_Pi_charge->push_back(0);
	  Reco_K_pT->push_back(-9999);
	  Reco_K_eta->push_back(-9999);
	  Reco_K_phi->push_back(-9999);
	  Reco_K_charge->push_back(0);
	}

	if(closest_cand_idx_Pass!=-1) {
	  Reco_B_m_Pass->push_back(-9999);
	  Reco_B_pT_Pass->push_back(-9999);
	  Reco_B_eta_Pass->push_back(-9999);
	  Reco_B_phi_Pass->push_back(-9999);
	  Reco_B_flavor_Pass->push_back(0);
	  Reco_Dstar_m_Pass->push_back(-9999);
	  Reco_Dstar_pT_Pass->push_back(-9999);
	  Reco_Dstar_eta_Pass->push_back(-9999);
	  Reco_Dstar_phi_Pass->push_back(-9999);
	  Reco_Dstar_charge_Pass->push_back(0);
	  Reco_D0_m_Pass->push_back(-9999);
	  Reco_D0_pT_Pass->push_back(-9999);
	  Reco_D0_eta_Pass->push_back(-9999);
	  Reco_D0_phi_Pass->push_back(-9999);
	  Reco_Mu_pT_Pass->push_back(-9999);
	  Reco_Mu_eta_Pass->push_back(-9999);
	  Reco_Mu_phi_Pass->push_back(-9999);
	  Reco_Mu_charge_Pass->push_back(0);
	  Reco_Pi_s_pT_Pass->push_back(-9999);
	  Reco_Pi_s_eta_Pass->push_back(-9999);
	  Reco_Pi_s_phi_Pass->push_back(-9999);
	  Reco_Pi_s_charge_Pass->push_back(0);
	  Reco_Pi_pT_Pass->push_back(-9999);
	  Reco_Pi_eta_Pass->push_back(-9999);
	  Reco_Pi_phi_Pass->push_back(-9999);
	  Reco_Pi_charge_Pass->push_back(0);
	  Reco_K_pT_Pass->push_back(-9999);
	  Reco_K_eta_Pass->push_back(-9999);
	  Reco_K_phi_Pass->push_back(-9999);
	  Reco_K_charge_Pass->push_back(0);
	}

	for (vtxItr=BMuDpst->begin(); vtxItr!=BMuDpst->end(); ++vtxItr) {
	  size_t _idx = std::distance(BMuDpst->begin(), vtxItr);
	  xAOD::Vertex* vtx = *vtxItr;
	  xAOD::BPhysHelper B_Helper(vtx);
	  xAOD::BPhysHypoHelper B_HypoHelper("B", vtx);
	  bool tagD0(true);
	  auto MuPiCand_vtx = B_Helper.vtx();
	  auto CascadeCand_vtx = B_Helper.cascadeVertex(0);

	  // Ignore cut selection of reconstructed track
	  if(closest_cand_idx==_idx) {
	    Reco_Mu_eta->push_back(MuPiCand_vtx->trackParticle(0)->eta());
	    Reco_Mu_phi->push_back(MuPiCand_vtx->trackParticle(0)->phi());
	    Reco_Mu_pT->push_back(MuPiCand_vtx->trackParticle(0)->pt());
	    Reco_Mu_charge->push_back(MuPiCand_vtx->trackParticle(0)->charge());
	    Reco_Pi_s_eta->push_back(MuPiCand_vtx->trackParticle(1)->eta());
	    Reco_Pi_s_phi->push_back(MuPiCand_vtx->trackParticle(1)->phi());
	    Reco_Pi_s_pT->push_back(MuPiCand_vtx->trackParticle(1)->pt());
	    Reco_Pi_s_charge->push_back(MuPiCand_vtx->trackParticle(1)->charge());
	
	    p4_mu.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(0)->pt(),
			       MuPiCand_vtx->trackParticle(0)->eta(),
			       MuPiCand_vtx->trackParticle(0)->phi(),
			       MU_MASS); //mu
	    p4_pi_s.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(1)->pt(),
				 MuPiCand_vtx->trackParticle(1)->eta(),
				 MuPiCand_vtx->trackParticle(1)->phi(),
				 PI_MASS);  // pi_soft
	    p4_mupi = p4_mu + p4_pi_s;

	    if(MuPiCand_vtx->trackParticle(1)->charge()==-1) tagD0 = false; 
	    else tagD0 = true;
     
	    if(tagD0){
	      Reco_Pi_eta->push_back(CascadeCand_vtx->trackParticle(0)->eta());
	      Reco_Pi_phi->push_back(CascadeCand_vtx->trackParticle(0)->phi());
	      Reco_Pi_pT->push_back(CascadeCand_vtx->trackParticle(0)->pt());
	      Reco_Pi_charge->push_back(CascadeCand_vtx->trackParticle(0)->charge());
	      Reco_K_eta->push_back(CascadeCand_vtx->trackParticle(1)->eta());
	      Reco_K_phi->push_back(CascadeCand_vtx->trackParticle(1)->phi());
	      Reco_K_pT->push_back(CascadeCand_vtx->trackParticle(1)->pt());
	      Reco_K_charge->push_back(CascadeCand_vtx->trackParticle(1)->charge());

	      p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
				 CascadeCand_vtx->trackParticle(0)->eta(),
				 CascadeCand_vtx->trackParticle(0)->phi(),
				 PI_MASS); //pi
	      p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
				CascadeCand_vtx->trackParticle(1)->eta(),
				CascadeCand_vtx->trackParticle(1)->phi(),
				K_MASS);  // k
	      p4_d0 = p4_pi + p4_k;
	      Reco_D0_m->push_back(p4_d0.M());
	      Reco_D0_eta->push_back(p4_d0.Eta());
	      Reco_D0_phi->push_back(p4_d0.Phi());
	      Reco_D0_pT->push_back(p4_d0.Pt());
	    }
	    else {
	      Reco_Pi_eta->push_back(CascadeCand_vtx->trackParticle(1)->eta());
	      Reco_Pi_phi->push_back(CascadeCand_vtx->trackParticle(1)->phi());
	      Reco_Pi_pT->push_back(CascadeCand_vtx->trackParticle(1)->pt());
	      Reco_Pi_charge->push_back(CascadeCand_vtx->trackParticle(1)->charge());
	      Reco_K_eta->push_back(CascadeCand_vtx->trackParticle(0)->eta());
	      Reco_K_phi->push_back(CascadeCand_vtx->trackParticle(0)->phi());
	      Reco_K_pT->push_back(CascadeCand_vtx->trackParticle(0)->pt());
	      Reco_K_charge->push_back(CascadeCand_vtx->trackParticle(0)->charge());

	      p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
				 CascadeCand_vtx->trackParticle(1)->eta(),
				 CascadeCand_vtx->trackParticle(1)->phi(),
				 PI_MASS); //pi
	      p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
				CascadeCand_vtx->trackParticle(0)->eta(),
				CascadeCand_vtx->trackParticle(0)->phi(),
				K_MASS);  // k
	      p4_d0 = p4_pi + p4_k;
	      Reco_D0_m->push_back(p4_d0.M());
	      Reco_D0_eta->push_back(p4_d0.Eta());
	      Reco_D0_phi->push_back(p4_d0.Phi());
	      Reco_D0_pT->push_back(p4_d0.Pt());
	    }
    
	    p4_dx = p4_pi_s + p4_d0;
	    Reco_Dstar_m->push_back(p4_dx.M());
	    Reco_Dstar_eta->push_back(p4_dx.Eta());
	    Reco_Dstar_phi->push_back(p4_dx.Phi());
	    Reco_Dstar_pT->push_back(p4_dx.Pt());
	    Reco_Dstar_charge->push_back(MuPiCand_vtx->trackParticle(1)->charge());
	    p4_b = p4_mu + p4_dx;
	    Reco_B_m->push_back(B_HypoHelper.mass());
	    Reco_B_eta->push_back(p4_b.Eta());
	    Reco_B_phi->push_back(p4_b.Phi());
	    Reco_B_pT->push_back(p4_b.Pt());
	    if(MuPiCand_vtx->trackParticle(0)->charge() == 1)
	      Reco_B_flavor->push_back(-1.);
	    else
	      Reco_B_flavor->push_back(1.);
	    //Reco_isPassed->push_back(recoVtx_isPassed_BMuDpst[_idx]);
	  }

	  // Pass the cut selection of reconstructed track
	  if(closest_cand_idx_Pass==_idx) {
	    Reco_Mu_eta_Pass->push_back(MuPiCand_vtx->trackParticle(0)->eta());
	    Reco_Mu_phi_Pass->push_back(MuPiCand_vtx->trackParticle(0)->phi());
	    Reco_Mu_pT_Pass->push_back(MuPiCand_vtx->trackParticle(0)->pt());
	    Reco_Mu_charge_Pass->push_back(MuPiCand_vtx->trackParticle(0)->charge());
	    Reco_Pi_s_eta_Pass->push_back(MuPiCand_vtx->trackParticle(1)->eta());
	    Reco_Pi_s_phi_Pass->push_back(MuPiCand_vtx->trackParticle(1)->phi());
	    Reco_Pi_s_pT_Pass->push_back(MuPiCand_vtx->trackParticle(1)->pt());
	    Reco_Pi_s_charge_Pass->push_back(MuPiCand_vtx->trackParticle(1)->charge());
	
	    p4_mu.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(0)->pt(),
			       MuPiCand_vtx->trackParticle(0)->eta(),
			       MuPiCand_vtx->trackParticle(0)->phi(),
			       MU_MASS); //mu
	    p4_pi_s.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(1)->pt(),
				 MuPiCand_vtx->trackParticle(1)->eta(),
				 MuPiCand_vtx->trackParticle(1)->phi(),
				 PI_MASS);  // pi_soft
	    p4_mupi = p4_mu + p4_pi_s;

	    if(MuPiCand_vtx->trackParticle(1)->charge()==-1) tagD0 = false; 
	    else tagD0 = true;
     
	    if(tagD0){
	      Reco_Pi_eta_Pass->push_back(CascadeCand_vtx->trackParticle(0)->eta());
	      Reco_Pi_phi_Pass->push_back(CascadeCand_vtx->trackParticle(0)->phi());
	      Reco_Pi_pT_Pass->push_back(CascadeCand_vtx->trackParticle(0)->pt());
	      Reco_Pi_charge_Pass->push_back(CascadeCand_vtx->trackParticle(0)->charge());
	      Reco_K_eta_Pass->push_back(CascadeCand_vtx->trackParticle(1)->eta());
	      Reco_K_phi_Pass->push_back(CascadeCand_vtx->trackParticle(1)->phi());
	      Reco_K_pT_Pass->push_back(CascadeCand_vtx->trackParticle(1)->pt());
	      Reco_K_charge_Pass->push_back(CascadeCand_vtx->trackParticle(1)->charge());

	      p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
				 CascadeCand_vtx->trackParticle(0)->eta(),
				 CascadeCand_vtx->trackParticle(0)->phi(),
				 PI_MASS); //pi
	      p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
				CascadeCand_vtx->trackParticle(1)->eta(),
				CascadeCand_vtx->trackParticle(1)->phi(),
				K_MASS);  // k
	      p4_d0 = p4_pi + p4_k;
	      Reco_D0_m_Pass->push_back(p4_d0.M());
	      Reco_D0_eta_Pass->push_back(p4_d0.Eta());
	      Reco_D0_phi_Pass->push_back(p4_d0.Phi());
	      Reco_D0_pT_Pass->push_back(p4_d0.Pt());
	    }
	    else {
	      Reco_Pi_eta_Pass->push_back(CascadeCand_vtx->trackParticle(1)->eta());
	      Reco_Pi_phi_Pass->push_back(CascadeCand_vtx->trackParticle(1)->phi());
	      Reco_Pi_pT_Pass->push_back(CascadeCand_vtx->trackParticle(1)->pt());
	      Reco_Pi_charge_Pass->push_back(CascadeCand_vtx->trackParticle(1)->charge());
	      Reco_K_eta_Pass->push_back(CascadeCand_vtx->trackParticle(0)->eta());
	      Reco_K_phi_Pass->push_back(CascadeCand_vtx->trackParticle(0)->phi());
	      Reco_K_pT_Pass->push_back(CascadeCand_vtx->trackParticle(0)->pt());
	      Reco_K_charge_Pass->push_back(CascadeCand_vtx->trackParticle(0)->charge());

	      p4_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
				 CascadeCand_vtx->trackParticle(1)->eta(),
				 CascadeCand_vtx->trackParticle(1)->phi(),
				 PI_MASS); //pi
	      p4_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
				CascadeCand_vtx->trackParticle(0)->eta(),
				CascadeCand_vtx->trackParticle(0)->phi(),
				K_MASS);  // k
	      p4_d0 = p4_pi + p4_k;
	      Reco_D0_m_Pass->push_back(p4_d0.M());
	      Reco_D0_eta_Pass->push_back(p4_d0.Eta());
	      Reco_D0_phi_Pass->push_back(p4_d0.Phi());
	      Reco_D0_pT_Pass->push_back(p4_d0.Pt());
	    }
    
	    p4_dx = p4_pi_s + p4_d0;
	    Reco_Dstar_m_Pass->push_back(p4_dx.M());
	    Reco_Dstar_eta_Pass->push_back(p4_dx.Eta());
	    Reco_Dstar_phi_Pass->push_back(p4_dx.Phi());
	    Reco_Dstar_pT_Pass->push_back(p4_dx.Pt());
	    Reco_Dstar_charge_Pass->push_back(MuPiCand_vtx->trackParticle(1)->charge());
	    p4_b = p4_mu + p4_dx;
	    Reco_B_m_Pass->push_back(B_HypoHelper.mass());
	    Reco_B_eta_Pass->push_back(p4_b.Eta());
	    Reco_B_phi_Pass->push_back(p4_b.Phi());
	    Reco_B_pT_Pass->push_back(p4_b.Pt());
	    if(MuPiCand_vtx->trackParticle(0)->charge() == 1)
	      Reco_B_flavor_Pass->push_back(-1.);
	    else
	      Reco_B_flavor_Pass->push_back(1.);
	  }
	}
      }
    }
  } // end is_MC


  // Reconstructed B0 Container
  xAOD::VertexContainer::const_iterator B_Itr;

  n_B = std::distance(BMuDpst->begin(), BMuDpst->end());

  // B -> D* mu nu, D* -> D0 Pi_s, D0 -> K Pi 
  for (B_Itr=BMuDpst->begin(); B_Itr!=BMuDpst->end(); ++B_Itr) {
    xAOD::Vertex* vtx = *B_Itr;
    xAOD::BPhysHelper B_Helper(vtx);
    xAOD::BPhysHypoHelper B_HypoHelper("B", vtx);
    xAOD::BPhysHypoHelper MuPiPiK_HypoHelper("MuPiPiK", vtx);
    xAOD::BPhysHypoHelper PiD0_HypoHelper("PiD0", vtx);
    xAOD::BPhysHypoHelper MuPi_HypoHelper("MuPi", vtx);
    xAOD::BPhysHypoHelper MuPiAft_HypoHelper("MuPiAft", vtx);
    xAOD::BPhysHypoHelper D0_HypoHelper("D0", vtx);
    xAOD::BPhysHypoHelper Kpi_HypoHelper("Kpi", vtx);
    static SG::AuxElement::ConstAccessor<float> Acc_Pt("Pt");
    static SG::AuxElement::ConstAccessor<float> Acc_D0Lxy("D0_Lxy");
    static SG::AuxElement::ConstAccessor<float> Acc_D0pT("D0_Pt");
    static SG::AuxElement::ConstAccessor<VertexLinkVector> MuPiAcc( "MuPiVertexLinks" );
    static SG::AuxElement::ConstAccessor<VertexLinkVector> CascadeAcc( "CascadeVertexLinks" );
    static SG::AuxElement::ConstAccessor<MuonLinkVector> MuAcc( "MuonLinks" );
    static SG::AuxElement::ConstAccessor<VertexLink> RPVAcc( "PvMaxSumPt2Link" );
    static SG::AuxElement::ConstAccessor<VertexLink> PVAcc( "OrigPvMaxSumPt2Link" );

    MuonLink MuLink;

    n_Dstar++;

    m_DecayType->push_back(1);

    bool tagD0(true), Mu_PassIso=false;
    Float_t KPi_mass=0, D0_chi2=0, D0_Lxy=0, MuPi_chi2=0, D0Pi_mass=0, D0Pi_eta=0, B_pT=0, B_mass=0, B_chi2=0;
    Float_t Mu_quality=0, Mu_pT=0, K_theta_star=0, Mu_eta=0, Mu_charge=0, Pi_s_charge=0, Pi_s_theta_star=0; 
    TLorentzVector tlv_mupi, tlv_d0, tlv_d0pi, tlv_dxmu;
    TLorentzVector tlv_mu, tlv_pi_s, tlv_pi, tlv_k;
    TLorentzVector p4_mupi, p4_d0, p4_d0pi, p4_dxmu;
    TLorentzVector p4_mu, p4_pi_s, p4_pi, p4_k;
    TLorentzVector mom_k, mom_pi, mom_pi_s, mom_d0, mom_dstar;

    Int_t n_PV=0;
    Int_t n_RPV=0;
    Int_t n_Mu=0;
    size_t index_Mu;
    Int_t n_MuPi=0;
    size_t index_MuPi;

    if(MuPiAcc.isAvailable(*vtx)) {
      for (auto MuPi_Itr : MuPiAcc(*vtx) ) {
	VertexLink mupi_vtx = MuPi_Itr;
	MuPi_vtx = mupi_vtx.getStorableObjectPointer();
	index_MuPi = mupi_vtx.index();
        
	if(mupi_vtx.isValid()) {
	  xAOD::VertexContainer::const_iterator MuPiCand_Itr;
	  for (MuPiCand_Itr=MuPi_vtx->begin(); MuPiCand_Itr!=MuPi_vtx->end(); ++MuPiCand_Itr ) {
	    size_t MuPi_idx = std::distance(MuPi_vtx->begin(), MuPiCand_Itr);
	    if(MuPi_idx!=index_MuPi) continue;
	    xAOD::Vertex* MuPiCand_vtx = *MuPiCand_Itr;
	    if(MuAcc.isAvailable(*MuPiCand_vtx)) {
	      n_MuPi++;
	      for (auto Mu_Itr : MuAcc(*MuPiCand_vtx) ) {
	        MuonLink mu = Mu_Itr;
		Mu = mu.getStorableObjectPointer();
		index_Mu = mu.index();
		if(mu.isValid()) {
		  xAOD::MuonContainer::const_iterator MuCand_Itr;
		  for (MuCand_Itr=Mu->begin(); MuCand_Itr!=Mu->end(); ++MuCand_Itr ) {
		    size_t Mu_idx = std::distance(Mu->begin(), MuCand_Itr);
		    if(Mu_idx!=index_Mu) continue;
		    const xAOD::Muon_v1* MuCand = *MuCand_Itr;
		    n_Mu++;
		    if(MuCand->quality() == xAOD::Muon::Tight){
		      Mu_quality=4;
		      m_Mu_quality->push_back(4);
		    } else if(MuCand->quality() == xAOD::Muon::Medium){
		      Mu_quality=3;
		      m_Mu_quality->push_back(3);
		    } else if(MuCand->quality() == xAOD::Muon::Loose){
		      Mu_quality=2;
		      m_Mu_quality->push_back(2);
		    } else if(MuCand->quality() == xAOD::Muon::VeryLoose) {
                      Mu_quality=1;
                      m_Mu_quality->push_back(1);
		    } else {
		      Mu_quality=0;
		      m_Mu_quality->push_back(0);
		    }
		    if ((float)MuCand->isolation(xAOD::Iso::ptvarcone30)/MuCand->pt()<0.06)
		      Mu_PassIso = true;
		  }
		}
	      }
	    }
	  }
	}
      }
    }

    auto MuPiCand_vtx = B_Helper.vtx();
    auto CascadeCand_vtx = B_Helper.cascadeVertex(0);
    auto nprec = CascadeCand_vtx->nTrackParticles();
    m_B_nPart->push_back(nprec);
    TVector3 B_Pos;
    B_Pos.SetXYZ( (*B_Itr)->x(), (*B_Itr)->y(), (*B_Itr)->z());
    m_B_eta->push_back(B_Pos.Eta());
    m_B_phi->push_back(B_Pos.Phi());

    // MuPi vertex
    m_n_MuPi->push_back(n_MuPi);

    m_Pi_s_m->push_back(MuPiCand_vtx->trackParticle(1)->m());
    m_Pi_s_pT->push_back(MuPiCand_vtx->trackParticle(1)->pt());
    m_Pi_s_charge->push_back(MuPiCand_vtx->trackParticle(1)->charge());
    Pi_s_charge=MuPiCand_vtx->trackParticle(1)->charge();
    m_Pi_s_eta->push_back(MuPiCand_vtx->trackParticle(1)->eta());
    m_Pi_s_phi->push_back(MuPiCand_vtx->trackParticle(1)->phi());
    m_Pi_s_d0->push_back(MuPiCand_vtx->trackParticle(1)->d0());
    m_Pi_s_z0->push_back(MuPiCand_vtx->trackParticle(1)->z0());
    m_Pi_s_qOverP->push_back(MuPiCand_vtx->trackParticle(1)->qOverP());
      
    m_n_Mu->push_back(n_Mu);
    m_Mu_PassIso->push_back(Mu_PassIso);
    m_Mu_m->push_back(MuPiCand_vtx->trackParticle(0)->m());
    m_Mu_pT->push_back(MuPiCand_vtx->trackParticle(0)->pt());
    Mu_pT=MuPiCand_vtx->trackParticle(0)->pt();
    m_Mu_charge->push_back(MuPiCand_vtx->trackParticle(0)->charge());
    Mu_charge=MuPiCand_vtx->trackParticle(0)->charge();
    m_Mu_eta->push_back(MuPiCand_vtx->trackParticle(0)->eta());
    Mu_eta=MuPiCand_vtx->trackParticle(0)->eta();
    m_Mu_phi->push_back(MuPiCand_vtx->trackParticle(0)->phi());
    m_Mu_d0->push_back(MuPiCand_vtx->trackParticle(0)->d0());
    m_Mu_z0->push_back(MuPiCand_vtx->trackParticle(0)->z0());
    m_Mu_qOverP->push_back(MuPiCand_vtx->trackParticle(0)->qOverP());

    tlv_mu.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(0)->pt(),
			MuPiCand_vtx->trackParticle(0)->eta(),
			MuPiCand_vtx->trackParticle(0)->phi(),
			MU_MASS);  // mu
    //MuPiCand_vtx->trackParticle(0)->m());  // contained mass is Pi mass
    tlv_pi_s.SetPtEtaPhiM(MuPiCand_vtx->trackParticle(1)->pt(),
			  MuPiCand_vtx->trackParticle(1)->eta(),
			  MuPiCand_vtx->trackParticle(1)->phi(),
			  PI_MASS);  // pi_soft
    //MuPiCand_vtx->trackParticle(1)->m());  // pi_soft
    tlv_mupi = tlv_mu + tlv_pi_s;

    p4_mu = MuPiCand_vtx->trackParticle(0)->p4();
    p4_pi_s = MuPiCand_vtx->trackParticle(1)->p4();
    p4_mu.SetXYZM(p4_mu.Px(), p4_mu.Py(), p4_mu.Pz(), MU_MASS);
    p4_pi_s.SetXYZM(p4_pi_s.Px(), p4_pi_s.Py(), p4_pi_s.Pz(), PI_MASS);
    p4_mupi = p4_mu + p4_pi_s;
    mom_pi_s=p4_pi_s;
    
    m_MuPi_mass->push_back( p4_mupi.M());
    m_MuPi_pT->push_back( p4_mupi.Pt());
    m_MuPi_eta->push_back( p4_mupi.Eta());
    m_MuPi_phi->push_back( p4_mupi.Phi());

    if(Pi_s_charge==-1) tagD0 = false; 
    else tagD0 = true;
	
    m_MuPi_chi2->push_back( MuPiCand_vtx->chiSquared() );
    MuPi_chi2=MuPiCand_vtx->chiSquared();
    m_MuPi_nDoF->push_back( MuPiCand_vtx->numberDoF() );

    m_D0_flag->push_back(tagD0);
    if(tagD0){
      m_Pi_m->push_back(CascadeCand_vtx->trackParticle(0)->m());
      m_Pi_pT->push_back(CascadeCand_vtx->trackParticle(0)->pt());
      m_Pi_charge->push_back(CascadeCand_vtx->trackParticle(0)->charge());
      m_Pi_eta->push_back(CascadeCand_vtx->trackParticle(0)->eta());
      m_Pi_phi->push_back(CascadeCand_vtx->trackParticle(0)->phi());
      m_Pi_d0->push_back(CascadeCand_vtx->trackParticle(0)->d0());
      m_Pi_z0->push_back(CascadeCand_vtx->trackParticle(0)->z0());
      m_Pi_qOverP->push_back(CascadeCand_vtx->trackParticle(0)->qOverP());

      m_K_m->push_back(CascadeCand_vtx->trackParticle(1)->m());
      m_K_pT->push_back(CascadeCand_vtx->trackParticle(1)->pt());
      m_K_charge->push_back(CascadeCand_vtx->trackParticle(1)->charge());
      m_K_eta->push_back(CascadeCand_vtx->trackParticle(1)->eta());
      m_K_phi->push_back(CascadeCand_vtx->trackParticle(1)->phi());
      m_K_d0->push_back(CascadeCand_vtx->trackParticle(1)->d0());
      m_K_z0->push_back(CascadeCand_vtx->trackParticle(1)->z0());
      m_K_qOverP->push_back(CascadeCand_vtx->trackParticle(1)->qOverP());

      tlv_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
			  CascadeCand_vtx->trackParticle(0)->eta(),
			  CascadeCand_vtx->trackParticle(0)->phi(),
			  PI_MASS); //pi
      //CascadeCand_vtx->trackParticle(0)->m()); //pi
      tlv_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
			 CascadeCand_vtx->trackParticle(1)->eta(),
		 	 CascadeCand_vtx->trackParticle(1)->phi(),
			 K_MASS);  // k
      //CascadeCand_vtx->trackParticle(1)->m());  // k
      tlv_d0 = tlv_pi + tlv_k;

      p4_pi = CascadeCand_vtx->trackParticle(0)->p4();
      p4_k = CascadeCand_vtx->trackParticle(1)->p4();
      p4_pi.SetXYZM(p4_pi.Px(), p4_pi.Py(), p4_pi.Pz(), PI_MASS);
      p4_k.SetXYZM(p4_k.Px(), p4_k.Py(), p4_k.Pz(), K_MASS);
      p4_d0 = p4_pi + p4_k;
      
      mom_pi=p4_pi;
      mom_k=p4_k;
      mom_d0 = mom_pi + mom_k;
      const TVector3 Bboost = mom_d0.BoostVector();
      mom_k.Boost ( -( Bboost ) );
      mom_pi.Boost ( -( Bboost ) );

      K_theta_star = (mom_d0.Vect()).Angle((mom_k.Vect()));
      m_K_theta_star->push_back((mom_d0.Vect()).Angle((mom_k.Vect())));
      m_Pi_theta_star->push_back((mom_d0.Vect()).Angle((mom_pi.Vect())));

    }
    else {
      m_Pi_m->push_back(CascadeCand_vtx->trackParticle(1)->m());
      m_Pi_pT->push_back(CascadeCand_vtx->trackParticle(1)->pt());
      m_Pi_charge->push_back(CascadeCand_vtx->trackParticle(1)->charge());
      m_Pi_eta->push_back(CascadeCand_vtx->trackParticle(1)->eta());
      m_Pi_phi->push_back(CascadeCand_vtx->trackParticle(1)->phi());
      m_Pi_d0->push_back(CascadeCand_vtx->trackParticle(1)->d0());
      m_Pi_z0->push_back(CascadeCand_vtx->trackParticle(1)->z0());
      m_Pi_qOverP->push_back(CascadeCand_vtx->trackParticle(1)->qOverP());

      m_K_m->push_back(CascadeCand_vtx->trackParticle(0)->m());
      m_K_pT->push_back(CascadeCand_vtx->trackParticle(0)->pt());
      m_K_charge->push_back(CascadeCand_vtx->trackParticle(0)->charge());
      m_K_eta->push_back(CascadeCand_vtx->trackParticle(0)->eta());
      m_K_phi->push_back(CascadeCand_vtx->trackParticle(0)->phi());
      m_K_d0->push_back(CascadeCand_vtx->trackParticle(0)->d0());
      m_K_z0->push_back(CascadeCand_vtx->trackParticle(0)->z0());
      m_K_qOverP->push_back(CascadeCand_vtx->trackParticle(0)->qOverP());

      tlv_pi.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(1)->pt(),
			  CascadeCand_vtx->trackParticle(1)->eta(),
			  CascadeCand_vtx->trackParticle(1)->phi(),
			  PI_MASS); //pi
      //CascadeCand_vtx->trackParticle(1)->m()); //pi
      tlv_k.SetPtEtaPhiM(CascadeCand_vtx->trackParticle(0)->pt(),
			 CascadeCand_vtx->trackParticle(0)->eta(),
		 	 CascadeCand_vtx->trackParticle(0)->phi(),
			 K_MASS);  // k
      //CascadeCand_vtx->trackParticle(0)->m());  // k
      tlv_d0 = tlv_pi + tlv_k;

      p4_pi = CascadeCand_vtx->trackParticle(1)->p4();
      p4_k = CascadeCand_vtx->trackParticle(0)->p4();
      p4_pi.SetXYZM(p4_pi.Px(), p4_pi.Py(), p4_pi.Pz(), PI_MASS);
      p4_k.SetXYZM(p4_k.Px(), p4_k.Py(), p4_k.Pz(), K_MASS);
      p4_d0 = p4_pi + p4_k;
      
      mom_pi=p4_pi;
      mom_k=p4_k;
      mom_d0 = mom_pi + mom_k;
      const TVector3 Bboost = mom_d0.BoostVector();
      mom_k.Boost ( -( Bboost ) );
      mom_pi.Boost ( -( Bboost ) );

      K_theta_star = (mom_d0.Vect()).Angle((mom_k.Vect()));
      m_K_theta_star->push_back((mom_d0.Vect()).Angle((mom_k.Vect())));
      m_Pi_theta_star->push_back((mom_d0.Vect()).Angle((mom_pi.Vect())));

    }

    m_Kpi_pT->push_back( tlv_d0.Pt());
    m_Kpi_eta->push_back( tlv_d0.Eta());
    m_Kpi_phi->push_back( tlv_d0.Phi());
    m_Kpi_mass->push_back( tlv_d0.M());
    KPi_mass=p4_d0.M();
    m_D0_chi2->push_back( CascadeCand_vtx->chiSquared() );
    D0_chi2 = CascadeCand_vtx->chiSquared();
    m_D0_nDoF->push_back( CascadeCand_vtx->numberDoF() );
    
    mom_dstar = mom_d0 + mom_pi_s;
    const TVector3 Bboost2 = mom_dstar.BoostVector();
    mom_pi_s.Boost ( -( Bboost2 ) );
    Pi_s_theta_star = (mom_dstar.Vect()).Angle((mom_pi_s.Vect()));
    m_Pi_s_theta_star->push_back((mom_dstar.Vect()).Angle((mom_pi_s.Vect())));

    if(PVAcc.isAvailable(*vtx)) {
      VertexLink pv_vtx = PVAcc(*vtx);
      PV_vtx = pv_vtx.getStorableObjectPointer();
      size_t index_PV = pv_vtx.index();
	  
      if(pv_vtx.isValid()) {
	xAOD::VertexContainer::const_iterator PVCand_Itr;
	for (PVCand_Itr=PV_vtx->begin(); PVCand_Itr!=PV_vtx->end(); ++PVCand_Itr ) {
	  size_t PV_idx = std::distance(PV_vtx->begin(), PVCand_Itr);
	  if(PV_idx!=index_PV) continue;
	  xAOD::Vertex* PVCand_vtx = *PVCand_Itr;
	  n_PV++;
	  m_PV_x->push_back( (*PVCand_vtx).x() );
	  m_PV_y->push_back( (*PVCand_vtx).y() );
	  m_PV_z->push_back( (*PVCand_vtx).z() );
	}
      }
    }

    if(RPVAcc.isAvailable(*vtx)) {
      VertexLink rpv_vtx = RPVAcc(*vtx);
      RPV_vtx = rpv_vtx.getStorableObjectPointer();
      size_t index_RPV = rpv_vtx.index();
	  
      if(rpv_vtx.isValid()) {
	xAOD::VertexContainer::const_iterator RPVCand_Itr;
	for (RPVCand_Itr=RPV_vtx->begin(); RPVCand_Itr!=RPV_vtx->end(); ++RPVCand_Itr ) {
	  size_t RPV_idx = std::distance(RPV_vtx->begin(), RPVCand_Itr);
	  if(RPV_idx!=index_RPV) continue;
	  xAOD::Vertex* RPVCand_vtx = *RPVCand_Itr;
	  n_RPV++;
	  m_RPV_x->push_back( (*RPVCand_vtx).x() );
	  m_RPV_y->push_back( (*RPVCand_vtx).y() );
	  m_RPV_z->push_back( (*RPVCand_vtx).z() );
	}
      }
    }
    m_n_PV->push_back( n_PV );
    m_n_RPV->push_back( n_RPV );

    m_B_x->push_back( (*vtx).x() );
    m_B_y->push_back( (*vtx).y() );
    m_B_z->push_back( (*vtx).z() );
    if(Acc_Pt.isAvailable(*vtx)){
      m_B_Pt->push_back(Acc_Pt(*vtx));
      B_pT=Acc_Pt(*vtx);
    }
    m_B_mass->push_back( B_HypoHelper.mass() );
    B_mass=B_HypoHelper.mass();
    m_B_mass_err->push_back( B_HypoHelper.massErr() );

    static SG::AuxElement::ConstAccessor<float> Acc_BChi2("ChiSquared");
    if(Acc_BChi2.isAvailable(*vtx)){
      m_B_Chi2->push_back( Acc_BChi2(*vtx) );
      B_chi2=Acc_BChi2(*vtx);
    }
    static SG::AuxElement::ConstAccessor<float> Acc_BnDoF("NumberDoF");
    if(Acc_BnDoF.isAvailable(*vtx))
      m_B_nDoF->push_back( Acc_BnDoF(*vtx) );

    m_D0_mass->push_back( Kpi_HypoHelper.mass() );
    if(Acc_D0Lxy.isAvailable(*vtx)){
      m_D0_Lxy->push_back(Acc_D0Lxy(*vtx));
      D0_Lxy = Acc_D0Lxy(*vtx);
    }
    if(Acc_D0pT.isAvailable(*vtx))
      m_D0_Pt->push_back(Acc_D0pT(*vtx));

    if(p4_pi_s.M() > 0 && p4_d0.M() > 0){
      tlv_d0pi = tlv_pi_s + tlv_d0;
      p4_d0pi = p4_pi_s + p4_d0;
      m_D0Pi_pT->push_back(tlv_d0pi.Pt());
      m_D0Pi_eta->push_back(tlv_d0pi.Eta());
      m_D0Pi_phi->push_back(tlv_d0pi.Phi());
      m_D0Pi_mass->push_back(tlv_d0pi.M());
      D0Pi_eta=tlv_d0pi.Eta();
      D0Pi_mass=p4_d0pi.M();
      tlv_dxmu = tlv_mu + tlv_d0pi;
      p4_dxmu = p4_mu + p4_d0pi;
      m_MuPiPiK_pT->push_back(tlv_dxmu.Pt());
      m_MuPiPiK_eta->push_back(tlv_dxmu.Eta());
      m_MuPiPiK_phi->push_back(tlv_dxmu.Phi());
      m_MuPiPiK_mass->push_back(tlv_dxmu.M());
      m_B_Px->push_back(p4_dxmu.Px());
      m_B_Py->push_back(p4_dxmu.Py());
      m_B_Pz->push_back(p4_dxmu.Pz());
      m_B_E->push_back(p4_dxmu.E());
      if(Mu_charge == 1)
	m_B_flavor->push_back(-1.);
      else if (Mu_charge == -1)
	m_B_flavor->push_back(1.);
      else
	m_B_flavor->push_back(0.);
    }
    //m_MuPiPiK_mass->push_back( MuPiPiK_HypoHelper.mass() );
    m_MuPiAft_mass->push_back( MuPiAft_HypoHelper.mass() );
    m_PiD0_mass->push_back( PiD0_HypoHelper.mass() );

    m_LxyMaxSumPt2->push_back( B_Helper.lxy(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_LxyErrMaxSumPt2->push_back( B_Helper.lxyErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_A0MaxSumPt2->push_back( B_Helper.a0(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_A0ErrMaxSumPt2->push_back( B_Helper.a0Err(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_A0xyMaxSumPt2->push_back( B_Helper.a0xy(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_A0xyErrMaxSumPt2->push_back( B_Helper.a0xyErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_Z0MaxSumPt2->push_back( B_Helper.z0(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_Z0ErrMaxSumPt2->push_back( B_Helper.z0Err(xAOD::BPhysHelper::PV_MAX_SUM_PT2) );
    m_TauInvMassPVMaxSumPt2->push_back( B_HypoHelper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS) );
    m_TauErrInvMassPVMaxSumPt2->push_back( B_HypoHelper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS) );
    m_TauConstMassPVMaxSumPt2->push_back( B_HypoHelper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS) );
    m_TauErrConstMassPVMaxSumPt2->push_back( B_HypoHelper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS) );

    //BDT score calculation
    i_Mu_quality = Mu_quality;
    i_MuPi_chi2 = MuPi_chi2;
    i_D0_chi2 = D0_chi2;
    i_B_chi2 = B_chi2;
    i_D0_Lxy = D0_Lxy;
    //i_B_Lxy = B_Lxy;
    i_Cos_theta_star_K = fabs(cos(K_theta_star));
    i_Cos_theta_star_Pi_s = fabs(cos(Pi_s_theta_star));
    i_B_pT = B_pT;
    //i_B_Mass = B_mass;
    //i_Delta_Mass = Delta_Mass;
    Float_t BDT_score = reader->EvaluateMVA("BDT method");
    m_BDT_score->push_back(BDT_score);
    
    size_t Dpst_idx = std::distance(BMuDpst->begin(), B_Itr);
    if(recoVtx_isUsed_BMuDpst_Pass[Dpst_idx]){
      m_isMatch_Pass->push_back(true);
      n_B_Match_Pass++;
      //n_Dstar_Match_Pass++;
    }  else
      m_isMatch_Pass->push_back(false);

    if(recoVtx_isUsed_BMuDpst[Dpst_idx]){
      m_isMatch->push_back(true);
      n_B_Match++;
      //n_Dstar_Match++;
    } else
      m_isMatch->push_back(false);

    if(Mu_pT>3000&&Mu_quality>2&&fabs(Mu_eta)<2.4&&fabs(D0Pi_eta)<2.5&&D0Pi_mass-KPi_mass>=135&&D0Pi_mass-KPi_mass<=152&&Mu_charge*Pi_s_charge==-1) {
      //if(Mu_pT>3000&&Mu_quality>2&&fabs(Mu_eta)<2.4&&fabs(D0Pi_eta)<2.5&&B_mass>2000&&B_mass<7500&&cos(K_theta_star)>-0.9&&B_chi2<20&&D0_chi2<20&&D0_Lxy>0.05&&D0Pi_mass-KPi_mass>=135&&D0Pi_mass-KPi_mass<=152&&Mu_charge*Pi_s_charge==-1)
      m_isPassedTight->push_back(true);
      n_Dstar_Pass_Tight++;
    } else
      m_isPassedTight->push_back(false);

    if(Mu_pT>2700&&Mu_quality>1&&fabs(Mu_eta)<2.4&&fabs(D0Pi_eta)<2.5) {
      //if(Mu_pT>3000&&Mu_quality>2&&fabs(Mu_eta)<2.4&&fabs(D0Pi_eta)<2.5&&B_mass>2000&&B_mass<7500&&cos(K_theta_star)>-0.9&&B_chi2<20&&D0_chi2<20&&D0_Lxy>0.05) {
      m_isPassed->push_back(true);
      n_Dstar_Pass++;
    } else
      m_isPassed->push_back(false);

  }

  *m_n_B = n_B;
  *m_n_B_Pass = n_B_Pass;
  *m_n_B_Match = n_B_Match;
  *m_n_B_Match_Pass = n_B_Match_Pass;
  *m_n_B_Truth = n_B_Truth;
  *m_n_Dstar = n_Dstar;
  *m_n_Dstar_Pass = n_Dstar_Pass;
  *m_n_Dstar_Pass_Tight = n_Dstar_Pass_Tight;
  *m_n_Dstar_Match = n_Dstar_Match;
  *m_n_Dstar_Match_Pass = n_Dstar_Match_Pass;
  *m_n_Dstar_Truth = n_Dstar_Truth;
  *m_n_Dstar_Truth_HighPt = n_Dstar_Truth_HighPt;
  *m_n_Mu_Truth = n_Mu_Truth;
  *m_n_MET = n_MET;
  *m_n_MET_Truth = n_MET_Truth;
  //*m_n_PV = n_PV;
  //*m_n_RPV = n_RPV;

  *m_px_MET = MET_px;
  *m_py_MET = MET_py;
  *m_sum_MET = MET_sum;
  *m_px_Rec_MET = Rec_MET_px;
  *m_py_Rec_MET = Rec_MET_py;
  *m_sum_Rec_MET = Rec_MET_sum;
  *m_px_Rec_MET_Mu = Rec_MET_Mu_px;
  *m_py_Rec_MET_Mu = Rec_MET_Mu_py;
  *m_sum_Rec_MET_Mu = Rec_MET_Mu_sum;
  *m_px_MET_Track = Track_MET_px;
  *m_py_MET_Track = Track_MET_py;
  *m_sum_MET_Track = Track_MET_sum;
  *m_px_MET_Truth = Truth_MET_px;
  *m_py_MET_Truth = Truth_MET_py;
  *m_sum_MET_Truth = Truth_MET_sum;

  *num_B = n_B;
  *num_B_Pass = n_B_Pass;
  *num_B_Match = n_B_Match;
  *num_B_Match_Pass = n_B_Match_Pass;
  *num_B_Truth = n_B_Truth;
  *num_Dstar = n_Dstar;
  *num_Dstar_Pass = n_Dstar_Pass;
  *num_Dstar_Match = n_Dstar_Match;
  *num_Dstar_Match_Pass = n_Dstar_Match_Pass;
  *num_Dstar_Truth = n_Dstar_Truth;
  *num_Dstar_Truth_HighPt = n_Dstar_Truth_HighPt;
  *num_B_Dstar_Truth = n_B_Dstar_Truth;
  *num_B_Dstar_D0Pi_Truth = n_B_Dstar_D0Pi_Truth;
  *num_B_D_Truth = n_B_D_Truth;
  *num_B_D_s_Truth = n_B_D_s_Truth;
  *num_B_Lambda_c_Truth = n_B_Lambda_c_Truth;

  if(n_B>0)
    B_tree->Fill();

  //if(n_B_Truth>0)
  Truth_B_tree->Fill();

  MET_tree->Fill();
  Truth_MET_tree->Fill();

  //  setFilterPassed(true); //if got here, assume that means algorithm passed
  ATH_MSG_INFO("Event " << en << ", Successfully Passed MyAlg");
  return StatusCode::SUCCESS;
}

StatusCode MyPackage_v2Alg::getTruthPartMatch(const xAOD::TruthEventContainer* xTruthEventContainer, std::vector<Decay_s>& v_decay, std::vector<Double_t>& truth_dr)
{
  xAOD::TruthEventContainer::const_iterator itr;

  for (itr=xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr) {
    size_t nPart = (*itr)->truthParticleLinks().size();
    for (auto itr_decay=v_decay.begin(); itr_decay!=v_decay.end(); ++itr_decay) {
      auto        decay   = itr_decay;
      Particle_s* _self   = &(decay->self);
      TVector3    _trkp3  = _self->p3;

      size_t      match_idx = 999;
      Double_t    min_dr    = 99.;
      for (size_t iPart=0; iPart<nPart; iPart++) {
        const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart);
        if( particle == 0 ) continue;
        //if( fabs(particle->pdgId()) != 321 && fabs(particle->pdgId()) != 211 ) continue;
        //if( particle->charge() != _self->charge ) continue;
        if( !(particle->isCharged()) ) continue;
	TVector3  _truth_p3(0,0,0); _truth_p3.SetPtEtaPhi( particle->pt(), particle->eta(), particle->phi() );
        Double_t  _dr = _truth_p3.DrEtaPhi(_trkp3);
        if (_dr > min_dr) continue;
        match_idx = iPart;
	min_dr = _dr;
      }
      if (match_idx > nPart) continue;

      const xAOD::TruthParticle* particle = (*itr)->truthParticle(match_idx);
      Int_t pdgId = particle->pdgId();
      TVector3  self_p3(0,0,0); self_p3.SetPtEtaPhi( particle->pt(), particle->eta(), particle->phi() );

      std::vector<Particle_s> parents;
      std::vector<Particle_s> siblings;

      size_t nParents = particle->nParents();
      for (size_t i_parent=0; i_parent<nParents; i_parent++) {
	const xAOD::TruthParticle* parent = particle->parent(i_parent);
	if (parent == 0) continue;
	//if( fabs(parent->pdgId()) != 413 && fabs(parent->pdgId()) != 421 ) continue;
	TVector3 parent_p3(0,0,0); parent_p3.SetPtEtaPhi( parent->pt(), parent->eta(), parent->phi() );

	parents.push_back( Particle_s{ parent_p3, parent->pdgId(), parent->m(), parent->charge() } );
	if (i_parent==0) {
	  size_t nChildren = parent->nChildren();
	  for (size_t i_child=0; i_child<nChildren; i_child++) {
	    const xAOD::TruthParticle* child = parent->child(i_child);
	    if (child == 0) continue;
	    TVector3 child_p3(0,0,0); child_p3.SetPtEtaPhi( child->pt(), child->eta(), child->phi() );
	    siblings.push_back( Particle_s{ child_p3, child->pdgId(), child->m(), child->charge() } );
	  }
	}
      }
      truth_dr.push_back(min_dr);

      if(parents.size() == 0) continue;
      _self->p3 = self_p3;
      _self->pdgId = pdgId;
      _self->mass = particle->m();
      _self->charge = particle->charge();

      itr_decay->parents  = parents;
      itr_decay->siblings = siblings;
    }
  }
  return StatusCode::SUCCESS;
}


StatusCode MyPackage_v2Alg::beginInputFile() {
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}
