#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]  #register output files like this. MYSTREAM is used in the code

athAlgSeq += CfgMgr.MyPackage_v2Alg(IsMC=True)                               #adds an instance of your alg to the main alg sequence

""" ====================================================================
# Define your output root file holding the histograms using MultipleStreamManager
# ====================================================================
rootStreamName = "MYSTREAM"
rootFileName   = "myfile.root"
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
MyFirstHistoXAODStream = MSMgr.NewRootStream( rootStreamName, rootFileName )
#"""

#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/ASG/DAOD_PHYS/v1/ttbar.FullSim.DAOD_PHYS.pool.root"]        #set on command-line with: --filesInput=...


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
