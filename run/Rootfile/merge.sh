isMC=1
isAdd=1

cd /home/ytsujika/AnalysisBphys/build/
source $AtlasSetup/scripts/asetup.sh
cd /home/ytsujika/AnalysisBphys/run/Rootfile/
# MC
if [ $isMC -eq 1 ]; then
    if [ $isAdd -eq 1 ]; then
	file=NonSkim_MC.root
	file0=/gpfs/fs8001/ytsujika/rootfile_for_hadd/NonSkim/Run41890581.root
	file1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/NonSkim/Run41890592.root
	if [ -e $file0 ]; then
	    rm -rf $file0
	fi
	if [ -e $file1 ]; then
	    rm -rf $file1
	fi
	# Run_number 34687508, 34687511, 34687513 (MC16)
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/NonSkim/Run41890581/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file0
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/NonSkim/Run41890592/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file1

	if [ -e $file ]; then
	    rm -rf $file
	fi
	
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/NonSkim/Run418905*.root | xargs hadd -f $file
    elif [ $isAdd -eq -1 ]; then
	file=NonSkim_Truth.root
	file0=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Truth/Run41890581.root
	file1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Truth/Run41890592.root
	if [ -e $file0 ]; then
	    rm -rf $file0
	fi
	if [ -e $file1 ]; then
	    rm -rf $file1
	fi
	# Run_number 34687508, 34687511, 34687513 (MC16)
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Truth/Run41890581/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file0
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Truth/Run41890592/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file1

	if [ -e $file ]; then
	    rm -rf $file
	fi
	
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Truth/Run418905*.root | xargs hadd -f $file
    elif [ $isAdd -eq -2 ]; then
	file2=Validation_MC.root
	file2_1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Validation/Run41719774.root
	file2_2=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Validation/Run41719777.root
	if [ -e $file2_1 ]; then
	    rm -rf $file2_1
	fi
	if [ -e $file2_2 ]; then
	    rm -rf $file2_2
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Validation/Run41719774/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file2_1
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Validation/Run41719777/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file2_2

	if [ -e $file2 ]; then
	    rm -rf $file2
	fi
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Validation/Run4171977*.root | xargs hadd -f $file2
    elif [ $isAdd -eq -10 ]; then
	file=Validation_MC.root
	if [ -e $file ]; then
	    rm -rf $file
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Validation/Run000/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file

    else    
	file=Main_MC.root
	file0=/gpfs/fs8001/ytsujika/rootfile_for_hadd/MC16/Run41691446.root
	file1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/MC16/Run41691447.root
	if [ -e $file0 ]; then
	    rm -rf $file0
	fi
	if [ -e $file1 ]; then
	    rm -rf $file1
	fi
	# Run_number 34687508, 34687511, 34687513 (MC16)
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41691446/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file0
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41691447/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file1

	if [ -e $file ]; then
	    rm -rf $file
	fi
	
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/MC16/Run4169144*.root | xargs hadd -f $file
    fi
	
# Data
else
    if [ $isAdd -eq -1 ]; then
	file=Main_PPref.root
	file0=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827301.root
	file1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827316.root
	file2=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827331.root
	file3=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827344.root
	file4=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827358.root
	file5=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827372.root
	file6=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827385.root
	file7=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827400.root
	file8=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827415.root
	file9=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827428.root
	file10=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827441.root
	file11=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827456.root
	file12=/gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827471.root
	if [ -e $file0 ]; then
	    rm -rf $file0
	fi
	if [ -e $file1 ]; then
	    rm -rf $file1
	fi
	if [ -e $file2 ]; then
	    rm -rf $file2
	fi
	if [ -e $file3 ]; then
	    rm -rf $file3
	fi
	if [ -e $file4 ]; then
	    rm -rf $file4
	fi
	if [ -e $file5 ]; then
	    rm -rf $file5
	fi
	if [ -e $file6 ]; then
	    rm -rf $file6
	fi
	if [ -e $file7 ]; then
	    rm -rf $file7
	fi
	if [ -e $file8 ]; then
	    rm -rf $file8
	fi
	if [ -e $file9 ]; then
	    rm -rf $file9
	fi
	if [ -e $file10 ]; then
	    rm -rf $file10
	fi
	if [ -e $file11 ]; then
	    rm -rf $file11
	fi
	if [ -e $file12 ]; then
	    rm -rf $file12
	fi

	# Run_number 30159245, 30159247, 34709367 (Data18)
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827301/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file0
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827316/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file1
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827331/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file2
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827344/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file3
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827358/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file4
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827372/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file5
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827385/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file6
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827400/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file7
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827415/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file8
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827428/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file9
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827441/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file10
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827456/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file11
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run38827471/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file12

	if [ -e $file ]; then
	    rm -rf $file
	fi

	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/PPref/Run38827*.root | xargs hadd -f $file

    else
	file=Main_Data.root
	file_1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17.root
	file_2=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18.root
	file0=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682139.root
	file1=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682445.root
	file2=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682461.root
	file20=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682461-0.root
	file21=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682461-1.root
	file3=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682153.root
	file30=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682153-0.root
	file31=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682153-1.root
	file32=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682153-2.root
	file4=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682480.root
	file5=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682503.root
	file6=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682519.root
	file7=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539.root
	file70=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-0.root
	file71=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-1.root
	file72=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-2.root
	file73=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-3.root
	file74=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-4.root
	file75=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539-5.root
	file8=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682555.root
	file9=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682580.root
	file10=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594.root
	file100=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594-0.root
	file101=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594-1.root
	file102=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594-2.root
	file103=/gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594-3.root
	if [ -e $file0 ]; then
	    rm -rf $file0
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682139/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file0
	if [ -e $file1 ]; then
	    rm -rf $file1
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682445/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file1
	if [ -e $file2 ]; then
	    rm -rf $file2
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682461/job.000*/produced_rootfiles/*.root* | xargs hadd -f $file20
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682461/job.001*/produced_rootfiles/*.root* | xargs hadd -f $file21
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682461*.root* | xargs hadd -f $file2
	if [ -e $file20 ]; then
	    rm -rf $file20
	fi
	if [ -e $file21 ]; then
	    rm -rf $file21
	fi
	if [ -e $file3 ]; then
	    rm -rf $file3
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682153/job.000*/produced_rootfiles/*.root* | xargs hadd -f $file30
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682153/job.001*/produced_rootfiles/*.root* | xargs hadd -f $file31
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682153/job.002*/produced_rootfiles/*.root* | xargs hadd -f $file32
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682153*.root* | xargs hadd -f $file3
	if [ -e $file30 ]; then
	    rm -rf $file30
	fi
	if [ -e $file31 ]; then
	    rm -rf $file31
	fi
	if [ -e $file32 ]; then
	    rm -rf $file32
	fi
	if [ -e $file4 ]; then
	    rm -rf $file4
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682480/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file4
	if [ -e $file5 ]; then
	    rm -rf $file5
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682503/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file5
	if [ -e $file6 ]; then
	    rm -rf $file6
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682519/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file6
	if [ -e $file7 ]; then
	    rm -rf $file7
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.000*/produced_rootfiles/*.root* | xargs hadd -f $file70
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.001*/produced_rootfiles/*.root* | xargs hadd -f $file71
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.002*/produced_rootfiles/*.root* | xargs hadd -f $file72
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.003*/produced_rootfiles/*.root* | xargs hadd -f $file73
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.004*/produced_rootfiles/*.root* | xargs hadd -f $file74
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682539/job.005*/produced_rootfiles/*.root* | xargs hadd -f $file75
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682539*.root* | xargs hadd -f $file7
	if [ -e $file70 ]; then
	    rm -rf $file70
	fi
	if [ -e $file71 ]; then
	    rm -rf $file71
	fi
	if [ -e $file72 ]; then
	    rm -rf $file72
	fi
	if [ -e $file73 ]; then
	    rm -rf $file73
	fi
	if [ -e $file74 ]; then
	    rm -rf $file74
	fi
	if [ -e $file75 ]; then
	    rm -rf $file75
	fi
	if [ -e $file8 ]; then
	    rm -rf $file8
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682555/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file8
	if [ -e $file9 ]; then
	    rm -rf $file9
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682580/job.00*/produced_rootfiles/*.root* | xargs hadd -f $file9
	if [ -e $file10 ]; then
	    rm -rf $file10
	fi
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682594/job.000*/produced_rootfiles/*.root* | xargs hadd -f $file100
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682594/job.001*/produced_rootfiles/*.root* | xargs hadd -f $file101
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682594/job.002*/produced_rootfiles/*.root* | xargs hadd -f $file102
	ls -v /home/ytsujika/AnalysisBphys/run/run1_B/Run41682594/job.003*/produced_rootfiles/*.root* | xargs hadd -f $file103
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682594*.root* | xargs hadd -f $file10
	if [ -e $file100 ]; then
	    rm -rf $file100
	fi
	if [ -e $file101 ]; then
	    rm -rf $file101
	fi
	if [ -e $file102 ]; then
	    rm -rf $file102
	fi
	if [ -e $file103 ]; then
	    rm -rf $file103
	fi

	if [ -e $file_1 ]; then
	    rm -rf $file_1
	fi
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data17/Run41682*.root | xargs hadd -f $file_1
	if [ -e $file_2 ]; then
	    rm -rf $file_2
	fi
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data18/Run41682*.root | xargs hadd -f $file_2
	if [ -e $file ]; then
	    rm -rf $file
	fi
	ls -v /gpfs/fs8001/ytsujika/rootfile_for_hadd/Data*.root | xargs hadd -f $file

    fi    

fi
