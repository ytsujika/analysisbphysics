# =============================================================================
# =============================================================================

basedir=/home/ytsujika/AnalysisBphys
outdir=produced_rootfiles

# =============================================================================
# setup
# =============================================================================

rel_ver=21.2.272

cd $basedir/build

source $AtlasSetup/scripts/asetup.sh
source ./x86_64-centos7-gcc8-opt/setup.sh
cd ../

cd $rundir

# =============================================================================
# setup dir
# =============================================================================

jobdir=job.${ijob}
if [ -e $jobdir ]; then
    rm -rf $jobdir
fi

mkdir $jobdir
cd $jobdir

cleanup_files=(PoolFileCatalog.xml
    eventLoopHeartBeat.txt
    log
    myfile.root
    prw.root
    signal.ascii
    background.ascii
    error.log
    success.log
)

for file in ${cleanup_files[@]}; do
    if [ -e $file ]; then
	echo "removing: $file"
	rm -f $file
    fi
done

# =============================================================================
# run athena
# =============================================================================

if [ $test_mode -eq 1 ]; then
    maxEvts=20
else
    maxEvts=-1
fi


if [ -d $outdir ] ; then
    echo "clearing $outdir ..."
    rm -rf $outdir
fi

mkdir $outdir

echo "===== ijob=$ijob: aodfile=$aodfile ====="

if [ -e aod.v0.pool.root ]; then
    rm aod.v0.pool.root
fi

ln -s ../aoddir/$aodfile aod.v0.pool.root

logfile=log.$ijob
export FRONTIER_SERVER=$frontier

#athena ${jobops} --evtMax=$maxEvts  --filesInput=aod.v0.pool.root > $logfile 2>&1
athena ${jobops} --evtMax=-1  --filesInput=aod.v0.pool.root > $logfile 2>&1

newfile=myfile.root.${ijob}
echo "moving myfile.root as $outdir/$newfile ..."
mv myfile.root $outdir/$newfile
mv $logfile    $outdir

# =============================================================================
# --- end of script
# =============================================================================
