#!/usr/bin/zsh -f

# =============================================================================
# options to change
# =============================================================================

test_mode=0
isMC=$1

Run_number=$2

aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction

if [ $test_mode -eq 1 ]; then
    queue=q30m
else
    queue=q4h
fi

#MC sample
    rundir=Run$Run_number
# Validation
if [ $Run_number -eq 000 ]; then
    aoddir=$aoddir_base/Validation/MC
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/Validation
elif [ $Run_number -eq 41719774 ]; then
    aoddir=$aoddir_base/Validation/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r10244_v27_a2_EXT0
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/Validation
elif [ $Run_number -eq 41719777 ]; then
    aoddir=$aoddir_base/Validation/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r11165_v27_a2_EXT0
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/Validation
elif [ $Run_number -eq 41890581 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r10244_v15_a4_EXT0
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/NonSkim
elif [ $Run_number -eq 41890592 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r11165_v15_a4_EXT0
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/NonSkim
elif [ $Run_number -eq 41907857 ]; then
    aoddir=$aoddir_base/NonSkim/user.ytsujika.mc16_13TeV.801990.P8BEG_A14_NNPDF23LO_bb_DstarMunu_2mu2p5.deriv.DAOD_BPHY27.e8574_a875_r10244_v15_a4_EXT0
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/NonSkim
#data
elif [ $Run_number -eq 354396 ]; then
    aoddir=$aoddir_base/Validation/Data
    base_dir=/home/ytsujika/AnalysisBphys/run/run1_B/Validation
else
    exit 0;
fi

# =============================================================================
# basically no change needed in below
# =============================================================================

if [ $isMC -eq 1 ]; then
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_mc.py
else
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_data.py
fi

jobname=analysis_Bphys

base_script=submit_analy_Bphys_script.sh

cd $base_dir

if [ -e $rundir ]; then
    echo "ERROR: $rundir already exists, and remove the directory"
    rm -rf $rundir
fi

mkdir $rundir
cd    $rundir

# =============================================================================
# count nr of jobs
# =============================================================================

if [ -e aoddir ]; then
    rm aoddir
fi
ln -s $aoddir aoddir

#Validation
if [ $Run_number -eq 000 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY27\.mc16_13TeV\.(\d+)\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41719774 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41719774\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41719777 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41719777\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41890581 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41890581\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41890592 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41890592\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 354396 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY27\.data18_13TeV\.00354396\.(\d+)\.pool\.root/;print "$1\n";}'`)
else
    exit 0;
fi
aodjobs=($(for v in "${aaa[@]}"; do echo "$v"; done | sort -n))

echo "================== $rundir =================="
echo "Input AOD directory: $aoddir"
echo "Total number of jobs: ${#aodjobs[@]}"
echo "Submit to queue: $queue"

# =============================================================================
# make script
# =============================================================================

scrdir=scripts
scrdir2=scripts2
exedir=execute
logdir=log
outdir=out
errdir=err

if [ ! -d  $scrdir ]; then
     mkdir $scrdir
fi
if [ ! -d  $scrdir2 ]; then
     mkdir $scrdir2
fi
if [ ! -d  $exedir ]; then
     mkdir $exedir
fi
if [ ! -d  $outdir ]; then
     mkdir $outdir
fi
if [ ! -d  $errdir ]; then
     mkdir $errdir
fi
if [ ! -d  $logdir ]; then
     mkdir $logdir
fi

njobs=0
for ijob in ${aodjobs[@]}
do
    #Validation
    if [ $Run_number -eq 000 ]; then
	aodfile=DAOD_BPHY27.mc16_13TeV.${ijob}.pool.root
    elif [ $Run_number -eq 41719774 ]; then
	aodfile=user.ytsujika.41719774.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41719777 ]; then
	aodfile=user.ytsujika.41719777.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41890581 ]; then
	aodfile=user.ytsujika.41890581.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41890592 ]; then
	aodfile=user.ytsujika.41890592.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 354396 ]; then
	aodfile=DAOD_BPHY27.data18_13TeV.00354396.${ijob}.pool.root
    fi
    echo "----- $aodfile -----"
    outfile=${outdir}/out.${ijob}
    errfile=${errdir}/err.${ijob}
    logfile=${logdir}/log.${ijob}
    exefile=${exedir}/${jobname}_${ijob}.sdf
    sfile=${scrdir}/${jobname}_${ijob}.sh
    sfile2=${scrdir2}/${jobname}_${ijob}.sh
    if [ -e $sfile ]; then
	rm  $sfile
    fi
    if [ -e $sfile2 ]; then
	rm  $sfile2
    fi

    touch $sfile
    echo "#!/usr/bin/zsh -f" >> $sfile
    echo "" >> $sfile
    echo "rundir=$base_dir/$rundir" >> $sfile
    echo "ijob=$ijob" >> $sfile
    echo "aodfile=$aodfile" >> $sfile
    echo "jobops=$jobops" >> $sfile
    echo "test_mode=$test_mode" >> $sfile
    echo "frontier=\"${FRONTIER_SERVER}\""
    echo "" >> $sfile
    cat ../../../../scripts/${base_script} >> $sfile
    chmod 755 $sfile

    touch $sfile2
    echo "#!/usr/bin/zsh -f" >> $sfile2
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/" >> $sfile2
    echo "export ALRB_CONT_RUNPAYLOAD=\"$sfile\"" >> $sfile2
    echo "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c centos7 -m /gpfs:/gpfs -m /home:/home -b" >> $sfile2
    echo "" >> $sfile2
    chmod 755 $sfile2

    if [ -e $exefile ]; then
	rm  $exefile
    fi
    touch $exefile
    echo "executable  = $base_dir/$rundir/$sfile2 " >> $exefile
    echo "output     = $outfile " >> $exefile
    echo "error      = $errfile " >> $exefile
    echo "log        = $logfile " >> $exefile
    echo "job_queue  = $queue" >> $exefile
    echo "queue" >> $exefile
    chmod 755 $exefile

    condor_submit $exefile
    njobs=`expr $njobs + 1`
#    if [ $njobs -ge 100 ]; then
#	exit
#    fi
    if [ $test_mode -eq 1 ]; then
	if [ $njobs -ge 2 ]; then
	    break;
	fi
    fi
done

cd ../../../../build/
# =============================================================================
# --- end of script
# =============================================================================
