#!/usr/bin/zsh -f

# =============================================================================
# options to change
# =============================================================================

test_mode=0
isMC=$1

Run_number=$2
aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction/

if [ $test_mode -eq 1 ]; then
    queue=q30m
else
    queue=q4h
fi

#MC sample
if [ $Run_number -eq 000 ]; then
    aoddir=$aoddir_base/Validation/MC
elif [ $Run_number -eq 100011 ]; then
    aoddir=$aoddir_base/Truth/test_100011_DAOD_TRUTH1
elif [ $Run_number -eq 41890581 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r10244_v15_a4_EXT0
elif [ $Run_number -eq 41890592 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r11165_v15_a4_EXT0
elif [ $Run_number -eq 41907857 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801990.P8BEG_A14_NNPDF23LO_bb_DstarMunu_2mu2p5.deriv.DAOD_BPHY27.e8574_a875_r10244_v15_a4_EXT0
elif [ $Run_number -eq 41907857 ]; then
    aoddir=$aoddir_base/NonSkim/MC16/user.ytsujika.mc16_13TeV.801990.P8BEG_A14_NNPDF23LO_bb_DstarMunu_2mu2p5.deriv.DAOD_BPHY27.e8574_a875_r11165_v15_a4_EXT0
else
    exit 0;
fi

rundir=Run$Run_number


# =============================================================================
# basically no change needed in below
# =============================================================================

jobname=analysis_Bphys

base_script=truth_submit_analy_Bphys_script.sh

cd /home/ytsujika/AnalysisBphys/

cd run/run1_B/Truth

if [ -e $rundir ]; then
    echo "ERROR: $rundir already exists, and remove the directory"
    rm -rf $rundir
fi

mkdir $rundir
cd    $rundir

# =============================================================================
# count nr of jobs
# =============================================================================

if [ -e aoddir ]; then
    rm aoddir
fi
ln -s $aoddir aoddir

#MC sample
if [ $Run_number -eq 000 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY27\.mc16_13TeV\.(\d+)\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 100011 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_TRUTH1\._(\d+)\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 41890581 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41890581\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41890592 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41890592\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41907857 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41907857\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
else
    exit 0;
fi
aodjobs=($(for v in "${aaa[@]}"; do echo "$v"; done | sort -n))

echo "================== $rundir =================="
echo "Input AOD directory: $aoddir"
echo "Total number of jobs: ${#aodjobs[@]}"
echo "Submit to queue: $queue"

# =============================================================================
# make script
# =============================================================================

scrdir=scripts
scrdir2=scripts2
exedir=execute
logdir=log
outdir=out
errdir=err

if [ ! -d  $scrdir ]; then
     mkdir $scrdir
fi
if [ ! -d  $scrdir2 ]; then
     mkdir $scrdir2
fi
if [ ! -d  $exedir ]; then
     mkdir $exedir
fi
if [ ! -d  $outdir ]; then
     mkdir $outdir
fi
if [ ! -d  $errdir ]; then
     mkdir $errdir
fi
if [ ! -d  $logdir ]; then
     mkdir $logdir
fi

njobs=0
for ijob in ${aodjobs[@]}
do
    #MC sample
    if [ $Run_number -eq 000 ]; then
	aodfile=DAOD_BPHY27.mc16_13TeV.${ijob}.pool.root
    elif [ $Run_number -eq 100011 ]; then
	aodfile=DAOD_TRUTH1._${ijob}.root.1
    elif [ $Run_number -eq 41890581 ]; then
	aodfile=user.ytsujika.41890581.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41890592 ]; then
	aodfile=user.ytsujika.41890592.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41907857 ]; then
	aodfile=user.ytsujika.41907857.EXT0._${ijob}.DAOD_BPHY27.pool.root
    fi
    echo "----- $aodfile -----"
    outfile=${outdir}/out.${ijob}
    errfile=${errdir}/err.${ijob}
    logfile=${logdir}/log.${ijob}
    exefile=${exedir}/${jobname}_${ijob}.sdf
    sfile=${scrdir}/${jobname}_${ijob}.sh
    sfile2=${scrdir2}/${jobname}_${ijob}.sh
    if [ -e $sfile ]; then
	rm  $sfile
    fi
    if [ -e $sfile2 ]; then
	rm  $sfile2
    fi
    touch $sfile
    echo "#!/usr/bin/zsh -f" >> $sfile
    echo "" >> $sfile
    echo "rundir=run/run1_B/Truth/$rundir" >> $sfile
    echo "ijob=$ijob" >> $sfile
    echo "aodfile=$aodfile" >> $sfile
    echo "jobops=$jobops" >> $sfile
    echo "test_mode=$test_mode" >> $sfile
    echo "frontier=\"${FRONTIER_SERVER}\""
    echo "" >> $sfile
    cat ../../../../scripts/${base_script} >> $sfile
    chmod 755 $sfile

    touch $sfile2
    echo "#!/usr/bin/zsh -f" >> $sfile2
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/" >> $sfile2
    echo "export ALRB_CONT_RUNPAYLOAD=\"$sfile\"" >> $sfile2
    echo "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c centos7 -m /gpfs:/gpfs -b" >> $sfile2
    echo "" >> $sfile2
    chmod 755 $sfile2

    if [ -e $exefile ]; then
	rm  $exefile
    fi
    touch $exefile
    echo "executable  = /home/ytsujika/AnalysisBphys/run/run1_B/Truth/$rundir/$sfile2 " >> $exefile
    echo "output     = $outfile " >> $exefile
    echo "error      = $errfile " >> $exefile
    echo "log        = $logfile " >> $exefile
    echo "job_queue  = $queue" >> $exefile
    echo "queue" >> $exefile
    chmod 755 $exefile

    condor_submit $exefile
    njobs=`expr $njobs + 1`
#    if [ $njobs -ge 100 ]; then
#	exit
#    fi
    if [ $test_mode -eq 1 ]; then
	if [ $njobs -ge 2 ]; then
	    break;
	fi
    fi
done

cd ../../../../build/
# =============================================================================
# --- end of script
# =============================================================================
