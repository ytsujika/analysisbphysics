# =============================================================================
# =============================================================================

basedir=/home/ytsujika/AnalysisBphys
outdir=produced_rootfiles

# =============================================================================
# function
# =============================================================================

function check_job() {

    files=(aod.v0.pool.root)
    logs=(log.$ijob)

    # -- log file check

    for log in "${logs[@]}"
    do
	echo "============ checking message in log: $log ============"
	echo "+++++ doDisJet in log +++++"
	grep -i doDisJet $log > disjet.log
	mynum=`grep True disjet.log | wc -l`
	if [ $mynum -eq 0 ]; then
	    echo "==> ERROR: no doDisJet=True printout in log=$log"
	fi

	echo "+++++ fullscanUcnvtrk in log +++++"
	grep -i fullscanU $log > fullscanu.log
	mynum=`grep -i fullscanU fullscanu.log | wc -l`
	if [ $mynum -eq 0 ]; then
	echo "==> ERROR: no fullscanU printout in log=$log"
	fi

	echo "+++++ athena prints in log +++++"
	grep ERROR $log | grep -v "TOTAL ERRORS" > error.log
	mynum=`cat error.log | wc -l`
	if [ $mynum -ne 0 ]; then
	    echo "==> ERROR: ERROR prints in log=$log"
	fi
    done

    # -- xAOD content check

    for file in "${files[@]}"
    do
	echo "============ checking object in xAOD: $log ============"

	checkxAOD.py $file |& grep -i lrt > compo.file

	echo "+++++ disJetFitTrk in xAOD +++++"
	mynum=`grep disJetFitTrk compo.file | wc -l`
	if [ $mynum -eq 0 ]; then
	    echo "==> ERROR: no disJetFitTrk class in xAOD file=$file"
	fi
	echo "+++++ disJetSpacePoint in xAOD +++++"
	mynum=`grep disJetSpacePoint compo.file | wc -l`
	if [ $mynum -eq 0 ]; then
	    echo "==> ERROR: no disJetSpacePoint class in xAOD file=$file"
	fi
    done
}

# =============================================================================
# setup
# =============================================================================

rel_ver=21.2.90

cd $basedir

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#setupATLAS

cd build
asetup
source ./x86_64-centos7-gcc8-opt/setup.sh
cd ../

cd $rundir

# =============================================================================
# setup dir
# =============================================================================

jobdir=job.${ijob}
if [ -e $jobdir ]; then
    rm -rf $jobdir
fi

mkdir $jobdir
cd $jobdir

cleanup_files=(PoolFileCatalog.xml
    eventLoopHeartBeat.txt
    log
    myfile.root
    prw.root
    signal.ascii
    background.ascii
    error.log
    success.log
)

for file in ${cleanup_files[@]}; do
    if [ -e $file ]; then
	echo "removing: $file"
	rm -f $file
    fi
done

# =============================================================================
# run athena
# =============================================================================

if [ $test_mode -eq 1 ]; then
    maxEvts=20
else
    maxEvts=-1
fi


if [ -d $outdir ] ; then
    echo "clearing $outdir ..."
    rm -rf $outdir
fi

mkdir $outdir

echo "===== ijob=$ijob: aodfile=$aodfile ====="

if [ -e aod.v0.pool.root ]; then
    rm aod.v0.pool.root
fi

ln -s ../aoddir/$aodfile aod.v0.pool.root

logfile=log.$ijob
export FRONTIER_SERVER=$frontier

#athena ${jobops} --evtMax=$maxEvts  --filesInput=aod.v0.pool.root > $logfile 2>&1
athena ${jobops} --evtMax=-1  --filesInput=aod.v0.pool.root > $logfile 2>&1

echo "running check_job ..."
check_job >> $logfile

newfile=myfile.root.${ijob}
echo "moving myfile.root as $outdir/$newfile ..."
mv myfile.root $outdir/$newfile
mv $logfile    $outdir

# =============================================================================
# --- end of script
# =============================================================================
