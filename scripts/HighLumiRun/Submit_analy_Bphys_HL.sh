#!/usr/bin/zsh -f

# =============================================================================
# options to change
# =============================================================================

test_mode=0
isMC=0
isNew=1

# Number : 30159245, 30159247, 34709367 (Data18)
Run_number=34709367

if [ $isMC -eq 1 ]; then
    aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction/HighLumiRun/MC16
else
    if [ $isNew -eq 1 ]; then
	aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction/HighLumiRun/Data18_NewDeriv
    else
	aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction/HighLumiRun/Data18
    fi
fi

if [ $test_mode -eq 1 ]; then
    queue=q30m
else
    if [ $isMC -eq 1 ]; then
	queue=q12h
    else
	queue=q1d
    fi
fi

if [ $Run_number -eq 30159245 ]; then
    aoddir=$aoddir_base/data18_13TeV.00355563.physics_Main.deriv.DAOD_BPHY22.f950_m1999_p5242
    rundir=Run$Run_number
elif [ $Run_number -eq 30159247 ]; then
    aoddir=$aoddir_base/data18_13TeV.00355599.physics_Main.deriv.DAOD_BPHY22.f950_m1999_p5242
    rundir=Run$Run_number
elif [ $Run_number -eq 34709367 ]; then
    aoddir=$aoddir_base/data18_13TeV.00355544.physics_Main.deriv.DAOD_BPHY22.f950_m1999_p5824
    rundir=Run$Run_number
else
    exit 0;
fi

# =============================================================================
# basically no change needed in below
# =============================================================================

if [ $isMC -eq 1 ]; then
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_mc.py
else
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_data.py
fi
jobname=analysis_Bphys

base_script=Submit_analy_Bphys_HL_script.sh

cd /home/ytsujika/AnalysisBphys/

if [ ! -d run/HighLumiRun ]; then
    echo "ERROR: no directory run"
fi

cd run/HighLumiRun

if [ -e $rundir ]; then
    echo "ERROR: $rundir already exists, and remove the directory"
    rm -rf $rundir
fi

mkdir $rundir
cd    $rundir

# =============================================================================
# count nr of jobs
# =============================================================================

if [ -e aoddir ]; then
    rm aoddir
fi
ln -s $aoddir aoddir

if [ $Run_number -eq 30034238 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.30034238\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 30034270 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.30034270\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 30034256 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.30034256\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 30159245 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.30159245\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 30159247 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.30159247\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 34709367 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY22\.34709367\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
else
    exit 0;
fi
aodjobs=($(for v in "${aaa[@]}"; do echo "$v"; done | sort -n))

echo "================== $rundir =================="
echo "Input AOD directory: $aoddir"
echo "Total number of jobs: ${#aodjobs[@]}"
echo "Submit to queue: $queue"

# =============================================================================
# make script
# =============================================================================

scrdir=scripts
exedir=execute
logdir=log
outdir=out
errdir=err

if [ ! -d  $scrdir ]; then
     mkdir $scrdir
fi
if [ ! -d  $exedir ]; then
     mkdir $exedir
fi
if [ ! -d  $outdir ]; then
     mkdir $outdir
fi
if [ ! -d  $errdir ]; then
     mkdir $errdir
fi
if [ ! -d  $logdir ]; then
     mkdir $logdir
fi

njobs=0
for ijob in ${aodjobs[@]}
do
    if [ $Run_number -eq 30159245 ]; then
	aodfile=DAOD_BPHY22.30159245._${ijob}.pool.root.1
    elif [ $Run_number -eq 30159247 ]; then
	aodfile=DAOD_BPHY22.30159247._${ijob}.pool.root.1
    elif [ $Run_number -eq 34709367 ]; then
	aodfile=DAOD_BPHY22.34709367._${ijob}.pool.root.1
    fi
    echo "----- $aodfile -----"
    outfile=${outdir}/out.${ijob}
    errfile=${errdir}/err.${ijob}
    logfile=${logdir}/log.${ijob}
    exefile=${exedir}/${jobname}_${ijob}.sdf
    sfile=${scrdir}/${jobname}_${ijob}.sh
    if [ -e $sfile ]; then
	rm  $sfile
    fi
    touch $sfile
    echo "#!/usr/bin/zsh -f" >> $sfile
    echo "" >> $sfile
    echo "rundir=run/HighLumiRun/$rundir" >> $sfile
    echo "ijob=$ijob" >> $sfile
    echo "aodfile=$aodfile" >> $sfile
    echo "jobops=$jobops" >> $sfile
    echo "test_mode=$test_mode" >> $sfile
    echo "frontier=\"${FRONTIER_SERVER}\""
    echo "" >> $sfile
    cat ../../../scripts/${base_script} >> $sfile
    chmod 755 $sfile

    if [ -e $exefile ]; then
	rm  $exefile
    fi
    touch $exefile
    echo "executable = /usr/bin/bash" >> $exefile
    echo "arguments  = \"$sfile\" " >> $exefile
    echo "output     = $outfile " >> $exefile
    echo "error      = $errfile " >> $exefile
    echo "log        = $logfile " >> $exefile
    echo "job_queue  = $queue" >> $exefile
    echo "queue" >> $exefile
    chmod 755 $exefile

    condor_submit $exefile
    njobs=`expr $njobs + 1`
#    if [ $njobs -ge 100 ]; then
#	exit
#    fi
    if [ $test_mode -eq 1 ]; then
	if [ $njobs -ge 2 ]; then
	    break;
	fi
    fi
done

cd ../../../build/
# =============================================================================
# --- end of script
# =============================================================================
