#Run the analysis to other samples
isMC=1
isAdd=2

#MC sample
if [ $isMC -eq 1 ];then
    if [ $isAdd -eq -1 ]; then
	source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 41890581
	source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 41890592
    elif [ $isAdd -eq 2 ]; then
	source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41890581
	source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41890592
    elif [ $isAdd -eq -2 ]; then
	source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 41890581
	source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 41890592
    elif [ $isAdd -eq 10 ]; then
	source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 100011
    elif [ $isAdd -eq 11 ]; then
	source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 000
	#source /home/ytsujika/AnalysisBphys/scripts/truth_submit_analy_Bphys.sh 1 000
	#source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41719774
	#source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41719777
    else
	source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41890581
	source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 1 41890592
    fi
#Data
else
    source /home/ytsujika/AnalysisBphys/scripts/validation_submit_analy_Bphys.sh 0 354396
fi
