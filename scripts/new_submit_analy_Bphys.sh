#!/usr/bin/zsh -f

# =============================================================================
# options to change
# =============================================================================

test_mode=0
isMC=$1

# Number : 30034238, 30034270, 30034256 (MC16), 30158587, 30158594, 30158592, 30158585, 30158596 (Data17)
Run_number=$2
aoddir_base=/gpfs/fs8001/ytsujika/dataset_for_DStarReconstruction/NewDeriv

if [ $test_mode -eq 1 ]; then
    queue=q30m
else
    queue=q4h
fi

#MC sample
if [ $Run_number -eq 0 ]; then
    aoddir=$aoddir_base/test
elif [ $Run_number -eq 38681562 ]; then
    aoddir=$aoddir_base/LowLumiRun/user.shion.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.recon.AOD.e8536_a875_r10244_v6_a1_EXT0
elif [ $Run_number -eq 38681600 ]; then
    aoddir=$aoddir_base/LowLumiRun/user.shion.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.recon.AOD.e8536_a875_r11165_v6_a1_EXT0
elif [ $Run_number -eq 38827187 ]; then
    aoddir=$aoddir_base/MC16/user.shion.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r10244_v7_a1_EXT0
elif [ $Run_number -eq 38827200 ]; then
    aoddir=$aoddir_base/MC16/user.shion.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r11165_v7_a1_EXT0
elif [ $Run_number -eq 41614085 ]; then
    aoddir=$aoddir_base/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r10244_v25_a2_EXT0
elif [ $Run_number -eq 41614086 ]; then
    aoddir=$aoddir_base/MC16/user.ytsujika.mc16_13TeV.801686.P8BEG_A14_NNPDF23LO_bb_mu2p5_Dstar4p5.deriv.DAOD_BPHY27.e8536_a875_r11165_v25_a2_EXT0
#Data
#Data17
elif [ $Run_number -eq 38827215 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341294.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827230 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341312.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827243 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341419.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827258 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341534.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827273 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341615.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827288 ]; then
    aoddir=$aoddir_base/Data17/user.shion.data17_13TeV.00341649.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
#Data18
elif [ $Run_number -eq 38827484 ]; then
    aoddir=$aoddir_base/Data18/user.shion.data18_13TeV.00354396.physics_Main.deriv.DAOD_BPHY27.f947_m1993_v7_a1_EXT0
elif [ $Run_number -eq 38827499 ]; then
    aoddir=$aoddir_base/Data18/user.shion.data18_13TeV.00355331.physics_Main.deriv.DAOD_BPHY27.f948_m1993_v7_a1_EXT0
elif [ $Run_number -eq 38827512 ]; then
    aoddir=$aoddir_base/Data18/user.shion.data18_13TeV.00355389.physics_Main.deriv.DAOD_BPHY27.f948_m1993_v7_a1_EXT0
elif [ $Run_number -eq 38827528 ]; then
    aoddir=$aoddir_base/Data18/user.shion.data18_13TeV.00355416.physics_Main.deriv.DAOD_BPHY27.f948_m1993_v7_a1_EXT0
elif [ $Run_number -eq 38827543 ]; then
    aoddir=$aoddir_base/Data18/user.shion.data18_13TeV.00355468.physics_Main.deriv.DAOD_BPHY27.f948_m1993_v7_a1_EXT0
#PPref
elif [ $Run_number -eq 38827301 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340644.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827316 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340683.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827331 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340697.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827344 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340718.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827358 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340814.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827372 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340849.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827385 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340850.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827400 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340910.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827415 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340925.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827428 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00340973.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827441 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00341027.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827456 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00341123.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
elif [ $Run_number -eq 38827471 ]; then
    aoddir=$aoddir_base/PPref/user.shion.data17_5TeV.00341184.physics_Main.deriv.DAOD_BPHY27.r10803_p3630_v7_a1_EXT0
else
    exit 0;
fi

rundir=Run$Run_number


# =============================================================================
# basically no change needed in below
# =============================================================================

if [ $isMC -eq 1 ]; then
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_mc.py
else
    jobops=MyPackage_v2/MyPackage_v2AlgJobOptions_data.py
fi
jobname=analysis_Bphys

base_script=submit_analy_Bphys_script.sh

cd /home/ytsujika/AnalysisBphys/

if [ ! -d run/run1_B/New ]; then
    echo "ERROR: no directory run"
fi

cd run/run1_B/New

if [ -e $rundir ]; then
    echo "ERROR: $rundir already exists, and remove the directory"
    rm -rf $rundir
fi

mkdir $rundir
cd    $rundir

# =============================================================================
# count nr of jobs
# =============================================================================

if [ -e aoddir ]; then
    rm aoddir
fi
ln -s $aoddir aoddir

#MC sample
if [ $Run_number -eq 0 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/DAOD_BPHY27\.0\._(\d+)\.pool\.root\.1/;print "$1\n";}'`)
elif [ $Run_number -eq 38681562 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38681562\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38681600 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38681600\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827187 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827187\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827200 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827200\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41614085 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41614085\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 41614086 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.ytsujika\.41614086\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
#Data
#Data17
elif [ $Run_number -eq 38827215 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827215\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827230 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827230\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827243 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827243\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827258 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827258\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827273 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827273\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827288 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827288\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
#Data18
elif [ $Run_number -eq 38827484 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827484\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827499 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827499\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827512 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827512\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827528 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827528\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827543 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827543\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
#PPref
elif [ $Run_number -eq 38827301 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827301\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827316 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827316\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827331 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827331\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827344 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827344\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827358 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827358\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827372 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827372\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827385 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827385\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827400 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827400\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827415 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827415\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827428 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827428\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827441 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827441\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827456 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827456\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
elif [ $Run_number -eq 38827471 ]; then
    aaa=(`ls -l aoddir/ | awk '{print $9}' | perl -e 'while(<>) {/user\.shion\.38827471\.EXT0\._(\d+)\.DAOD_BPHY27\.pool\.root/;print "$1\n";}'`)
else
    exit 0;
fi
aodjobs=($(for v in "${aaa[@]}"; do echo "$v"; done | sort -n))

echo "================== $rundir =================="
echo "Input AOD directory: $aoddir"
echo "Total number of jobs: ${#aodjobs[@]}"
echo "Submit to queue: $queue"

# =============================================================================
# make script
# =============================================================================

scrdir=scripts
scrdir2=scripts2
exedir=execute
logdir=log
outdir=out
errdir=err

if [ ! -d  $scrdir ]; then
     mkdir $scrdir
fi
if [ ! -d  $scrdir2 ]; then
     mkdir $scrdir2
fi
if [ ! -d  $exedir ]; then
     mkdir $exedir
fi
if [ ! -d  $outdir ]; then
     mkdir $outdir
fi
if [ ! -d  $errdir ]; then
     mkdir $errdir
fi
if [ ! -d  $logdir ]; then
     mkdir $logdir
fi

njobs=0
for ijob in ${aodjobs[@]}
do
    #MC sample
    if [ $Run_number -eq 0 ]; then
	aodfile=DAOD_BPHY27.0._${ijob}.pool.root.1
    elif [ $Run_number -eq 38681562 ]; then
	aodfile=user.shion.38681562.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38681600 ]; then
	aodfile=user.shion.38681600.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827187 ]; then
	aodfile=user.shion.38827187.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827200 ]; then
	aodfile=user.shion.38827200.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41614085 ]; then
	aodfile=user.ytsujika.41614085.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 41614086 ]; then
	aodfile=user.ytsujika.41614086.EXT0._${ijob}.DAOD_BPHY27.pool.root
    #Data
    #Data17
    elif [ $Run_number -eq 38827215 ]; then
	aodfile=user.shion.38827215.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827230 ]; then
	aodfile=user.shion.38827230.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827243 ]; then
	aodfile=user.shion.38827243.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827258 ]; then
	aodfile=user.shion.38827258.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827273 ]; then
	aodfile=user.shion.38827273.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827288 ]; then
	aodfile=user.shion.38827288.EXT0._${ijob}.DAOD_BPHY27.pool.root
    #Data18
    elif [ $Run_number -eq 38827484 ]; then
	aodfile=user.shion.38827484.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827499 ]; then
	aodfile=user.shion.38827499.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827512 ]; then
	aodfile=user.shion.38827512.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827528 ]; then
	aodfile=user.shion.38827528.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827543 ]; then
	aodfile=user.shion.38827543.EXT0._${ijob}.DAOD_BPHY27.pool.root
    #PPref
    elif [ $Run_number -eq 38827301 ]; then
	aodfile=user.shion.38827301.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827316 ]; then
	aodfile=user.shion.38827316.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827331 ]; then
	aodfile=user.shion.38827331.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827344 ]; then
	aodfile=user.shion.38827344.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827358 ]; then
	aodfile=user.shion.38827358.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827372 ]; then
	aodfile=user.shion.38827372.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827385 ]; then
	aodfile=user.shion.38827385.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827400 ]; then
	aodfile=user.shion.38827400.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827415 ]; then
	aodfile=user.shion.38827415.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827428 ]; then
	aodfile=user.shion.38827428.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827441 ]; then
	aodfile=user.shion.38827441.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827456 ]; then
	aodfile=user.shion.38827456.EXT0._${ijob}.DAOD_BPHY27.pool.root
    elif [ $Run_number -eq 38827471 ]; then
	aodfile=user.shion.38827471.EXT0._${ijob}.DAOD_BPHY27.pool.root
    fi
    echo "----- $aodfile -----"
    outfile=${outdir}/out.${ijob}
    errfile=${errdir}/err.${ijob}
    logfile=${logdir}/log.${ijob}
    exefile=${exedir}/${jobname}_${ijob}.sdf
    sfile=${scrdir}/${jobname}_${ijob}.sh
    sfile2=${scrdir2}/${jobname}_${ijob}.sh
    if [ -e $sfile ]; then
	rm  $sfile
    fi
    if [ -e $sfile2 ]; then
	rm  $sfile2
    fi
    touch $sfile
    echo "#!/usr/bin/zsh -f" >> $sfile
    echo "" >> $sfile
    echo "rundir=run/run1_B/New/$rundir" >> $sfile
    echo "ijob=$ijob" >> $sfile
    echo "aodfile=$aodfile" >> $sfile
    echo "jobops=$jobops" >> $sfile
    echo "test_mode=$test_mode" >> $sfile
    echo "frontier=\"${FRONTIER_SERVER}\""
    echo "" >> $sfile
    cat ../../../../scripts/${base_script} >> $sfile
    chmod 755 $sfile

    touch $sfile2
    echo "#!/usr/bin/zsh -f" >> $sfile2
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/" >> $sfile2
    echo "export ALRB_CONT_RUNPAYLOAD=\"$sfile\"" >> $sfile2
    echo "source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh -c centos7 -m /gpfs:/gpfs -b" >> $sfile2
    echo "" >> $sfile2
    chmod 755 $sfile2

    if [ -e $exefile ]; then
	rm  $exefile
    fi
    touch $exefile
    echo "executable  = /home/ytsujika/AnalysisBphys/run/run1_B/New/$rundir/$sfile2 " >> $exefile
    echo "output     = $outfile " >> $exefile
    echo "error      = $errfile " >> $exefile
    echo "log        = $logfile " >> $exefile
    echo "job_queue  = $queue" >> $exefile
    echo "queue" >> $exefile
    chmod 755 $exefile

    condor_submit $exefile
    njobs=`expr $njobs + 1`
#    if [ $njobs -ge 100 ]; then
#	exit
#    fi
    if [ $test_mode -eq 1 ]; then
	if [ $njobs -ge 2 ]; then
	    break;
	fi
    fi
done

cd ../../../../build/
# =============================================================================
# --- end of script
# =============================================================================
